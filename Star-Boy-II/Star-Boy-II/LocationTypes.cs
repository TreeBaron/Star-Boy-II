﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Star_Boy_II
{
    public static class LocationTypes
    {
        public const string Station = "Station";

        public const string Planet = "Planet";

        public const string RoadMarker = "RoadMarker";
    }
}
