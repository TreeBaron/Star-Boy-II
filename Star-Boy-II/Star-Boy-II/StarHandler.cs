﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Windows.UI.WebUI;

namespace Star_Boy_II
{
    public class StarHandler : Renderable
    {
        List<Renderable> Stars = new List<Renderable>();

        Player Player = null;

        Rectangle NewView = Rectangle.Empty;
        Rectangle OldView = Rectangle.Empty;

        Game Game;

        public void Initialize(Game game, Dictionary<string, Texture2D> textures, Player player, Rectangle view)
        {
            Player = player;
            Game = game;
            view = GetViewPosition(view);
            
            Random r = new Random();
            for(int i = 0; i < MetaGameSettings.StarCount; i++)
            {
                Renderable star = new Renderable();

                if (r.NextDouble() > 0.1)
                {
                    star.Texture = textures["StarSprite"];
                }
                else if (r.NextDouble() > 0.5)
                {
                    star.Texture = textures["RedStarSprite"];
                }
                else
                {
                    star.Texture = textures["BlueStarSprite"];
                }

                star.Scale = 0.3f*(float)MetaGameSettings.Random.NextDouble();
                var x = r.Next(view.X - view.Width, view.X + view.Width + 1);
                var y = r.Next(view.Y - view.Height, view.Y + view.Height + 1);
                star.Position = new Vector2(x, y);
                Stars.Add(star);
            }
        }

        public override void Draw(SpriteBatch spriteBatch, Rectangle window)
        {
            var r = MetaGameSettings.Random;
            window = GetViewPosition(window);

            OldView = window;

            if (OldView != Rectangle.Empty && NewView != Rectangle.Empty)
            {
                foreach (var item in Stars)
                {
                    item.Draw(spriteBatch, window);
                }

                var outOfBoundsStars = Stars.Where(x => !InView(x, window));

                // disperse between old view and new view
                foreach(var star in outOfBoundsStars)
                {
                    var x = r.Next(NewView.X, NewView.X + NewView.Width + 1);
                    var y = r.Next(NewView.Y, NewView.Y + NewView.Height + 1);
                    star.Position = new Vector2(x,y);

                    // this is an innefficient, random brute force way of doing this
                    // which may cause variable framerate
                    // but it's easy and probably fine
                    // ...probably
                    while(InView(star, OldView))
                    {
                        x = r.Next(NewView.X - NewView.Width, NewView.X + (NewView.Width*2) + 1);
                        y = r.Next(NewView.Y - NewView.Height, NewView.Y + (NewView.Height*2) + 1);
                        star.Position = new Vector2(x, y);
                    }
                }

            }
        }

        public override void Update(GameTime gameTime)
        {
            // calculate new view using players velocity
            if(OldView != Rectangle.Empty)
            {
                NewView = new Rectangle(OldView.X, OldView.Y, OldView.Width, OldView.Height);
                NewView.X += (int)(Player.Velocity.X * (float)gameTime.ElapsedGameTime.TotalSeconds);
                NewView.Y += (int)(Player.Velocity.Y * (float)gameTime.ElapsedGameTime.TotalSeconds);
            }
        }

        private Rectangle GetViewPosition(Rectangle rectangle)
        {
            // use player position to calculate camera viewing rectangle and adjust accordingly
            var xAdjust = Player.GetCenter().X - (rectangle.Width / 2);
            var yAdjust = Player.GetCenter().Y - (rectangle.Height / 2);
            var newRect = new Rectangle((int)xAdjust, (int)yAdjust, rectangle.Width, rectangle.Height);
            return newRect;
        }

        private bool InView(Renderable star, Rectangle view)
        {
            var center = star.GetCenter();

            const int buffer = 150;

            // build in margin of error for view so that stars don't immediately dissappear
            view.X -= (int)(star.GetWidth() / 2) + buffer;
            view.Y -= (int)(star.GetHeight() / 2) + buffer;
            view.Width += (int)(star.GetWidth() / 2) + buffer;
            view.Height += (int)(star.GetHeight() / 2) + buffer;

            // negative numbers, the less than should be greater than, etc.
            Rectangle starRec = new Rectangle((int)center.X, (int)center.Y, 1, 1);

            if (starRec.Intersects(view)) return true;

            return false;
        }
    }
}
