
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Diagnostics;
using System;
using System.Linq;
using Star_Boy_II.Particles;

namespace Star_Boy_II
{
    public static class LagManager
    {
        public static void Update(GameTime gameTime, Game game)
        {
            #region Debugging Stats
            Dictionary<string, int> objectCounts = new Dictionary<string, int>();
            for (int i = 0; i < game._level.Objects.Count; i++)
            {
                var item = game._level.Objects[i];
                var name = item.GetType().Name;
                if (objectCounts.ContainsKey(name))
                {
                    objectCounts[name] += 1;
                }
                else
                {
                    objectCounts.Add(name, 1);
                }
            }

            var sorted = objectCounts.OrderByDescending(x => x.Value);

            for (int i = 0; i < Math.Min(5, sorted.Count()); i++)
            {
                var selected = sorted.ElementAt(i);
                Debug.WriteLine("[" + (i + 1) + "] " + selected.Key + " at " + selected.Value);
            }

            #endregion

            #region Asteroid Management
            if (objectCounts.ContainsKey(nameof(Asteroid)) && objectCounts[nameof(Asteroid)] > 150)
            {
                // cull asteroids
                var allAsteroids = game._level.Objects.Where(x => x.GetType().Name == nameof(Asteroid));

                // sort by distance to player...
                var sortedAsteroids = allAsteroids.OrderByDescending(x => Vector2.Distance(game._level.Player.Position, x.Position));

                if (sortedAsteroids.Any())
                {
                    while (sortedAsteroids.Count(x => x.RemoveFromLevel == false) > 150)
                    {
                        sortedAsteroids.First(x => x.RemoveFromLevel == false).RemoveFromLevel = true;
                    }
                }
            }
            #endregion

            #region Particle Management
            if (objectCounts.ContainsKey(nameof(ParticleEmitter)) && objectCounts[nameof(ParticleEmitter)] > 200)
            {
                // cull particle emitters
                var allParticleEmitters = game._level.Objects.Where(x => x.GetType().Name == nameof(ParticleEmitter));

                // sort by distance to player...
                var sortedEmitters = allParticleEmitters.OrderByDescending(x => Vector2.Distance(game._level.Player.Position, x.Position));

                if (sortedEmitters.Any())
                {
                    while (sortedEmitters.Count(x => x.RemoveFromLevel == false) > 200)
                    {
                        sortedEmitters.First(x => x.RemoveFromLevel == false).RemoveFromLevel = true;
                    }
                }
            }
            #endregion

            #region Bullet Management
            if (objectCounts.ContainsKey(nameof(Bullet)) && objectCounts[nameof(Bullet)] > 100)
            {
                // cull particle emitters
                var allBullets = game._level.Objects.Where(x => x.GetType().Name == nameof(Bullet)).Cast<Bullet>();

                // sort by distance to player...
                var sortedBullets = allBullets.OrderBy(x => x.Age.Ticks);

                if (sortedBullets.Any())
                {
                    while (sortedBullets.Count(x => x.RemoveFromLevel == false) > 100)
                    {
                        sortedBullets.First(x => x.RemoveFromLevel == false).RemoveFromLevel = true;
                    }
                }
            }
            #endregion
        }
    }
}