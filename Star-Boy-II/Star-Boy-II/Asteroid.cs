﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Star_Boy_II.Particles;

namespace Star_Boy_II
{
    public class Asteroid : Renderable
    {
        public enum AsteroidType
        {
            Fuel,
            Money,
            Damage,
            Health,
        }

        public AsteroidType PowerUpType { get; set; }

        public SpaceShip Ship { get; set; }

        public Level Level { get; set; }

        public Asteroid(SpaceShip ship, Level level, AsteroidType powerUpType, int speed = 100)
        {
            TeamNumber = 1;
            Random random = new Random();
            Ship = ship;
            Level = level;
            PowerUpType = powerUpType;
            Velocity = new Vector2(random.Next(-1* speed, speed), random.Next(-1 * speed, speed));
        }

        public override void OnCollision(object collidedWith)
        {
            ICollidable renderableCol = collidedWith as ICollidable;
            if (collidedWith.GetType() == typeof(Bullet))
            {
                var bullet = (Bullet)collidedWith;
                if (bullet.TeamNumber != TeamNumber)
                {
                    Ship.ApplyEffect(bullet.Effect);
                }
            }
            else if (renderableCol != null)
            {
                Level.Objects.Add(ParticleEmitterFactory.GetShreddedMetalEmitter(Position, "Asteroid collision."));
                Level.Objects.Add(ParticleEmitterFactory.GetExplosionEmitter(Position, "Asteroid collision."));
                Ship.Health -= 50;
            }

            if (Ship.IsDead())
            {
                // place powerup
                if (PowerUpType == AsteroidType.Fuel)
                {
                    Level.Objects.Add(GameFactory.GetFuelPowerUp(TeamNumber, Level, Position));
                    Level.Objects.Add(ParticleEmitterFactory.GetSmokeEmitter(Position, "Powerup collision."));
                }
                else if (PowerUpType == AsteroidType.Money)
                {
                    Level.Objects.Add(GameFactory.GetMoneyPowerUp(TeamNumber, Level, Position));
                    Level.Objects.Add(ParticleEmitterFactory.GetSmokeEmitter(Position, "Powerup collision."));
                }
                else if (PowerUpType == AsteroidType.Health)
                {
                    Level.Objects.Add(GameFactory.GetHealthPowerUp(TeamNumber, Level, Position));
                    Level.Objects.Add(ParticleEmitterFactory.GetShreddedMetalEmitter(Position, "Powerup collision."));
                }
                else
                {
                    Level.Objects.Add(ParticleEmitterFactory.GetSmokeEmitter(Position, "Dead ship."));
                    Level.Objects.Add(ParticleEmitterFactory.GetShreddedMetalEmitter(Position, "Dead ship."));
                    Level.Objects.Add(ParticleEmitterFactory.GetExplosionEmitter(Position, "Dead ship."));
                }
               
                Random random = new Random(DateTime.Now.Second);
                // spawn more asteroids at half size
                if(Scale >= 2.0 && PowerUpType != AsteroidType.Health)
                {
                    for(int i = 0; i < 3; i++)
                    {
                        var asteroid = GameFactory.GetRandomAsteroid(Level);
                        asteroid.Scale = Scale / 3f;
                        asteroid.Position = Position;

                        if (asteroid.PowerUpType != AsteroidType.Health)
                        {
                            Level.Objects.Add(asteroid);
                        }
                    }
                }

                RemoveFromLevel = true;
            }
        }

        public override void Update(GameTime gameTime)
        {
            Position += Velocity * (float)gameTime.ElapsedGameTime.TotalSeconds;
            UpdateCollisionBox();

            if(Vector2.Distance(Position, Level.Camera.CamerasDesiredPosition) > MetaGameSettings.MapWidth)
            {
                RemoveFromLevel = true;
            }
        }
    }
}
