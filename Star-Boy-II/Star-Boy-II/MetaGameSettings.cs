using System;
using System.Numerics;

namespace Star_Boy_II
{
    public static class MetaGameSettings
    {
        public static Random Random = new Random();

        public const int PlayerAsteroidDamage = 40;

        public static int AsteroidTeamNumber = 69;

        public static int MapWidth { get; internal set; } = 100_000;
        public static int MapHeight { get; internal set; } = 100_000;
        public static int ScreenDistance { get; internal set; } = 5_000;
        public static int StarCount { get; set; } = 3200;

        public static bool Fullscreen = false;

        public static Vector2 GetRandomMapPosition(Random random)
        {
            return new Vector2(random.Next(-MetaGameSettings.MapWidth, MetaGameSettings.MapWidth), random.Next(-MetaGameSettings.MapWidth, MetaGameSettings.MapWidth));
        }
    }
}