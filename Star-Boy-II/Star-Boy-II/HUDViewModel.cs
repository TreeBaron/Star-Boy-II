﻿using Star_Boy_II.TextAdventureStuff;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Star_Boy_II
{
    public class HUDViewModel : INotifyPropertyChanged
    {
        public Game Game { get; set; }

        public InputManager InputManager { get; set; }

        public Action ScrollDown { get; set; }

        private string _shipstatustext = "";
        public string ShipStatusText
        {
            get { return _shipstatustext; }
            set
            {
                if (value != _shipstatustext)
                {
                    _shipstatustext = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _spaceshipImagePath = "";
        public string SpaceshipImagePath
        {
            get { return _spaceshipImagePath; }
            set
            {
                if (value != _spaceshipImagePath)
                {
                    _spaceshipImagePath = value;
                    OnPropertyChanged();
                }
            }
        }

        private Visibility _showMainUI = Visibility.Visible;
        public Visibility ShowMainUI
        {
            get { return _showMainUI; }
            set
            {
                _showMainUI = value;
                OnPropertyChanged();
            }
        }

        private Visibility _showTextAdventureScreen = Visibility.Collapsed;
        public Visibility ShowTextAdventureScreen
        {
            get { return _showTextAdventureScreen; }
            set
            {
                _showTextAdventureScreen = value;
                OnPropertyChanged();
            }
        }

        public bool OnAdventureSCreen()
        {
            if(ShowTextAdventureScreen == Visibility.Visible)
            {
                return true;
            }
            return false;
        }

        public void ShowAdventureScreen()
        {
            ShowTextAdventureScreen = Visibility.Visible;
            Game._level.Paused = true;
            DisplayText = "";
            ShowMainUI = Visibility.Collapsed;
        }

        public void HideAdventureScreen()
        {
            ShowTextAdventureScreen = Visibility.Collapsed;
            Game._level.Paused = false;
            ShowMainUI = Visibility.Visible;
            InputManager.Inputs.Add(""); // hacky fix to bug
        }

        private string _userInputText = "Type a command here.";
        public string UserInputText
        {
            get { return _userInputText; }
            set
            {
                if (value != _userInputText)
                {
                    _userInputText = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _displayText = "";
        public string DisplayText
        {
            get { return _displayText; }
            set
            {
                _displayText = value;
                OnPropertyChanged();
            }
        }

        private string _money = "$0";
        public string Money
        {
            get { return _money; }
            set
            {
                _money = value;
                OnPropertyChanged();
            }
        }

        private string _bearing = "0.0";
        public string Bearing
        {
            get { return _bearing; }
            set
            {
                _bearing = value;
                OnPropertyChanged();
            }
        }

        private string _position = "(0,0)";
        public string Position
        {
            get { return _position; }
            set
            {
                _position = value;
                OnPropertyChanged();
            }
        }


        private string _planetBearing = "0.0";
        public string PlanetBearing
        {
            get { return _planetBearing; }
            set
            {
                _planetBearing = value;
                OnPropertyChanged();
            }
        }

        private int _ammo = 0;
        public int Ammo
        {
            get { return _ammo; }
            set
            {
                _ammo = value;
                OnPropertyChanged();
            }
        }

        private int _health = 100;
        public int Health
        {
            get { return _health; }
            set
            {
                _health = value;
                OnPropertyChanged();
            }
        }

        private int _fuel = 100;
        public int Fuel
        {
            get { return _fuel; }
            set
            {
                _fuel = value;
                OnPropertyChanged();
            }
        }

        public string WheelSelection { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        protected bool SetProperty<T>(ref T field, T newValue, [CallerMemberName] string propertyName = null)
        {
            if (!Equals(field, newValue))
            {
                field = newValue;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
                return true;
            }

            return false;
        }
  
    }
}
