﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Star_Boy_II.Particles;

namespace Star_Boy_II
{
    public class TrafficAI : Renderable
    {
        public SpaceShip Ship { get; set; } = SpaceShip.GetDefaultShip();

        public float WaypointDistance { get; set; } = 250f;

        Vector2 CurrentWaypoint { get; set; }

        List<Vector2> WaypointsToVisit { get; set; }

        List<Vector2> WaypointsVisited { get; set; }

        Level Level { get; set; }

        public TrafficAI(List<Vector2> waypointsToVisit, Level level, Texture2D texture)
        {
            Texture = texture;
            Random random = new Random();
            Level = level;
            WaypointsToVisit = waypointsToVisit;
            WaypointsVisited = new List<Vector2>();
            Ship.Speed = 6.0f + (float)(random.NextDouble() * 30.0f);
            Ship.TurnRate = Degrees(0.25);

            Ship.MaxHealth = 1000; // syntax!
            Ship.Health = 1000;

            if (waypointsToVisit.Count > 0)
            {
                CurrentWaypoint = waypointsToVisit[0];
                WaypointsToVisit.RemoveAt(0);
            }

            WaypointDistance = Math.Max(GetWidth(), GetHeight());
        }

        public override void Update(GameTime gameTime)
        {
            for(int i = 0; i < Level.RoadWaypoints.Count; i++)
            {
                var selectedPoint = Level.RoadWaypoints[i];
                if(!WaypointsToVisit.Contains(selectedPoint) && !WaypointsVisited.Contains(selectedPoint) && CurrentWaypoint != selectedPoint)
                {
                    WaypointsToVisit.Add(selectedPoint);
                }
            }

            base.Update(gameTime);

            CurrentWaypoint.Normalize();

            float wantedRotation = AngleUtility.GetAngleToPosition(Position, CurrentWaypoint);

            Rotation = AngleUtility.RotationLogic(Rotation, wantedRotation, Ship.TurnRate);

            var distance = Vector2.Distance(GetCenter(), CurrentWaypoint);
            const float meter = 750;
            if (distance < meter)
            {
                // slow throttle as approaching target
                var throttle = (((1.0 / meter) * distance) * Ship.Speed) + (Ship.Speed * 0.03);
                MoveTowards(Rotation, (float)throttle, gameTime);
            }
            else
            {
                MoveTowards(Rotation, Ship.Speed, gameTime);
            }

            // simulate friction
            Velocity = new Vector2(Velocity.X * 0.9f, Velocity.Y * 0.9f);

            if (distance < WaypointDistance)
            {
                if (WaypointsToVisit.Count == 0) return;

                WaypointsVisited.Add(CurrentWaypoint);
                CurrentWaypoint = WaypointsToVisit[0];
                WaypointsToVisit.RemoveAt(0);
            }

            UpdateCollisionBox();
        }

        public override void OnCollision(Object collidedWith)
        {
            ICollidable renderableCol = collidedWith as ICollidable;
            if (collidedWith.GetType() == typeof(Bullet))
            {
                var bullet = (Bullet)collidedWith;
                if (bullet.TeamNumber != TeamNumber && RemoveFromLevel == false && bullet.Owner != null)
                {
                    Ship.ApplyEffect(bullet.Effect);

                    // Turn into a combat ai in the blink of an eye
                    var gun = GameFactory.GetLazerGun(TeamNumber, Level, 1);
                    var combatModel = new CombatAI(TeamNumber, bullet.Owner, Ship.GetCopy(), Level, gun);
                    combatModel.Texture = Texture;
                    combatModel.Rotation = Rotation;
                    combatModel.Scale = Scale;
                    combatModel.Position = Position;

                    gun.Bullet.OwnerShip = Ship;

                    combatModel.UpdateCollisionBox();

                    combatModel.TeamNumber = TeamNumber;

                    Level.Objects.Add(combatModel);

                    // Spawn raider in response to player hostility
                    Level.Player.SpawnRaider(true);

                    this.RemoveFromLevel = true;
                }
            }
            else if (renderableCol != null && renderableCol.GetType() != typeof(Player))
            {
                //SlidePast(this as ICollidable, renderableCol);
                Rotation += Ship.TurnRate;

                //Level.Objects.Add(ParticleEmitterFactory.GetShreddedMetalEmitter(Position));
            }

            if (Ship.IsDead())
            {
                //Level.Objects.Add(ParticleEmitterFactory.GetLargeExplosionEmitter(GetCenter()));
                RemoveFromLevel = true;
            }
        }
    }
}
