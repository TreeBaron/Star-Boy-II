﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace Star_Boy_II
{
    public class Game : Microsoft.Xna.Framework.Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        public Level _level = new Level();
        public Camera Camera { get; set; } = new Camera();
        public GamePage GamePage { get; internal set; }

        public Game()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;

        }

        public void ResetGame()
        {
            var ogLevel = _level;
            _level = new Level();
            _level.HUD = ogLevel.HUD;
            Camera = new Camera();
            _level.Camera = Camera;
            _level.LoadContent(this, this.Window.ClientBounds, GraphicsDevice);
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            _level.Camera = Camera;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            Renderable.HitBoxTexture = new Texture2D(GraphicsDevice, 1, 1);
            Renderable.HitBoxTexture.SetData(new Color[] { Color.HotPink });

            // TODO: use this.Content to load your game content here
            _level.LoadContent(this, this.Window.ClientBounds, GraphicsDevice);

        }

        protected override void Update(GameTime gameTime)
        {
            try
            {
                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                    Exit();

                // TODO: Add your update logic here
                _level.Update(gameTime);
                Camera.Update(gameTime, this.Window.ClientBounds);

                LagManager.Update(gameTime, this);

                base.Update(gameTime);
            }
            catch(Exception e)
            {
                MessageBox.Show("Title", e.Message, new List<string> { "Okie Dokie"});
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            try
            { 
                GraphicsDevice.Clear(Color.Black);

                // TODO: Add your drawing code here
                _spriteBatch.Begin(SpriteSortMode.Immediate, transformMatrix: Camera.Transform);
                _level.Draw(_spriteBatch, this.Window.ClientBounds);
                _spriteBatch.End();

                base.Draw(gameTime);
            }
            catch (Exception e)
            {
                MessageBox.Show("Title", e.Message, new List<string> { "Okie Dokie" });
            }
        }

        public void Reset()
        {
        }
    }
}
