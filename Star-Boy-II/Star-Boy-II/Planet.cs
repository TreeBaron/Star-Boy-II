using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Star_Boy_II.Particles;
using Star_Boy_II.TextAdventureStuff;
using System;
using System.Collections.Generic;

namespace Star_Boy_II
{
    public class Planet : Renderable
    {
        public string Name { get; set; }

        public Location EntryPoint { get; set; }

        public SpaceShip Ship { get; set; }

        public Level Level { get; set; }

        public ProductionSource Storehouse { get; set; }

        public int UpgradeLevel { get; set; }

        public Dictionary<string, int> WantsToAquire { get; set; } = new Dictionary<string, int>();
        public bool HasRepairs { get; set; } = false;
        public bool HasAmmo { get; set; } = false;
        public bool HasFuel { get; set; } = false;

        public bool IsDiscoverable = true;

        public Planet(string typeName)
        {
            TypeName = typeName;
            Ship = SpaceShip.GetDefaultShip();
            Ship.MaxHealth = 50_000;
            Ship.Health = 50_000;
            Storehouse = ProductionSource.GetRandomProductionSource();
            SetLevelAndWants(1);
        }

        public void SetLevelAndWants(int level)
        {
            UpgradeLevel = level;

            if (TypeName == LocationTypes.Station && level == 1)
            {
                WantsToAquire = new Dictionary<string, int>()
                {
                    { ResourceTypes.Wheat, 200 },
                    { ResourceTypes.Steel, 200 },
                    { ResourceTypes.Electronics, 200 },
                    { ResourceTypes.Fuel, 200 },
                };
            }

            if (TypeName == LocationTypes.Station && level == 2)
            {
                WantsToAquire = new Dictionary<string, int>()
                {
                    { ResourceTypes.Wheat, 400 },
                    { ResourceTypes.Steel, 400 },
                    { ResourceTypes.Electronics, 400 },
                    { ResourceTypes.Fuel, 400 },
                };
            }

            if (TypeName == LocationTypes.Station && level == 3)
            {
                WantsToAquire = new Dictionary<string, int>()
                {
                    { ResourceTypes.Wheat, 800 },
                    { ResourceTypes.Steel, 800 },
                    { ResourceTypes.Electronics, 800 },
                    { ResourceTypes.Fuel, 800 },
                };
            }

            if (TypeName == LocationTypes.Station && level == 4)
            {
                WantsToAquire = new Dictionary<string, int>()
                {
                    { ResourceTypes.Wheat, 1600 },
                    { ResourceTypes.Steel, 1600 },
                    { ResourceTypes.Electronics, 1600 },
                    { ResourceTypes.Fuel, 1600 },
                };
            }

            if (TypeName == LocationTypes.Planet && level == 1)
            {
                WantsToAquire = new Dictionary<string, int>()
                {
                    { ResourceTypes.Wheat, 200 },
                    { ResourceTypes.Steel, 200 },
                    { ResourceTypes.Electronics, 200 },
                    { ResourceTypes.Fuel, 200 },
                };
            }

            if (TypeName == LocationTypes.Planet && level == 2)
            {
                WantsToAquire = new Dictionary<string, int>()
                {
                    { ResourceTypes.Wheat, 400 },
                    { ResourceTypes.Steel, 400 },
                    { ResourceTypes.Electronics, 400 },
                    { ResourceTypes.Fuel, 400 },
                };
            }

            if (TypeName == LocationTypes.Planet && level == 3)
            {
                WantsToAquire = new Dictionary<string, int>()
                {
                    { ResourceTypes.Wheat, 800 },
                    { ResourceTypes.Steel, 800 },
                    { ResourceTypes.Electronics, 800 },
                    { ResourceTypes.Fuel, 800 },
                };
            }

            if (TypeName == LocationTypes.Planet && level == 4)
            {
                WantsToAquire = new Dictionary<string, int>()
                {
                    { ResourceTypes.Wheat, 1600 },
                    { ResourceTypes.Steel, 1600 },
                    { ResourceTypes.Electronics, 1600 },
                    { ResourceTypes.Fuel, 1600 },
                };
            }

            if (WantsToAquire.ContainsKey(Storehouse.ProductionItem))
            {
                WantsToAquire.Remove(Storehouse.ProductionItem);
            }

        }

        public override void UpdateCollisionBox()
        {
            base.UpdateCollisionBox();
        }

        public override void Update(GameTime gameTime)
        {
            if (this.TypeName != LocationTypes.Planet)
            {
                UpdateCollisionBox();
            }

            if (AfterUpdate != null)
            {
                AfterUpdate(gameTime, this);
            }

            Storehouse.Update(gameTime);
        }

        public override void OnCollision(Object collidedWith)
        {
            if(this.TypeName == LocationTypes.Planet)
            {
                return;
            }

            ICollidable renderableCol = collidedWith as ICollidable;
            if (collidedWith.GetType() == typeof(Bullet))
            {
                var bullet = (Bullet)collidedWith;
                if (bullet.TeamNumber != TeamNumber)
                {
                    Ship.ApplyEffect(bullet.Effect);
                }
            }

            if (Ship.IsDead())
            {
                Level.Objects.Add(ParticleEmitterFactory.GetLargeExplosionEmitter(Position, "Dead space station.", TeamNumber));
                RemoveFromLevel = true;
            }
        }

        /// <summary>
        /// Perform an upgrade to the next level. Add locations, ships, stations, whatever.
        /// </summary>
        /// <param name="v"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void Upgrade(int upgradeLevel)
        {
            UpgradeLevel = upgradeLevel;

            
            if (TypeName == LocationTypes.Station)
            {
                if (upgradeLevel == 1)
                {
                    //todo: code this...
                }
            }
            else if (TypeName == LocationTypes.Planet)
            {
                if (upgradeLevel == 1)
                {
                    //todo: code this...
                }
            }
        }
    }
}