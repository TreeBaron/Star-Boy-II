﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Star_Boy_II.Particles;

namespace Star_Boy_II
{
    public class CombatAI : Renderable
    {
        public SpaceShip Ship { get; set; } = SpaceShip.GetDefaultShip();

        Level Level { get; set; }

        private Renderable Reticle { get; set; }

        private Renderable Target { get; set; }

        private Gun Gun { get; set; }

        public Renderable PowerUp { get; set; }

        public bool Turret { get; set; } = false;

        public bool SearchForTargets { get; set; } = false;

        public bool LeadTarget { get; set; } = false;

        public bool TargetAsteroids { get; set; } = false;

        public bool ImmuneToNonPlayerDamage { get; set; }

        public bool CanTargetPlayer { get; set; }

        public float EngagementDistance = MetaGameSettings.ScreenDistance * 2;

        private int CoinFlip = 0;

        /// <summary>
        /// Basically a firing cone, AI will not fire unless enemy is in the cone.
        /// (In Degrees).
        /// </summary>
        public int FiringCone = 35;

        private float VeerOffDistance = 500;

        public CombatAI(int teamNumber, Renderable target, SpaceShip ship, Level level, Gun gun)
        {
            if(MetaGameSettings.Random.NextDouble() < 0.5)
            {
                CoinFlip = 0;
            }
            else
            {
                CoinFlip = 1;
            }

            TeamNumber = teamNumber;
            Target = target;
            Level = level;

            Gun = gun;
            Ship = ship;

            PowerUp = GameFactory.GetHealthPowerUp(TeamNumber, Level, Position);

            Reticle = GameFactory.GetTargetReticle();

            Gun.Bullet.TeamNumber = TeamNumber;

            VeerOffDistance = MetaGameSettings.Random.Next(300, 1000);
        }

        public override void Draw(SpriteBatch spriteBatch, Rectangle window)
        {
            if (Target != null && Target.GetType() == typeof(Player))
            {
                Reticle.Draw(spriteBatch, window);
            }
            base.Draw(spriteBatch, window);
        }

        public override void Update(GameTime gameTime)
        {
            float throttle = 0.0f;

            #region Targeting Logic
            if (Vector2.Distance(Level.Player.Position, Position) < MetaGameSettings.ScreenDistance * 4)
            {
                if (SearchForTargets && (Target == null || Target.RemoveFromLevel == true) && Vector2.Distance(Level.Player.Position, Position) < EngagementDistance)
                {
                    var enemies = Level.Objects.Where(x => x.TeamNumber != this.TeamNumber).ToList();
                    if (enemies.Any())
                    {

                        if (Target != null && Target.RemoveFromLevel == true)
                        {
                            Target = null;
                        }

                        enemies = enemies.OrderBy(x => Vector2.Distance(Position, x.Position)).ToList();
                        enemies = enemies.Where(x => Vector2.Distance(Position, x.Position) < EngagementDistance).ToList();

                        if (Target == null && CanTargetPlayer && TeamNumber != Level.Player.TeamNumber && Vector2.Distance(Level.Player.Position, Position) < EngagementDistance)
                        {
                            Target = Level.Player;
                        }

                        if (Target == null)
                        {
                            Target = enemies.FirstOrDefault(x => x.RemoveFromLevel == false && x.GetType() == typeof(CombatAI) && ((CombatAI)x).ImmuneToNonPlayerDamage == false && Vector2.Distance(Position, x.Position) < EngagementDistance);
                        }

                        if (Target == null)
                        {
                            Target = enemies.FirstOrDefault(x => x.RemoveFromLevel == false &&x.GetType() == typeof(Bullet) && Vector2.Distance(Position, x.Position) < EngagementDistance);
                        }

                        if (Target == null && TargetAsteroids)
                        {
                            var asteroid = Level.Objects.FirstOrDefault(x => x.RemoveFromLevel == false && x.GetType() == typeof(Asteroid) && Vector2.Distance(Position, x.Position) < EngagementDistance);
                            Target = asteroid;
                        }
                    }
                }

                if(SearchForTargets && Target != null && Vector2.Distance(Target.Position, Position) > MetaGameSettings.ScreenDistance*2) 
                {
                    Target = null;
                }
            }
            else if(SearchForTargets)
            {
                Target = null;
            }
            #endregion

            Reticle.Position = Position;

            Gun.Update(gameTime);

            if (Target != null && Vector2.Distance(Level.Player.Position, Position) < EngagementDistance)
            {
                FireGunIfTargetInSights();
            }

            float wantedRotation = 0.0f;
            
            // spin in circles if no target
            if(Target == null)
            {
                throttle = 0.1f;
                if(CoinFlip == 0)
                {
                    wantedRotation = Rotation + Ship.TurnRate / 10;
                }
                else
                {
                    wantedRotation = Rotation - Ship.TurnRate / 10;
                }
            }

            // if there is a target...
            if(Target != null)
            {
                // If we're too close, wave off
                if(Vector2.Distance(Position, Target.Position) < VeerOffDistance)
                {
                    throttle = 1.0f;
                    wantedRotation = Rotation;

                    if(CoinFlip == 0)
                    {
                        wantedRotation += Ship.TurnRate / 5;
                    }
                    else
                    {
                        wantedRotation -= Ship.TurnRate / 5;
                    }
                }
                else
                {
                    wantedRotation = AngleUtility.GetAngleToPosition(GetCenter(), Target.Position);
                    throttle = 0.8f;
                }
            }

            Rotation = AngleUtility.RotationLogic(Rotation, wantedRotation, Ship.TurnRate);

            if (!Turret)
            {
                MoveTowards(Rotation, throttle * Ship.Speed, gameTime);
            }

            // simulate friction
            Velocity = new Vector2(Velocity.X * 0.9f, Velocity.Y * 0.9f);

            UpdateCollisionBox();
        }

        private void FireGunIfTargetInSights()
        {
            Gun.Position = Position;
            Gun.Rotation = Rotation;
            float rotationToTarget = AngleUtility.GetAngleToPosition(GetCenter(), Target.GetCenter());

            if(Math.Abs(rotationToTarget-Rotation) < Degrees(FiringCone) && Vector2.Distance(Position, Target.Position) < EngagementDistance / 2)
            {
                Gun.Fire(new Vector2(), Target);
            }
        }

        public override void OnCollision(Object collidedWith)
        {
            bool diedToPlayer = false;
            ICollidable renderableCol = collidedWith as ICollidable;
            if (collidedWith.GetType() == typeof(Bullet))
            {
                var bullet = (Bullet)collidedWith;
                if (bullet.TeamNumber != TeamNumber)
                {
                    if(bullet.Owner != null && bullet.Owner.GetType() == typeof(Player))
                    {
                        Ship.ApplyEffect(bullet.Effect);
                        diedToPlayer = true;
                    }
                    else if (!ImmuneToNonPlayerDamage)
                    {
                        Ship.ApplyEffect(bullet.Effect);
                    }
                }
            }

            if (Ship.IsDead())
            {
                if(diedToPlayer)
                {
                    Level.Player.KillList.Add(this);
                }

                if(PowerUp != null)
                {
                    PowerUp.Position = Position;
                    Level.Objects.Add(PowerUp);
                }
                Level.Objects.Add(ParticleEmitterFactory.GetLargeExplosionEmitter(Position, "Dead ship.", TeamNumber));
                RemoveFromLevel = true;
            }
        }
    }
}
