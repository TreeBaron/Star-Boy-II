using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using SharpDX.WIC;
using Star_Boy_II.Particles;
using Star_Boy_II.TextAdventureStuff;

namespace Star_Boy_II
{
    public class Level
    {
        public Viewport LevelBounds { get; set; } = new Viewport(0, 0, 50_000, 50_000);

        public Camera Camera { get; set; }

        public Player Player { get; set; }

        public List<Renderable> Objects { get; set; } = new List<Renderable>();

        public List<Vector2> RoadWaypoints { get; set; } = new List<Vector2>() { new Vector2(0, 5_000) };

        public Game Game { get; set; }

        public bool Paused { get; set; } = false;

        public InputManager InputManager { get; set; }

        /// <summary>
        /// XAML Interface object. Sety externally and modified typically by the player object.
        /// </summary>
        public HUDViewModel HUD { get; set; }

        public CommandContext CommandContext { get; set; }

        public List<Planet> MappedPlanets { get; set; } = new List<Planet>();

        public List<string> Textures = new List<string>()
        {
            "SpaceshipSprite",
            "StarSprite",
            "TorpedoSprite",
            "RedLaserSprite",
            "ExplosionSprite",
            "ExplosionSprite2",
            "BeaconSprite",
            "BeaconSpriteRed",
            "DeadEarthSprite",
            "PortalSprite",
            "RepairStationSprite",
            "AmmoStationSprite",
            "TradeStationSprite",
            "EvilSpaceStation",
            "FuelStationSprite",
            "CargoShipSprite",
            "CargoShipSprite2",
            "CargoShipSprite3",
            "CargoShipSprite4",
            "CargoShipSprite5",
            "EnemyShipSprite",
            "EnemyShipSprite2",
            "EnemyShipSprite3",
            "EnemyShipSprite4",
            "TurretSprite",
            "EnemyTurretSprite",
            "SmokeSprite",
            "ShreddedMetalSprite",
            "SparkSprite",
            "LazerSprite",
            "BombAsteroidSprite",
            "FuelAsteroidSprite",
            "MoneyAsteroidSprite",
            "FuelPowerUpSprite",
            "MoneyPowerUpSprite",
            "TargetMarkerSprite",
            "HealthPowerUp",
            "AmmoPowerUp",
            "HealthSatelliteSprite",
            "EscapePodSprite",
            "CosmonautParticle",
            "RedStarSprite",
            "BlueStarSprite",
            "PirateFighter1",
            "PirateFighter2",
            "FriendlyFighter1"
        };

        public List<string> SoundEffects = new List<string>()
        {
            "explosion",
            "pew"
        };

        public Dictionary<string, Texture2D> TextureDictionary = new Dictionary<string, Texture2D>();

        public Dictionary<string, SoundEffect> SoundEffectDictionary = new Dictionary<string, SoundEffect>();

        public void LoadContent(Game game, Rectangle window, GraphicsDevice graphicsDevice)
        {
            GameFactory.GraphicsDevice = graphicsDevice;
            
            Game = game;

            // use game.Content to load your game content here
            foreach (var textureName in Textures)
            {
                var result = game.Content.Load<Texture2D>(textureName);
                TextureDictionary.Add(textureName, result);
            }

            foreach(var soundEffect in SoundEffects)
            {
                var result = game.Content.Load<SoundEffect>(soundEffect);
                SoundEffectDictionary.Add(soundEffect, result);
            }

            // turn down volume
            SoundEffect.MasterVolume = 0.01f;

            // give factories texture access
            ParticleEmitterFactory.TextureDictionary = TextureDictionary;
            GameFactory.TextureDictionary = TextureDictionary;
            GameFactory.SoundDictionary = SoundEffectDictionary;

            // Initialize Player gun
            var playerGun1 = GameFactory.GetPlayerGun(0, this, 0.25);

            // Initialize Player
            Player player = new Player();
            Player = player;

            AddFriendlyMappedPlanets(player);
            AddEnemyMappedPlanets(player);

            player.Initialize(game, Camera, TextureDictionary);
            player.Guns.Add(playerGun1);
            Objects.Add(player);

            //set player reticle marker
            Renderable MouseMarker = new Renderable();
            MouseMarker.Texture = TextureDictionary["TargetMarkerSprite"];
            MouseMarker.Scale = 0.15f;
            player.TargetReticle = MouseMarker;

            // set guns owner
            playerGun1.Bullet.Owner = player;
            playerGun1.Bullet.OwnerShip = player.Ship;

            // set is player
            player.Ship.IsPlayer = true;

            // Initialize Star Handler
            StarHandler starHandler = new StarHandler();
            starHandler.Initialize(game, TextureDictionary, player, window);
            Objects.Insert(0, starHandler);


            AddAsteroids(10);
            AddSpaceTraffic(150);
            AddRandomPlanets(40);

            player.Position += new Vector2(1000, 0);

            AddEnemyFighters(12, player.Position);
            AddFriendlyFighters(12, player.Position);

        }

        private void AddFriendlyMappedPlanets(Player player)
        {
            var planets = new Dictionary<string, Vector2>()
            {
                { "STRIFE", new Vector2(2208, 2130) },
                { "ELBIN", new Vector2(3475, 1805) },
                { "TITAN", new Vector2(1343, 2883) },
                { "FIT", new Vector2(448, 3572) },
                { "CYPHRON", new Vector2(2153, 3603) },
                { "CENTAURI", new Vector2(3330, 3544) },
                { "URTH", new Vector2(2649, 2712)},
            };

            // subtract all coordinates by -2000, -2000
            // scale to map
            for(int i = 0; i < planets.Count; i++)
            {
                var key = planets.Keys.ElementAt(i);
                var value = planets[key];
                planets[key] += new Vector2(-2000,-2000);
                planets[key] = Vector2.Multiply(planets[key], 25);

                var planet = GameFactory.GenerateMappedPlanet(this, key, value);
                planet.Name = key;

                planet.AfterUpdate = (GameTime gt, Renderable urthBoi) =>
                {
                    var smallDegree = AngleUtility.DegreesToRadians(0.01f);
                    urthBoi.Rotation += smallDegree;
                };

                planet.IsDiscoverable = false;

                Objects.Add(planet);

                MappedPlanets.Add(planet);

                if(key == "URTH")
                {
                    AddStations(planet, true, true, true, true, true);
                    player.Position = planet.Position;

                }
                else if (key == "NEBULON")
                {
                    AddStations(planet, true, true, true, true, true);
                }
                else
                {
                    var r = MetaGameSettings.Random;
                    AddStations(planet, (r.NextDouble() > 0.5), (r.NextDouble() > 0.5), (r.NextDouble() > 0.5), true, (r.NextDouble() > 0.5));
                }
            }
        }

        private void AddEnemyMappedPlanets(Player player)
        {
            var planets = new Dictionary<string, Vector2>()
            {
                { "NAY", new Vector2(2276, 281)},
                { "NEXAR", new Vector2(667, 763)},
                { "NEBULON", new Vector2(1493, 1367)},
                { "CESTUS", new Vector2(502, 2112) },
                { "LEXON", new Vector2(2038, 1975) },
                { "PIKE", new Vector2(3170, 1396) },
            };

            // subtract all coordinates by -2000, -2000
            // scale to map
            for (int i = 0; i < planets.Count; i++)
            {
                var key = planets.Keys.ElementAt(i);
                var value = planets[key];
                planets[key] += new Vector2(-2000, -2000);
                planets[key] = Vector2.Multiply(planets[key], 25);

                var planet = GameFactory.GenerateMappedPlanet(this, key, value);
                planet.Name = key;

                planet.TeamNumber = 1;

                planet.AfterUpdate = (GameTime gt, Renderable urthBoi) =>
                {
                    var smallDegree = AngleUtility.DegreesToRadians(0.01f);
                    urthBoi.Rotation -= smallDegree;
                };

                planet.IsDiscoverable = false;

                Objects.Add(planet);

                AddEnemyStation(planet);

                MappedPlanets.Add(planet);

            }
        }

        private void AddEnemyFighters(int count, Vector2 position)
        {
            for (int i = 0; i < count; i++)
            {
                var r = MetaGameSettings.Random;
                var fighter = GameFactory.GetEnemyFighter(this, null);
                fighter.Position = position;
                fighter.Position += new Vector2(r.Next(-3000, 3000), r.Next(-3000, 3000));
                fighter.SearchForTargets = true;
                Objects.Add(fighter);
            }
        }

        private void AddFriendlyFighters(int count, Vector2 position)
        {
            for (int i = 0; i < count; i++)
            {
                var r = MetaGameSettings.Random;
                var fighter = GameFactory.GetFriendlyFighter(this, null);
                fighter.Position = position;
                fighter.Position += new Vector2(r.Next(-3000, 3000), r.Next(-3000, 3000));
                fighter.SearchForTargets = true;
                Objects.Add(fighter);
            }
        }

        private void AddAsteroids(int count, Vector2? position = null)
        {
            // create asteroids...
            for (int i = 0; i < 50; i++)
            {
                var asteroid = GameFactory.GetRandomAsteroid(this);
                Objects.Add(asteroid);

                if(position != null)
                {
                    asteroid.Position = (Vector2)position;
                }
            }
        }

        private void AddEnemyStation(Planet planet)
        {
            var planetPosition = planet.Position;
            var enemyStation = GameFactory.GetEnemyStation();

            enemyStation.Position = planetPosition;

            enemyStation.Position += new Vector2(1000, 0);

            Objects.Add(enemyStation);

            AddEnemySideTurrets(enemyStation.Position);
        }

        private void AddStations(Planet planet, bool addRepairStation, bool addAmmoStation, bool addFuelStation, bool addTradeStation, bool addSideTurrets)
        {
            var planetPosition = planet.Position;
            var tradeStation = GameFactory.GetTradeStation(addRepairStation, addAmmoStation, addFuelStation);

            tradeStation.Position = planetPosition;

            tradeStation.Position += new Vector2(-2000, 0);

            tradeStation.Name = planet.Name + " Trade Station";

            if (addTradeStation)
            {
                Objects.Add(tradeStation);

                if(addSideTurrets)
                {
                    AddSideTurrets(tradeStation.Position);
                }
            }
        }

        private void AddRandomPlanets(int count)
        {
            // generate starting planets...
            for (int i = 0; i < count; i++)
            {
                var stuff = GameFactory.GeneratePlanetStuff(this);
                Objects.AddRange(stuff);
            }
        }

        private void AddSpaceTraffic(int count)
        {
            // Create space traffic
            for (int i = 0; i < count; i++)
            {
                var r = MetaGameSettings.Random;
                var ai = GameFactory.GetTrafficAI(this, false);
                ai.Position = new Vector2(r.Next(-MetaGameSettings.MapWidth, MetaGameSettings.MapWidth), r.Next(-MetaGameSettings.MapWidth, MetaGameSettings.MapWidth));
                Objects.Add(ai);
            }

        }

        private void AddEnemySideTurrets(Vector2 position)
        {
            var turret1 = GameFactory.GetEnemyTurret(this, null);
            var turret2 = GameFactory.GetEnemyTurret(this, null);

            var offset = 400;
            turret1.Position = position + new Vector2(offset, 0);
            turret2.Position = position + new Vector2(-offset, 0);

            Objects.Add(turret1);
            Objects.Add(turret2);
        }

        private void AddSideTurrets(Vector2 position)
        {
            var turret1 = GameFactory.GetFriendlyTurret(this, null);
            var turret2 = GameFactory.GetFriendlyTurret(this, null);

            var offset = 400;
            turret1.Position = position + new Vector2(offset,0);
            turret2.Position = position + new Vector2(-offset, 0);

            turret2.LeadTarget = true;

            Objects.Add(turret1);
            Objects.Add(turret2);
        }

        public void Draw(SpriteBatch spriteBatch, Rectangle window)
        {
           // player always on top
           Objects = Objects.OrderBy(x =>
           {
               if (x.GetType() == typeof(Player))
               {
                   return 1;
               }
               return 0;
           }).ToList();

           for(int i = 0; i < Objects.Count; i++)
           {
               var item = Objects[i];
               item.Draw(spriteBatch, window);
           }
        }

        public void Update(GameTime gameTime)
        {
            Player.UIUpdate();

            if(InputManager != null)
            {
                InputManager.Update(gameTime);
            }

            if (!Paused)
            {
                var itemsToRemove = new List<Renderable>();
                for (int i = 0; i < Objects.Count; i++)
                {
                    var item = Objects[i];

                    if (item.RemoveFromLevel)
                    {
                        itemsToRemove.Add(item);
                    }
                    else
                    {
                        item.Update(gameTime);
                    }
                }

                foreach (var item in itemsToRemove)
                {
                    Objects.Remove(item);
                }

                CheckCollisions();
            }
        }

        private Rectangle GetViewPosition(Rectangle rectangle)
        {
            // use player position to calculate camera viewing rectangle and adjust accordingly
            var xAdjust = Camera.Center.X - (rectangle.Width / 2);
            var yAdjust = Camera.Center.Y - (rectangle.Height / 2);
            var newRect = new Rectangle((int)xAdjust, (int)yAdjust, rectangle.Width, rectangle.Height);
            return newRect;
        }

        private void CheckCollisions()
        {
            //var collisions = Objects.Where(x => { ICollidable variable = x as ICollidable; return variable != null; }).ToList();
            var collisions = new List<Renderable>();
            for (int i = 0; i < Objects.Count; i++)
            {
                if (Vector2.Distance(Objects[i].Position, Camera.CamerasDesiredPosition) < MetaGameSettings.ScreenDistance)
                {
                    collisions.Add(Objects[i]);
                }
            }    

            for(int i = 0; i < collisions.Count; i++)
            {
                var collision1 = collisions[i];
                var check1 = (ICollidable)collision1;

                if (check1.CollisionBox != null)
                {
                    var position = new Vector2(check1.CollisionBox.Position.X, check1.CollisionBox.Position.Y);

                        for (int x = 0; x < collisions.Count; x++)
                        {
                            var collision2 = collisions[x];
                            if (collision1 == collision2 || (collision1 as ICollidable).TeamNumber == (collision2 as ICollidable).TeamNumber) continue;

                            // if it's in camera view...
                            var col1 = (ICollidable)collision1;
                            var col2 = (ICollidable)collision2;

                            var shouldSkip = col1.GetType().Name == col2.GetType().Name && col1.GetType().Name == nameof(Asteroid);

                            // check real collision...
                            if (!shouldSkip && col2.CollisionBox != null && col1.CollisionBox.Intersects(col2.CollisionBox))
                            {
                                col1.OnCollision(col2);
                                col2.OnCollision(col1);
                            }
                        
                        }
                }
            }
        }
    }
}
