﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Star_Boy_II
{
    public class SimpleTimer
    {
        public double MillisecondsElapsed = 0;
        public double TimerLengthInMilliseconds = 0;
        public bool Done = false;

        public SimpleTimer(double seconds)
        {
            TimerLengthInMilliseconds = seconds * 1_000.0;
        }

        public void Update(GameTime gameTime)
        {
            MillisecondsElapsed += gameTime.ElapsedGameTime.TotalMilliseconds;

            if (MillisecondsElapsed > TimerLengthInMilliseconds)
            {
                Done = true;
            }

        }

        public void Reset()
        {
            Done = false;
            MillisecondsElapsed = 0.0;
        }
    }
}
