﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Star_Boy_II.Particles
{
    public class ParticleEmitter : Renderable
    {
        public string Description { get; set; } = "Creator not specified.";

        public int ParticlesPerSecond { get; set; } = 0;

        public bool Shrink { get; set; } = true;

        public float ShrinkRate { get; set; } = 0.9f;

        private SimpleTimer TenthOfSecondTimer { get; set; }

        private double DivisionTracker = 0;

        private SimpleTimer ExistenceTime { get; set; } = new SimpleTimer(2);

        private Particle OriginalParticle { get; set; }

        private Random R = new Random();

        public int MaxVelocity { get; set; } = 100;

        public bool RandomVelocity { get; set; } = true;

        public bool RandomRotation { get; set; } = true;

        public Level Level { get; set; }

        List<Particle> Particles { get; set; } = new List<Particle>();

        public ParticleEmitter(string description, Particle particle, Vector2 position, double emitTime, int amount)
        {
            Description = description;
            Position = position;
            OriginalParticle = particle;
            for(int i = 0; i < amount; i++)
            {
                var newParticle = OriginalParticle.GetCopy();
                newParticle.TypeName = "Particle";
                newParticle.Position = Position;

                if (RandomVelocity)
                {
                    var randomX = R.Next(-1 * MaxVelocity, MaxVelocity + 1) * R.NextDouble();
                    var randomY = R.Next(-1 * MaxVelocity, MaxVelocity + 1) * R.NextDouble();
                    newParticle.Velocity = new Vector2((float)randomX, (float)randomY);
                }

                if (RandomRotation)
                {
                    newParticle.Rotation = (float)((Math.PI * 2.0) * R.NextDouble());
                }

                Particles.Add(newParticle);
            }

            ExistenceTime = new SimpleTimer(emitTime);

            TenthOfSecondTimer = new SimpleTimer(0.1);
        }

        public override void Draw(SpriteBatch spriteBatch, Rectangle window)
        {
            foreach(var particle in Particles)
            {
                particle.Draw(spriteBatch, window);
            }
        }

        public override void Update(GameTime gameTime)
        {
            foreach(var particle in Particles)
            {
                particle.Update(gameTime);

                if(Shrink)
                {
                    particle.Scale = particle.Scale * ShrinkRate;
                }
            }

            TenthOfSecondTimer.Update(gameTime);
            ExistenceTime.Update(gameTime);

            if(TenthOfSecondTimer.Done)
            {
                TenthOfSecondTimer.Reset();
                DivisionTracker += 0.1;
                int runAmount = (int)((double)ParticlesPerSecond * DivisionTracker);

                if (runAmount > 0) DivisionTracker = 0;

                for (int i = 0; i < runAmount; i++)
                {
                    var particle = OriginalParticle.GetCopy();
                    particle.Position = Position;

                    if(RandomVelocity)
                    {
                        var randomX = R.Next(-1 * MaxVelocity, MaxVelocity + 1) * R.NextDouble();
                        var randomY = R.Next(-1 * MaxVelocity, MaxVelocity + 1) * R.NextDouble();
                        particle.Velocity = new Vector2((float)randomX, (float)randomY);
                    }

                    if(RandomRotation)
                    {
                        particle.Rotation = (float)((Math.PI * 2.0) * R.NextDouble());
                    }
                }
            }

            if(ExistenceTime.Done)
            {
                foreach(var particle in Particles)
                {
                    particle.RemoveFromLevel = true;
                }
                this.RemoveFromLevel = true;
            }
        }
    }
}
