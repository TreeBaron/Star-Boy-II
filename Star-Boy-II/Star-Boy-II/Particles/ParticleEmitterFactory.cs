﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Star_Boy_II.Particles
{
    public static class ParticleEmitterFactory
    {
        public static Dictionary<string, Texture2D> TextureDictionary { get; set; }

        /// <summary>
        /// Returns a particle emitter that generates an explosion.
        /// </summary>
        /// <returns><see cref="ParticleEmitter"/></returns>
        public static ParticleEmitter GetExplosionEmitter(Vector2 position, string description)
        {
            Particle explosionParticle = new Particle();
            explosionParticle.Texture = TextureDictionary["ExplosionSprite"];

            ParticleEmitter emitter = new ParticleEmitter(description, explosionParticle, position, 1, 3);
            emitter.RandomRotation = true;
            emitter.RandomVelocity = true;
            emitter.MaxVelocity = 100;
            emitter.Shrink = true;

            return emitter;
        }

        /// <summary>
        /// Returns a particle emitter that generates an explosion.
        /// </summary>
        /// <returns><see cref="ParticleEmitter"/></returns>
        public static ParticleEmitter GetLargeExplosionEmitter(Vector2 position, string description, int teamNumber)
        {
            Particle explosionParticle = new Particle();

            if (teamNumber == 0)
            {
                explosionParticle.Texture = TextureDictionary["ExplosionSprite"];
            }
            else
            {
                explosionParticle.Texture = TextureDictionary["ExplosionSprite2"];
            }
            explosionParticle.Scale = 2.0f;

            ParticleEmitter emitter = new ParticleEmitter(description, explosionParticle, position, 10, 3);
            emitter.RandomRotation = true;
            emitter.RandomVelocity = true;
            emitter.MaxVelocity = 350;
            emitter.ShrinkRate = 0.96f;
            emitter.Shrink = true;

            return emitter;
        }

        /// <summary>
        /// Returns a particle emitter that generates an explosion.
        /// </summary>
        /// <returns><see cref="ParticleEmitter"/></returns>
        public static ParticleEmitter GetFlameEmitter(Vector2 position, string description)
        {
            Particle explosionParticle = new Particle();
            explosionParticle.Texture = TextureDictionary["ExplosionSprite"];
            explosionParticle.Scale = 0.2f;

            ParticleEmitter emitter = new ParticleEmitter(description, explosionParticle, position, 2, 1);
            emitter.RandomRotation = true;
            emitter.RandomVelocity = true;
            emitter.MaxVelocity = 200;
            emitter.Shrink = true;

            return emitter;
        }

        /// <summary>
        /// Returns a particle emitter that generates an explosion.
        /// </summary>
        /// <returns><see cref="ParticleEmitter"/></returns>
        public static ParticleEmitter GetPlayerShipFlameEmitter(Vector2 position, string description)
        {
            Particle explosionParticle = new Particle();
            explosionParticle.Texture = TextureDictionary["ExplosionSprite"];
            explosionParticle.Scale = 0.2f;

            ParticleEmitter emitter = new ParticleEmitter(description, explosionParticle, position, 1, 1);
            emitter.RandomRotation = true;
            emitter.RandomVelocity = false;
            emitter.ShrinkRate = 0.9f;
            emitter.Shrink = true;

            return emitter;
        }

        /// <summary>
        /// Returns a particle emitter that generates an explosion.
        /// </summary>
        /// <returns><see cref="ParticleEmitter"/></returns>
        public static ParticleEmitter GetShreddedMetalEmitter(Vector2 position, string description)
        {
            Particle explosionParticle = new Particle();
            explosionParticle.Texture = TextureDictionary["ShreddedMetalSprite"];
            explosionParticle.Scale = 0.15f;

            ParticleEmitter emitter = new ParticleEmitter(description, explosionParticle, position, 5, 1);
            emitter.RandomRotation = true;
            emitter.RandomVelocity = true;
            emitter.MaxVelocity = 1000;
            emitter.Shrink = true;
            emitter.ShrinkRate = 0.99f;

            return emitter;
        }

        public static ParticleEmitter GetCrewEmitter(Vector2 position, string description)
        {
            Particle explosionParticle = new Particle();
            explosionParticle.Texture = TextureDictionary["CosmonautParticle"];
            explosionParticle.Scale = 0.15f;

            ParticleEmitter emitter = new ParticleEmitter(description, explosionParticle, position, 5, 5);
            emitter.RandomRotation = true;
            emitter.RandomVelocity = true;
            emitter.MaxVelocity = 1000;
            emitter.Shrink = false;
            emitter.ShrinkRate = emitter.ShrinkRate / 4.0f;

            return emitter;
        }

        /// <summary>
        /// Returns a particle emitter that generates an explosion.
        /// </summary>
        /// <returns><see cref="ParticleEmitter"/></returns>
        public static ParticleEmitter GetSmokeEmitter(Vector2 position, string description)
        {
            Particle explosionParticle = new Particle();
            explosionParticle.Texture = TextureDictionary["SmokeSprite"];
            explosionParticle.Scale = 0.1f;

            ParticleEmitter emitter = new ParticleEmitter(description, explosionParticle, position, 1.5, 1);
            emitter.RandomRotation = true;
            emitter.RandomVelocity = true;
            emitter.MaxVelocity = 400;
            emitter.Shrink = true;
            emitter.ShrinkRate = 0.95f;

            return emitter;
        }
    }
}
