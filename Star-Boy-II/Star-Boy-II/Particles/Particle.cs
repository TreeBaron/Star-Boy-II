﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Star_Boy_II.Particles
{
    public class Particle : Renderable
    {
        public SimpleTimer DeathTimer { get; set; } = new SimpleTimer(3);

        public bool HasFriction { get; set; } = false;

        public override void Update(GameTime gameTime)
        {
            if (DeathTimer != null)
            {
                DeathTimer.Update(gameTime);
            }

            if (DeathTimer != null && DeathTimer.Done)
            {
                RemoveFromLevel = true;
            }

            // handle velocity
            Position += Velocity * (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (HasFriction)
            {
                // simulate friction
                Velocity = new Vector2(Velocity.X * 0.99f, Velocity.Y * 0.99f);
            }
        }

        public new Particle GetCopy()
        {
            Particle p = (Particle)this.MemberwiseClone();
            p.DeathTimer = new SimpleTimer(this.DeathTimer.TimerLengthInMilliseconds / 1_000);
            p.Texture = Texture;
            p.Rotation = Rotation;
            p.Scale = Scale;
            p.Velocity = Velocity;
            p.Position = Position;
            return p;
        }

        public override void UpdateCollisionBox()
        {
            // GNDN
        }

        public override void OnCollision(object collidedWith)
        {
            // GNDN
        }
    }
}
