﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Star_Boy_II
{
    public class Gun : Renderable
    {
        public Bullet Bullet { get; set; }

        public SimpleTimer Timer { get; set; }

        private double TimeBetweenFiring { get; set; } = 1;

        private bool CanFire { get; set; } = true;

        public Level Level { get; set; }

        public Gun(double timeBetweenFiring)
        {
            TimeBetweenFiring = timeBetweenFiring;
        }

        public override void Update(GameTime gameTime)
        {
            Timer.Update(gameTime);

            if(Timer.Done)
            {
                Timer.Reset();
                Timer.TimerLengthInMilliseconds = 1_000 * TimeBetweenFiring;
                CanFire = true;
            }
        }

        public bool Fire(Vector2 startVelocity, Renderable target = null)
        {
            if(CanFire)
            {
                var bullet = Bullet.GetCopy();

                // set bullet velocity in direction of our rotation
                Vector2 directionOfTravel = new Vector2((float)Math.Cos(Rotation), (float)Math.Sin(Rotation));
                directionOfTravel.Normalize();

                bullet.Velocity = startVelocity;
                bullet.Velocity += directionOfTravel * bullet.Speed;
                bullet.Position = Position;
                bullet.Rotation = Rotation;

                if (target != null)
                {
                    bullet.Target = target;
                }

                Level.Objects.Add(bullet);
                CanFire = false;
                return true;
            }

            return false;
        }
    }
}
