﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Star_Boy_II
{
    public class Camera
    {
        public Vector2 CamerasDesiredPosition { get; set; } = new Vector2(0, 0);

        public Matrix Transform { get; set; }

        private Viewport View { get; set; }

        private Vector2 Position { get; set; }

        public float Rotation { get; set; }

        public bool RotationEnabled { get; set; } = false;

        public float ZoomLevel { get; set; } = 1.0f;

        public float MaxZoom { get; set; } = 50.0f;

        public float MinZoom { get; set; } = 0.01f;

        public Vector2 Center { get; private set; }

        private int LastScrollValue { get; set; }

        public void Update(GameTime gameTime, Rectangle window)
        {
            var scrollValue = Mouse.GetState().ScrollWheelValue;
            float zoom = 0.05f;
            if (scrollValue > LastScrollValue)
            {
                ZoomLevel += zoom;
            }
            else if (scrollValue < LastScrollValue)
            {
                ZoomLevel -= zoom;
            }

            LastScrollValue = scrollValue;

            if (ZoomLevel > MaxZoom)
            {
                ZoomLevel = MaxZoom;
            }

            if (ZoomLevel < MinZoom)
            {
                ZoomLevel = MinZoom;
            }

            Position = new Vector2((CamerasDesiredPosition.X - View.Width / 2f), (CamerasDesiredPosition.Y - View.Height / 2f));

            var center = CamerasDesiredPosition;

            Center = center;

            if (RotationEnabled == false)
            {
                Rotation = (float)0;// (Math.PI);
            }

            Transform = 
                Matrix.CreateTranslation(new Vector3(-center.X, -center.Y, 0)) *
                Matrix.CreateRotationZ(Rotation) *
                Matrix.CreateScale(new Vector3(ZoomLevel, ZoomLevel, 1f)) *
                Matrix.CreateTranslation(new Vector3(window.Width/2, window.Height /2, 0));


        }

        public Rectangle GetMaxViewPort()
        {
            //Mod boi is a simulation of mouse movement scaler at max zoom
            //you don't know what you're doing so don't touch this.
            double ModBoi = MaxZoom / this.ZoomLevel;
            ModBoi = ModBoi / MaxZoom;
            ModBoi *= 1.2;

            int Width = (int)((View.Width * ModBoi) / 2);
            int Height = (int)((View.Height * ModBoi) / 2);
            Rectangle R = new Rectangle((int)Position.X - Width, (int)Position.Y - Height, (int)(View.Width * ModBoi), (int)(View.Height * ModBoi));
            return R;
        }
    }
}

