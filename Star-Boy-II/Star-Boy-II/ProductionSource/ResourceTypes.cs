﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Star_Boy_II
{
    public static class ResourceTypes
    {
        public const string Wheat = "Wheat";

        public const string Steel = "Steel";

        public const string Electronics = "Electronics";

        public const string Fuel = "Fuel";
    }
}
