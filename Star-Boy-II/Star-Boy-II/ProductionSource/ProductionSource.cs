﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Star_Boy_II
{
    public class ProductionSource : Renderable
    {
        public string ProductionItem { get; private set; }

        public int Amount { get; private set; }

        private SimpleTimer ProductionRate;

        public static ProductionSource GetRandomProductionSource()
        {
            return new ProductionSource(GetRandomResource(), 3);
        }

        public static string GetRandomResource()
        {
            Random r = new Random();

            List<string> items = new List<string>()
            {
                ResourceTypes.Wheat,
                ResourceTypes.Steel,
                ResourceTypes.Fuel,
                ResourceTypes.Electronics
            };
            return items[r.Next(0, items.Count)];
        }

        public ProductionSource(string productionItem, double productionRate)
        {
            ProductionItem = productionItem;
            ProductionRate = new SimpleTimer(productionRate);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            ProductionRate.Update(gameTime);

            if(ProductionRate.Done)
            {
                Amount += 1;
                ProductionRate.Reset();
            }
        }

        public (string, int) Take(int amount)
        {
            if(amount > Amount)
            {
                throw new Exception("Requested more from ProductionSource than is available.");
            }

            Amount = Amount - amount;
            return (ProductionItem, amount);
        }
    }
}
