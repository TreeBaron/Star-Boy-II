using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SharpDX.Direct2D1.Effects;
using System;

namespace Star_Boy_II
{
    public class GoKillMission : IMission
    {
        private Renderable ThingToKill { get; set; }

        private Texture2D PointerTexture { get; set; }

        private Player Player { get; set; }

        private int MoneyToReward { get; set; }
        public Action OnCompleted { get; set; }

        public GoKillMission(Renderable thingToKill, Texture2D pointerTexture, Player player, int monetaryReward)
        {
            PointerTexture = pointerTexture;
            Player = player;
            MoneyToReward = monetaryReward;
            ThingToKill = thingToKill;
        }

        void IMission.Update(GameTime gameTime)
        {
            //GNDN
        }

        void IMission.Draw(SpriteBatch spriteBatch)
        {
            var rotation = AngleUtility.GetAngleToPosition(Player.GetCenter(), ThingToKill.Position);

            Vector2 directionOfTravel = new Vector2((float)Math.Cos(rotation), (float)Math.Sin(rotation));
            directionOfTravel.Normalize();
            directionOfTravel += directionOfTravel * 400;

            var scale = 0.25f;
            var width = PointerTexture.Width * scale;
            var height = PointerTexture.Height * scale;

            spriteBatch.Draw(PointerTexture, Player.Position + directionOfTravel, null, Color.White, rotation, new Vector2(width/2 , height/2), scale, SpriteEffects.None, 0); //Draw with rotation!
        }

        bool IMission.IsComplete(Level level, Player player)
        {
            if (player.KillList.Contains(ThingToKill))
            {
                player.Ship.ApplyEffect(new SpaceShip
                {
                    Money = MoneyToReward,
                });

                if (OnCompleted != null)
                {
                    OnCompleted();
                }

                return true;
            }

            return false;
        }
    }
}