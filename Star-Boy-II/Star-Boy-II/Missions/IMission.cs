using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Star_Boy_II
{
    public interface IMission
    {
        void Update(GameTime gameTime);

        void Draw(SpriteBatch spriteBatch);

        bool IsComplete(Level level, Player player);

        Action OnCompleted { get; set; }
    }
}