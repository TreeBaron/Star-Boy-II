using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SharpDX.Direct2D1.Effects;
using System;

namespace Star_Boy_II
{
    public class GoToMission : IMission
    {
        private Vector2 PositionToGoTo { get; set; }

        private Texture2D PointerTexture { get; set; }

        private Player Player { get; set; }

        private float DistanceToAccept { get; set; } = 250f;

        private int MoneyToReward { get; set; }
        public Action OnCompleted { get; set; }

        public GoToMission(Vector2 positionToGoTo, Texture2D pointerTexture, Player player, int monetaryReward)
        {
            PositionToGoTo = positionToGoTo;
            PointerTexture = pointerTexture;
            Player = player;
            MoneyToReward = monetaryReward;
        }

        void IMission.Update(GameTime gameTime)
        {
            //GNDN
        }

        void IMission.Draw(SpriteBatch spriteBatch)
        {
            var rotation = AngleUtility.GetAngleToPosition(Player.GetCenter(), PositionToGoTo);

            Vector2 directionOfTravel = new Vector2((float)Math.Cos(rotation), (float)Math.Sin(rotation));
            directionOfTravel.Normalize();
            directionOfTravel += directionOfTravel * 400;

            var scale = 0.25f;
            var width = PointerTexture.Width * scale;
            var height = PointerTexture.Height * scale;
            var position = Player.Position - new Vector2(width/2,height / 2);

            spriteBatch.Draw(PointerTexture, Player.Position + directionOfTravel, null, Color.White, rotation, new Vector2(width/2 , height/2), scale, SpriteEffects.None, 0); //Draw with rotation!
        }

        bool IMission.IsComplete(Level level, Player player)
        {
            if (Vector2.Distance(player.GetCenter(), PositionToGoTo) <= DistanceToAccept)
            {
                player.Ship.ApplyEffect(new SpaceShip
                {
                    Money = MoneyToReward,
                });

                if (OnCompleted != null)
                {
                    OnCompleted();
                }

                return true;
            }

            return false;
        }
    }
}