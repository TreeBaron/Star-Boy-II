using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SharpDX.Direct2D1.Effects;
using System;

namespace Star_Boy_II
{
    public class ScanPlanetMission : IMission
    {
        private Player Player { get; set; }

        private int MoneyToReward { get; set; }

        public Action OnCompleted { get; set; }

        public Planet PlanetToScan { get; set; }

        public ScanPlanetMission(Planet planetToScan, Player player, int monetaryReward)
        {
            Player = player;
            MoneyToReward = monetaryReward;
            PlanetToScan = planetToScan;
        }

        void IMission.Update(GameTime gameTime)
        {
            //GNDN
        }

        void IMission.Draw(SpriteBatch spriteBatch)
        {
            //GNDN
        }

        bool IMission.IsComplete(Level level, Player player)
        {
           if(player.ScannedPlanets.Contains(PlanetToScan))
           {
                if(OnCompleted != null)
                {
                    OnCompleted();
                }

                player.Ship.Money += MoneyToReward;

                return true;
           }

            return false;
        }
    }
}