﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Star_Boy_II.Missions
{
    public class Quest : IMission
    {
        public string Name { get; set; }

        public Action OnCompleted { get; set; } 

        /// <summary>
        /// Pending objectives.
        /// </summary>
        public List<IMission> Objectives { get; set; } = new List<IMission>();

        List<IMission> CompletedObjectives { get; set; } = new List<IMission>();

        public bool Completed { get; set; } = false;
        public string TypeName { get; set; }

        public void Draw(SpriteBatch spriteBatch)
        {
            Objectives.First().Draw(spriteBatch);
        }

        public bool IsComplete(Level level, Player player)
        {
            var currentObjective = Objectives.First();
            var complete = currentObjective.IsComplete(level, player);
            if (complete)
            {
                CompletedObjectives.Add(currentObjective);
            }

            Objectives = Objectives.Where(x => !CompletedObjectives.Contains(x)).ToList();

            if(Objectives.Count == 0)
            {
                if(OnCompleted != null)
                {
                    OnCompleted();
                }
                Completed = true;
                return true;
            }

            return false;
        }

        public void Update(GameTime gameTime)
        {
            Objectives.First().Update(gameTime);
        }
    }
}
