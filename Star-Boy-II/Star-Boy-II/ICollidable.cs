﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Star_Boy_II
{
    public interface ICollidable
    {
        void OnCollision(Object collidedWith);

        void UpdateCollisionBox();

        CollisionTracker CollisionBox { get; set; }

        int TeamNumber { get; set; }
    }
}
