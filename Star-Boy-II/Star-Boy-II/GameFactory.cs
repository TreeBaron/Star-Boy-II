﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Star_Boy_II.TextAdventureStuff;
using Windows.UI.Xaml.Shapes;

namespace Star_Boy_II
{
    public static class GameFactory
    {
        public static Dictionary<string, Texture2D> TextureDictionary { get; set; }

        public static Random Random = new Random();

        public static Dictionary<string, SoundEffect> SoundDictionary { get; set; }

        public static GraphicsDevice GraphicsDevice { get; set; }

        public static Texture2D GetRandomPlanetFromDisk(GraphicsDevice graphicsDevice, Random random)
        {
            string applicationPath = Directory.GetCurrentDirectory() + "\\PlanetTextures\\";
            var filePaths = Directory.GetFiles(applicationPath);
            var filePath = filePaths[random.Next(0, filePaths.Count())];
            Texture2D sprite = Texture2D.FromFile(graphicsDevice, filePath);

            return sprite;
        }

        public static Texture2D GetPlanetFromDisk(GraphicsDevice graphicsDevice, string nameOfPlanet)
        {
            string applicationPath = Directory.GetCurrentDirectory() + "\\PlanetTextures\\";
            applicationPath += nameOfPlanet+".png";
            Texture2D sprite = Texture2D.FromFile(graphicsDevice, applicationPath);

            return sprite;
        }

        public static Planet GenerateMappedPlanet(Level level, string planetName, Vector2 position)
        {
            Random random = MetaGameSettings.Random;
            Planet randomPlanet = new Planet(LocationTypes.Planet);
            randomPlanet.Position = MetaGameSettings.GetRandomMapPosition(random);
            randomPlanet.TypeName = LocationTypes.Planet;
            randomPlanet.Texture = GetPlanetFromDisk(GraphicsDevice, planetName);
            randomPlanet.Scale = 0.5f + (float)random.NextDouble();
            randomPlanet.Name = PlanetNames.GetRandomPlanetName();

            randomPlanet.AfterUpdate = (GameTime gt, Renderable planetBoi) =>
            {
                var smallDegree = AngleUtility.DegreesToRadians(0.01f);
                planetBoi.Rotation += smallDegree;
            };
            return randomPlanet;
        }

        public static List<Renderable> GeneratePlanetStuff(Level level)
        {
            List<Renderable> items = new List<Renderable>();

            Random random = MetaGameSettings.Random;
            Planet randomPlanet = new Planet(LocationTypes.Planet);
            randomPlanet.Position = MetaGameSettings.GetRandomMapPosition(random);
            randomPlanet.TypeName = LocationTypes.Planet;
            randomPlanet.Texture = GetRandomPlanetFromDisk(GraphicsDevice, random);
            randomPlanet.Scale = 0.5f + (float)random.NextDouble();
            randomPlanet.Name = PlanetNames.GetRandomPlanetName();

            randomPlanet.AfterUpdate = (GameTime gt, Renderable planetBoi) =>
            {
                var smallDegree = AngleUtility.DegreesToRadians(0.01f);
                planetBoi.Rotation += smallDegree;
            };

            items.Add(randomPlanet);

            return items;
        }


        public static Gun GetPlayerGun(int teamNumber, Level level, double timeBetweenFiring)
        {
            SpaceShip effect = SpaceShip.GetEmptyEffect();
            effect.Health = -20;

            Bullet bullet = new Bullet(3, level, effect);
            bullet.TeamNumber = teamNumber;
            bullet.Speed = 750f * 3;
            bullet.OnDeath = () =>
            {
                //SoundDictionary["explosion"].Play();
            };
            bullet.Texture = TextureDictionary["LazerSprite"];
            bullet.Scale = 1.5f;
            Gun playerGun = new Gun(timeBetweenFiring);
            playerGun.Texture = TextureDictionary["TorpedoSprite"];
            playerGun.Level = level;
            playerGun.Timer = new SimpleTimer(0.25);
            playerGun.Bullet = bullet;

            return playerGun;
        }

        public static Gun GetLazerGun(int teamNumber, Level level, double timeBetweenFiring)
        {
            SpaceShip effect = SpaceShip.GetEmptyEffect();
            effect.Health = -20;

            Bullet bullet = new Bullet(3, level, effect);
            bullet.TeamNumber = teamNumber;
            bullet.Speed = 750f;
            bullet.OnDeath = () =>
            {
                //SoundDictionary["explosion"].Play();
            };
            bullet.Texture = TextureDictionary["LazerSprite"];
            bullet.Scale = 0.5f;
            Gun playerGun = new Gun(timeBetweenFiring);
            playerGun.Texture = TextureDictionary["TorpedoSprite"];
            playerGun.Level = level;
            playerGun.Timer = new SimpleTimer(0.25);
            playerGun.Bullet = bullet;

            return playerGun;
        }

        public static Gun GetTorpedoLauncher(int teamNumber, Level level, double timeBetweenFiring)
        {
            SpaceShip effect = SpaceShip.GetEmptyEffect();
            effect.Health = -5;

            Bullet bullet = new Bullet(5, level, effect);
            bullet.TeamNumber = teamNumber;
            bullet.Speed = 1000f;
            bullet.Texture = TextureDictionary["RedLaserSprite"];
            bullet.Scale = 1.5f;
            Gun playerGun = new Gun(timeBetweenFiring);
            playerGun.Texture = TextureDictionary["RedLaserSprite"];
            playerGun.Level = level;
            playerGun.Timer = new SimpleTimer(0.25);
            playerGun.Bullet = bullet;

            return playerGun;
        }

        public static Gun GetTrackingMissileLauncher(int teamNumber, Level level, double timeBetweenFiring)
        {
            SpaceShip effect = SpaceShip.GetEmptyEffect();
            effect.Health = -20;

            Bullet bullet = new Bullet(20, level, effect);
            bullet.TeamNumber = teamNumber;
            bullet.Speed = 6.5f;
            bullet.OnDeath = () =>
            {
                //SoundDictionary["explosion"].Play();
            };
            bullet.Texture = TextureDictionary["TorpedoSprite"];
            bullet.Scale = 0.25f;

            Gun playerGun = new Gun(timeBetweenFiring);
            playerGun.Texture = TextureDictionary["TorpedoSprite"];
            playerGun.Level = level;
            playerGun.Timer = new SimpleTimer(0.25);
            playerGun.Bullet = bullet;

            return playerGun;
        }

        public static Bullet GetFuelPowerUp(int teamNumber, Level level, Vector2 center)
        {
            SpaceShip effect = SpaceShip.GetEmptyEffect();
            effect.Fuel = 1_000;

            Bullet bullet = new Bullet(20, level, effect);
            bullet.TeamNumber = teamNumber;
            bullet.Position = center;
            bullet.IsPowerUp = true;
            bullet.Texture = TextureDictionary["FuelPowerUpSprite"];
            bullet.Scale = 1.25f;

            return bullet;
        }

        public static Bullet GetMoneyPowerUp(int teamNumber, Level level, Vector2 center)
        {
            SpaceShip effect = SpaceShip.GetEmptyEffect();
            effect.Money = 25;

            Bullet bullet = new Bullet(20, level, effect);
            bullet.TeamNumber = teamNumber;
            bullet.Position = center;
            bullet.IsPowerUp = true;
            bullet.Texture = TextureDictionary["MoneyPowerUpSprite"];
            bullet.Scale = 1.25f;

            return bullet;
        }

        public static Bullet GetHealthPowerUp(int teamNumber, Level level, Vector2 center)
        {
            SpaceShip effect = SpaceShip.GetEmptyEffect();
            effect.Health = 10;

            Bullet bullet = new Bullet(20, level, effect);
            bullet.TeamNumber = teamNumber;
            bullet.Position = center;
            bullet.IsPowerUp = true;
            bullet.Texture = TextureDictionary["HealthPowerUp"];
            bullet.Scale = 1.25f;

            return bullet;
        }

        public static Bullet GetAmmoPowerUp(int teamNumber, Level level, Vector2 center)
        {
            SpaceShip effect = SpaceShip.GetEmptyEffect();
            effect.Ammo = 10;

            Bullet bullet = new Bullet(20, level, effect);
            bullet.TeamNumber = teamNumber;
            bullet.Position = center;
            bullet.IsPowerUp = true;
            bullet.Texture = TextureDictionary["AmmoPowerUp"];
            bullet.Scale = 1.25f;

            return bullet;
        }

        public static TrafficAI GetTrafficAI(Level level, bool enemy)
        {
            List<Texture2D> textures;

            if (!enemy)
            {
                textures = new List<Texture2D>()
                {
                    TextureDictionary["CargoShipSprite"],
                    TextureDictionary["CargoShipSprite2"],
                    TextureDictionary["CargoShipSprite3"],
                    TextureDictionary["CargoShipSprite4"],
                    TextureDictionary["CargoShipSprite5"],
                };
            }
            else
            {
                textures = new List<Texture2D>()
                {
                    TextureDictionary["EnemyShipSprite"],
                    TextureDictionary["EnemyShipSprite2"],
                    TextureDictionary["EnemyShipSprite3"],
                    TextureDictionary["EnemyShipSprite4"],
                };
            }

            Random r = new Random(DateTime.Now.Millisecond);

            var waypoints = new List<Vector2>();
            // Get planets in level...
            waypoints = level.Objects.Where(x => x.GetType().Name == nameof(Planet)).Select(x => x.GetCenter()).ToList();
            // scramble waypoints so ships go different places
            waypoints = waypoints.OrderBy(x => r.NextDouble()).ToList();

            TrafficAI ai = new TrafficAI(waypoints, level, textures[Random.Next(0, textures.Count)]);
            ai.Scale = 1.5f;
            ai.Position += new Vector2(Random.Next(-MetaGameSettings.MapWidth, MetaGameSettings.MapWidth), Random.Next(-MetaGameSettings.MapWidth, MetaGameSettings.MapWidth));
            ai.TeamNumber = 1;

            return ai;
        }

        public static TrafficAI GetTrafficAI(Level level, Vector2 goTo)
        {
            List<Texture2D> textures;
            textures = new List<Texture2D>()
            {
                 TextureDictionary["CargoShipSprite"],
                 TextureDictionary["CargoShipSprite2"],
                 TextureDictionary["CargoShipSprite3"],
                 TextureDictionary["CargoShipSprite4"],
                 TextureDictionary["CargoShipSprite5"],
            };

            Random r = new Random(DateTime.Now.Millisecond);

            var waypoints = new List<Vector2>() { goTo };

            TrafficAI ai = new TrafficAI(waypoints, level, textures[Random.Next(0, textures.Count)]);
            ai.Scale = 1.5f;
            ai.Position += new Vector2(Random.Next(-MetaGameSettings.MapWidth, MetaGameSettings.MapWidth), Random.Next(-MetaGameSettings.MapWidth, MetaGameSettings.MapWidth));
            ai.TeamNumber = 1;

            return ai;
        }

        private static string GetRandomPrefix()
        {
            Random random = new Random(DateTime.Now.Millisecond);
            string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            char letter = alphabet[random.Next(0, alphabet.Length)];
            string prefix = letter+"-"+random.Next(1, 30);
            return prefix;
        }

        public static Planet GetRepairStation(Level level)
        {
            // Set start portal...
            Planet repairStation = new Planet(LocationTypes.Station);
            repairStation.HasRepairs = true;
            repairStation.Position = new Vector2(0, 0);
            repairStation.Texture = TextureDictionary["RepairStationSprite"];
            repairStation.AfterUpdate = (GameTime gameTime, Renderable portal) =>
            {
                portal.Rotation += AngleUtility.DegreesToRadians(-0.5f);
            };
            repairStation.Name = "Repair Station " + GetRandomPrefix();

            repairStation.EntryPoint = LocationFactory.GetRepairStation();

            return repairStation;
        }

        public static Planet GetAmmoStation(Level level)
        {
            // Set start portal...
            Planet ammoStation = new Planet(LocationTypes.Station);
            ammoStation.HasAmmo = true;
            ammoStation.Position = new Vector2(0, 0);
            ammoStation.Texture = TextureDictionary["AmmoStationSprite"];
            ammoStation.AfterUpdate = (GameTime gameTime, Renderable portal) =>
            {
                portal.Rotation += AngleUtility.DegreesToRadians(-0.5f);
            };
            ammoStation.Name = "Ammo Station " + GetRandomPrefix();

            ammoStation.EntryPoint = LocationFactory.GetAmmoStation(ammoStation.Name);

            return ammoStation;
        }

        public static Planet GetTradeStation(bool addRepairStation, bool addAmmoStation, bool addFuelStation)
        {
            // Set start portal...
            Planet tradeStation = new Planet(LocationTypes.Station);
            tradeStation.Position = new Vector2(0, 0);
            tradeStation.Texture = TextureDictionary["TradeStationSprite"];
            tradeStation.AfterUpdate = (GameTime gameTime, Renderable portal) =>
            {
                portal.Rotation += AngleUtility.DegreesToRadians(-0.15f);
            };
            tradeStation.Name = "Trade Station " + GetRandomPrefix();

            tradeStation.EntryPoint = LocationFactory.GetTradeStation(tradeStation, addRepairStation, addAmmoStation, addFuelStation);

            return tradeStation;
        }

        public static Planet GetEnemyStation()
        {
            // Set start portal...
            Planet enemyStation = new Planet(LocationTypes.Station);
            enemyStation.Position = new Vector2(0, 0);
            enemyStation.Texture = TextureDictionary["EvilSpaceStation"];
            enemyStation.AfterUpdate = (GameTime gameTime, Renderable portal) =>
            {
                portal.Rotation -= AngleUtility.DegreesToRadians(-0.05f);
            };
            enemyStation.Name = "Enemy Station " + GetRandomPrefix();

            enemyStation.TeamNumber = 1;
            return enemyStation;
        }
        public static Planet GetFuelStation(Level level)
        {
            // Set start portal...
            Planet fuelStation = new Planet(LocationTypes.Station);
            fuelStation.Position = new Vector2(0, 0);
            fuelStation.Texture = TextureDictionary["FuelStationSprite"];
            fuelStation.AfterUpdate = (GameTime gameTime, Renderable portal) =>
            {
                portal.Rotation += AngleUtility.DegreesToRadians(-0.5f);
            };
            fuelStation.Name = "Fuel Station " + GetRandomPrefix();
            fuelStation.HasFuel = true;

            fuelStation.EntryPoint = LocationFactory.GetFuelStation();

            return fuelStation;
        }

        public static Asteroid GetDangerousAsteroid(Level level)
        {
            Asteroid asteroid;
            asteroid = new Asteroid(SpaceShip.GetEmptyEffect(), level, Asteroid.AsteroidType.Damage, 75);
            asteroid.Texture = TextureDictionary["BombAsteroidSprite"];
            asteroid.Scale = 3.75f * (float)Random.NextDouble();
            asteroid.Position += new Vector2(Random.Next(-30000, 30000), Random.Next(-30000, 30000));

            return asteroid;
        }

        public static Asteroid GetRandomAsteroid(Level level)
        {
            var asteroidSpeed = 50;

            asteroidSpeed = MetaGameSettings.Random.Next(50, 300);

            Asteroid asteroid;

            if (Random.NextDouble() < 0.05)
            {
                asteroid = new Asteroid( SpaceShip.GetEmptyEffect(), level, Asteroid.AsteroidType.Fuel, asteroidSpeed);
                asteroid.Texture = TextureDictionary["FuelAsteroidSprite"];
                asteroid.Scale = (0.75f * (float)Random.NextDouble()) + 0.25f;
                asteroid.Position = new Vector2(Random.Next(-30000, 30000), Random.Next(-30000, 30000));
            }
            else if (Random.NextDouble() < 0.025)
            {
                asteroid = new Asteroid(SpaceShip.GetEmptyEffect(), level, Asteroid.AsteroidType.Health, asteroidSpeed);
                asteroid.Texture = TextureDictionary["HealthSatelliteSprite"];
                asteroid.Scale = 2.0f;
                asteroid.Position = new Vector2(Random.Next(-30000, 30000), Random.Next(-30000, 30000));
            }
            else
            {
                asteroid = new Asteroid(SpaceShip.GetEmptyEffect(), level, Asteroid.AsteroidType.Damage, asteroidSpeed);
                asteroid.Texture = TextureDictionary["BombAsteroidSprite"];
                asteroid.Scale = 2.0f;
                asteroid.Position = new Vector2(Random.Next(-30000, 30000), Random.Next(-30000, 30000));
            }

            asteroid.Rotation = (float)((Math.PI * 2) * Random.NextDouble());

            asteroid.TeamNumber = MetaGameSettings.AsteroidTeamNumber;

            asteroid.Scale = 3f + (float)Random.NextDouble();

            return asteroid;
        }

        public static Renderable GetTargetReticle()
        {
            Renderable targetReticle = new Renderable();
            targetReticle.Texture = TextureDictionary["TargetMarkerSprite"];
            targetReticle.Scale = 1.0f;

            return targetReticle;
        }

        public static CombatAI GetSuperMissileFrigate(Level level, Renderable target)
        {
            var trafficToCopy = GameFactory.GetTrafficAI(level, true);
            var gun = GameFactory.GetTrackingMissileLauncher(1, level, 1.5);
            var combatModel = new CombatAI(1, target, trafficToCopy.Ship.GetCopy(), level, gun);
            combatModel.Texture = trafficToCopy.Texture;
            gun.Bullet.Owner = combatModel;
            gun.Bullet.OwnerShip = combatModel.Ship;
            gun.Bullet.TracksTarget = true;

            combatModel.Ship.Health = 1_000;
            combatModel.Ship.MaxHealth = 1_000;

            combatModel.FiringCone = 380;

            combatModel.UpdateCollisionBox();

            combatModel.TeamNumber = 1;

            combatModel.Texture = TextureDictionary["EnemyShipSprite3"];
            combatModel.Scale = 1.5f;

            return combatModel;
        }
        
        public static CombatAI GetMissileFrigate(Level level, Renderable target)
        {
            var trafficToCopy = GameFactory.GetTrafficAI(level, true);
            var gun = GameFactory.GetTrackingMissileLauncher(1, level, 5);
            var combatModel = new CombatAI(1, target, trafficToCopy.Ship.GetCopy(), level, gun);
            combatModel.Texture = trafficToCopy.Texture;
            gun.Bullet.Owner = combatModel;
            gun.Bullet.OwnerShip = combatModel.Ship;
            gun.Bullet.TracksTarget = true;

            combatModel.Ship.Health = 500;

            combatModel.FiringCone = 180;

            combatModel.UpdateCollisionBox();

            combatModel.TeamNumber = 1;

            combatModel.Texture = TextureDictionary["EnemyShipSprite"];
            combatModel.Scale = 1.5f;

            return combatModel;
        }

        public static CombatAI GetEnemyFighter(Level level, Renderable target)
        {
            return GetGenericFighter(1, "EnemyShipSprite3", "RedLaserSprite", level, target);
        }

        public static CombatAI GetFriendlyFighter(Level level, Renderable target)
        {
            var friendly =  GetGenericFighter(0, "FriendlyFighter1", "LazerSprite", level, target);
            friendly.Scale = 0.35f;
            return friendly;
        }

        public static CombatAI GetGenericFighter(int teamNumber,string sprite, string laserSprite, Level level, Renderable target)
        {
            var trafficToCopy = GameFactory.GetTrafficAI(level, true);
            var gun = GameFactory.GetTorpedoLauncher(0, level, 0.25);
            var combatModel = new CombatAI(teamNumber, target, trafficToCopy.Ship.GetCopy(), level, gun);
            combatModel.Texture = trafficToCopy.Texture;
            gun.Bullet.Owner = combatModel;
            gun.Bullet.OwnerShip = combatModel.Ship;
            gun.Bullet.TracksTarget = false;
            gun.Bullet.Texture = TextureDictionary[laserSprite];
            gun.Bullet.Speed *= 3;

            combatModel.Ship.MaxAmmo = 10_000;
            combatModel.Ship.Ammo = 10_000;
            combatModel.Ship.Speed *= 4;
            combatModel.Ship.Health = 350;

            combatModel.FiringCone = 5;

            combatModel.UpdateCollisionBox();

            combatModel.TeamNumber = teamNumber;
            gun.Bullet.TeamNumber = teamNumber;
            gun.TeamNumber = teamNumber;

            combatModel.Ship.TurnRate = 0.004363323f * 7f;

            combatModel.Texture = TextureDictionary[sprite];
            combatModel.Scale = 0.8f;

            combatModel.LeadTarget = false;

            combatModel.SearchForTargets = true;

            combatModel.EngagementDistance = MetaGameSettings.ScreenDistance;

            combatModel.CanTargetPlayer = false;

            return combatModel;
        }

        public static CombatAI GetPirateFighter(Level level, Renderable target)
        {
            var trafficToCopy = GameFactory.GetTrafficAI(level, true);
            var gun = GameFactory.GetTorpedoLauncher(1, level, 1);
            var combatModel = new CombatAI(1, target, trafficToCopy.Ship.GetCopy(), level, gun);
            combatModel.Texture = trafficToCopy.Texture;
            gun.Bullet.Owner = combatModel;
            gun.Bullet.OwnerShip = combatModel.Ship;
            gun.Bullet.TracksTarget = false;

            combatModel.Ship.MaxAmmo = 10_000;
            combatModel.Ship.Ammo = 10_000;

            combatModel.Ship.Health = 150;

            combatModel.FiringCone = 45;

            combatModel.UpdateCollisionBox();

            combatModel.TeamNumber = 1;

            combatModel.Ship.TurnRate = 0.004363323f * 4f;

            combatModel.Texture = TextureDictionary["PirateFighter1"];
            combatModel.Scale = 1.0f;

            combatModel.LeadTarget = false;

            return combatModel;
        }


        public static CombatAI GetPirateMissileFrigate(Level level, Renderable target)
        {
            var trafficToCopy = GameFactory.GetTrafficAI(level, true);
            var gun = GameFactory.GetTrackingMissileLauncher(1, level, 10);
            var combatModel = new CombatAI(1, target, trafficToCopy.Ship.GetCopy(), level, gun);
            combatModel.Texture = trafficToCopy.Texture;
            gun.Bullet.Owner = combatModel;
            gun.Bullet.OwnerShip = combatModel.Ship;
            gun.Bullet.TracksTarget = true;

            combatModel.Ship.Health = 800;

            combatModel.FiringCone = 180;

            combatModel.UpdateCollisionBox();

            combatModel.TeamNumber = 1;

            combatModel.Texture = TextureDictionary["PirateFighter2"];
            combatModel.Scale = 1.5f;

            return combatModel;
        }


        public static CombatAI GetFriendlyTurret(Level level, Renderable target)
        {
            var trafficToCopy = GameFactory.GetTrafficAI(level, true);
            var gun = GameFactory.GetTorpedoLauncher(0, level, 1);
            var combatModel = new CombatAI(0, target, trafficToCopy.Ship.GetCopy(), level, gun);
            combatModel.Texture = trafficToCopy.Texture;
            gun.Bullet.Owner = combatModel;
            gun.Bullet.OwnerShip = combatModel.Ship;
            gun.Bullet.TracksTarget = false;
            gun.Bullet.Texture = TextureDictionary["LazerSprite"];
            gun.Bullet.Scale = 1.5f;
            gun.Bullet.Speed *= 3;

            combatModel.Ship.Health = 500;

            combatModel.FiringCone = 45;

            combatModel.UpdateCollisionBox();

            combatModel.TeamNumber = 0;

            combatModel.Ship.TurnRate = 0.004363323f * 4f;

            combatModel.Texture = TextureDictionary["TurretSprite"];
            combatModel.Scale = 0.5f;

            combatModel.SearchForTargets = true;

            combatModel.Ship.Speed = 0;

            combatModel.EngagementDistance = MetaGameSettings.ScreenDistance / 2;

            combatModel.TargetAsteroids = true;

            return combatModel;
        }

        public static CombatAI GetEnemyTurret(Level level, Renderable target)
        {
            var trafficToCopy = GameFactory.GetTrafficAI(level, true);
            var gun = GameFactory.GetTorpedoLauncher(1, level, 0.5);
            var combatModel = new CombatAI(1, target, trafficToCopy.Ship.GetCopy(), level, gun);
            combatModel.Texture = trafficToCopy.Texture;
            gun.Bullet.Owner = combatModel;
            gun.Bullet.OwnerShip = combatModel.Ship;
            gun.Bullet.TracksTarget = false;
            gun.Bullet.Texture = TextureDictionary["RedLaserSprite"];
            gun.Bullet.Scale = 1.5f;
            gun.Bullet.Speed *= 4;

            combatModel.Ship.Health = 2750;

            combatModel.FiringCone = 45;

            combatModel.UpdateCollisionBox();

            combatModel.TeamNumber = 1;

            combatModel.Ship.TurnRate = 0.004363323f * 4f;

            combatModel.Texture = TextureDictionary["EnemyTurretSprite"];
            combatModel.Scale = 1.5f;

            combatModel.SearchForTargets = true;

            combatModel.Ship.Speed = 0;

            combatModel.EngagementDistance = MetaGameSettings.ScreenDistance / 2;

            combatModel.TargetAsteroids = true;

            return combatModel;
        }

        public static CombatAI GetGunBoat(Level level, Renderable target)
        {
            var trafficToCopy = GameFactory.GetTrafficAI(level, true);
            var gun = GameFactory.GetTrackingMissileLauncher(1, level, 5);
            var combatModel = new CombatAI(1, target, trafficToCopy.Ship.GetCopy(), level, gun);
            combatModel.Texture = trafficToCopy.Texture;
            gun.Bullet.Owner = combatModel;
            gun.Bullet.OwnerShip = combatModel.Ship;
            gun.Bullet.TracksTarget = true;

            combatModel.Ship.Health = 500;

            combatModel.FiringCone = 180;

            combatModel.UpdateCollisionBox();

            combatModel.TeamNumber = 1;

            return combatModel;
        }

    }
}
