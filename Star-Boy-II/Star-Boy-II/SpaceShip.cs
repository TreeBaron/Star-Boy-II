﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Star_Boy_II
{
    public class SpaceShip
    {
        public bool LimitValues { get; set; } = false;

        public bool IsPlayer { get; set; } = false;

        public string Name { get; set; } = "Unnamed Spaceship";

        public int Money { get; set; }

        public int MaxCargo { get; set; }

        public Dictionary<string, int> CargoBay { get; set; }

        public int GetCargoSpaceLeft()
        {
            int totalSpaceTaken = GetCargoSpaceTakenUp();
            return MaxCargo - totalSpaceTaken;
        }

        public int GetCargoSpaceTakenUp()
        {
            int totalSpaceTaken = 0;
            foreach (var item in CargoBay)
            {
                totalSpaceTaken += item.Value;
            }
            return totalSpaceTaken;
        }

        private int _health = 0;
        public int Health { 
            get
            {
                return _health;
            }
            set
            {
                _health = value;
                if(_health > MaxHealth && LimitValues)
                {
                    _health = MaxHealth;
                }
            }
        }

        public int MaxHealth { get; set; }

        private int _fuel = 0;
        public int Fuel {
            get
            {
                return _fuel;
            }
            set
            {
                _fuel = value;
                if(_fuel > MaxFuel && LimitValues)
                {
                    _fuel= MaxFuel;
                }
            }
        }

        public int MaxFuel { get; set; }

        private int _ammo = 0;
        public int Ammo { 
            get
            {
                return _ammo;
            }
            set 
            { 
                _ammo = value;
                if(_ammo > MaxAmmo && LimitValues)
                {
                    _ammo = MaxAmmo;
                }
            }
        }

        public int MaxAmmo { get; set; }

        public float Speed { get; set; }

        public float TurnRate { get; set; }
        public bool HasBeenUpgraded { get; set; } = false;

        /// <summary>
        /// Determines if a spaceship is dead.
        /// </summary>
        /// <returns>If a spaceship is dead.</returns>
        public bool IsDead()
        {
            if(Money < 0)
            {
                return true;
            }

            if(Health <= 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Apply one spaceship to another as an additive effect.
        /// This is dynamic on float, int, and double properties and need not
        /// be updated (theoretically). This uses reflection to do so.
        /// </summary>
        /// <param name="effect">A <see cref="SpaceShip"/> to apply as an effect.</param>
        public SpaceShip ApplyEffect(SpaceShip effect, bool byPassMoney = true)
        {
            // do not apply money effects if the player does not have enough
            if(byPassMoney == false && (Money + effect.Money) < 0)
            {
                return this;
            }

            foreach(PropertyInfo propertyInfo in this.GetType().GetProperties())
            {
                // only if it is a float or integer...
                if (propertyInfo.PropertyType == typeof(float))
                {
                    // get this objects property...
                    var firstValue = (float)this.GetType().GetProperty(propertyInfo.Name).GetValue(this, null);
                    var secondValue = (float)effect.GetType().GetProperty(propertyInfo.Name).GetValue(effect, null);
                    this.GetType().GetProperty(propertyInfo.Name).SetValue(this,(firstValue + secondValue));
                }
                else if (propertyInfo.PropertyType == typeof(int))
                {
                    // get this objects property...
                    var firstValue = (int)this.GetType().GetProperty(propertyInfo.Name).GetValue(this, null);
                    var secondValue = (int)effect.GetType().GetProperty(propertyInfo.Name).GetValue(effect, null);
                    this.GetType().GetProperty(propertyInfo.Name).SetValue(this, (firstValue + secondValue));
                }
                else if (propertyInfo.PropertyType == typeof(double))
                {
                    // get this objects property...
                    var firstValue = (double)this.GetType().GetProperty(propertyInfo.Name).GetValue(this, null);
                    var secondValue = (double)effect.GetType().GetProperty(propertyInfo.Name).GetValue(effect, null);
                    this.GetType().GetProperty(propertyInfo.Name).SetValue(this, (firstValue + secondValue));
                }
            }

            return this;
        }

        public SpaceShip GetCopy()
        {
            return (SpaceShip)this.MemberwiseClone();
        }

        public static SpaceShip GetEmptyEffect()
        {
            var spaceship = new SpaceShip()
            {
                Money = 0,
                Health = 0,
                MaxHealth = 0,
                Fuel = 0,
                MaxFuel = 0,
                Ammo = 0,
                MaxAmmo = 0,
                Speed = 0,
                MaxCargo = 0,
                TurnRate = 0,
            };

            return spaceship;
        }

        public static SpaceShip GetDefaultShip()
        {
            var spaceship = new SpaceShip()
            {
                Money = 50,
                Health = 50,
                MaxHealth = 100,
                Fuel = 50_000,
                MaxFuel = 100_000,
                Ammo = 100,
                MaxAmmo = 100,
                Speed = 12.0f,
                TurnRate = 0.01f,
                MaxCargo = 100,
                LimitValues = true,
            };

            return spaceship;
        }

        public static SpaceShip GetPlayerShip()
        {
            var spaceship = new SpaceShip()
            {
                Money = 500,
                Health = 500,
                MaxHealth = 1000,
                Fuel = 50_000,
                MaxFuel = 100_000,
                Ammo = 100,
                MaxAmmo = 100,
                Speed = 16.0f,
                TurnRate = 0.01f,
                MaxCargo = 100,
                LimitValues = true,
            };

            spaceship.CargoBay = new Dictionary<string, int>();

            return spaceship;
        }
    }
}
