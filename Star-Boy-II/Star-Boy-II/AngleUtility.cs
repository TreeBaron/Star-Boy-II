﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Star_Boy_II
{
    public static class AngleUtility
    {
        public static bool RectanglesCollide(List<(Point, Point)> first, List<(Point, Point)> second)
        {
            foreach(var line1 in first)
            {
                foreach(var line2 in second)
                {
                    if(DoIntersect(line1.Item1, line1.Item2, line2.Item1, line2.Item2))
                    {
                        return true;
                    }
                }    
            }
            return false;
        }

        // Given three collinear points p, q, r, the function checks if
        // point q lies on line segment 'pr'
        static bool OnSegment(Point p, Point q, Point r)
        {
            if (q.x <= Math.Max(p.x, r.x) && q.x >= Math.Min(p.x, r.x) &&
                q.y <= Math.Max(p.y, r.y) && q.y >= Math.Min(p.y, r.y))
                return true;

            return false;
        }

        // To find orientation of ordered triplet (p, q, r).
        // The function returns following values
        // 0 --> p, q and r are collinear
        // 1 --> Clockwise
        // 2 --> Counterclockwise
        static int Orientation(Point p, Point q, Point r)
        {
            // See https://www.geeksforgeeks.org/orientation-3-ordered-points/
            // for details of below formula.
            int val = (q.y - p.y) * (r.x - q.x) -
                    (q.x - p.x) * (r.y - q.y);

            if (val == 0) return 0; // collinear

            return (val > 0) ? 1 : 2; // clock or counterclock wise
        }

        // The main function that returns true if line segment 'p1q1'
        // and 'p2q2' intersect.
        static bool DoIntersect(Point p1, Point q1, Point p2, Point q2)
        {
            // Find the four orientations needed for general and
            // special cases
            int o1 = Orientation(p1, q1, p2);
            int o2 = Orientation(p1, q1, q2);
            int o3 = Orientation(p2, q2, p1);
            int o4 = Orientation(p2, q2, q1);

            // General case
            if (o1 != o2 && o3 != o4)
                return true;

            // Special Cases
            // p1, q1 and p2 are collinear and p2 lies on segment p1q1
            if (o1 == 0 && OnSegment(p1, p2, q1)) return true;

            // p1, q1 and q2 are collinear and q2 lies on segment p1q1
            if (o2 == 0 && OnSegment(p1, q2, q1)) return true;

            // p2, q2 and p1 are collinear and p1 lies on segment p2q2
            if (o3 == 0 && OnSegment(p2, p1, q2)) return true;

            // p2, q2 and q1 are collinear and q1 lies on segment p2q2
            if (o4 == 0 && OnSegment(p2, q1, q2)) return true;

            return false; // Doesn't fall in any of the above cases
        }

        /// <summary>
        /// Returns degrees in radians.
        /// </summary>
        /// <param name="amount">The amount of degrees.</param>
        /// <returns></returns>
        public static float DegreesToRadians(float amount)
        {
            return (float)Math.PI / 180.0f * amount;
        }

        public static float RadiansToDegrees(float amount)
        {
            return 180.0f / (float)Math.PI * amount;
        }

        //Source: https://stackoverflow.com/questions/8148651/rotation-of-an-object-around-a-central-vector2-point
        public static Vector2 RotateAboutOrigin(Vector2 point, Vector2 origin, float rotation)
        {
            return Vector2.Transform(point - origin, Matrix.CreateRotationZ(rotation)) + origin;
        }

        public static float GetAngleToPosition(Vector2 Position_Start, Vector2 Position_End)
        {
            Vector2 direction = Position_Start - Position_End;
            direction.Normalize();
            float angle = (float)Math.Atan2(direction.Y, direction.X); //swapped x and y
            var result = angle + (float)Math.PI;
            return result;
        }

        /// <summary>
        /// Turn towards wanted rotation.
        /// </summary>
        /// <param name="wantedRotation">The desired rotation to turn to.</param>
        public static float RotationLogic(float currentRotation, float wantedRotation, float turnRate)
        {
            while (currentRotation < 0)
            {
                currentRotation += (float)(Math.PI * 2);
            }

            while (currentRotation > (float)(Math.PI * 2))
            {
                currentRotation -= (float)(Math.PI * 2);
            }

            while (wantedRotation < 0)
            {
                wantedRotation += (float)(Math.PI * 2);
            }

            while (wantedRotation > (float)(Math.PI * 2))
            {
                wantedRotation -= (float)(Math.PI * 2);
            }

            //helps large objects not freak out...
            if (Math.Abs(currentRotation - wantedRotation) < turnRate)
            {
                return wantedRotation;
            }

            if (currentRotation > wantedRotation)
            {
                if (Math.Abs(wantedRotation - currentRotation) < MathHelper.Pi)
                {
                    currentRotation -= turnRate;
                }
                else
                {
                    currentRotation += turnRate;
                }
            }
            else
            {
                if (Math.Abs(wantedRotation - currentRotation) < MathHelper.Pi)
                {
                    currentRotation += turnRate;
                }
                else
                {
                    currentRotation -= turnRate;
                }
            }

            return currentRotation;
        }
    }
}
