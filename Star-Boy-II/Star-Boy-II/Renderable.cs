﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using Windows.Foundation;

namespace Star_Boy_II
{
    public class Renderable : ICollidable
    {
        public bool DrawHitBox { get; set; } = false;

        /// <summary>
        /// The velocity of the given object.
        /// </summary>
        public Vector2 Velocity { get; set; }

        /// <summary>
        /// Indicates to the level that this object should be dereferenced and removed.
        /// Mark this when an object is permanently "dead"
        /// </summary>
        public bool RemoveFromLevel { get; set; }

        public bool Visible { get; set; } = true;

        public Texture2D Texture { get; set; }

        public Vector2 Position { get; set; } = new Vector2(0, 0);

        public float Scale { get; set; } = 1.0f;

        public float Rotation { get; set; } = 0;

        public Action<GameTime, Renderable> AfterUpdate { get; set; }

        /// <summary>
        /// Indicates a type of Renderable without needing to define a class.
        /// This could be like, planet, asteroid, vehicle, nebula, etc.
        /// </summary>
        public string TypeName { get; set; } = "Generic";

        public CollisionTracker CollisionBox { get; set; }

        public int TeamNumber { get; set; } = 0;

        public static Texture2D HitBoxTexture { get; set; }

        public virtual void Draw(SpriteBatch spriteBatch, Rectangle window)
        {
            if (!Visible) return;

            if(DrawHitBox && CollisionBox != null)
            {
                var cornerPositions = CollisionBox.CornerVectors;

                foreach(var cornerPosition in cornerPositions)
                {
                    var rectangle = new Rectangle((int)cornerPosition.X, (int)cornerPosition.Y, 12, 12);

                    spriteBatch.Draw(HitBoxTexture, rectangle, Color.White);
                }
            }

            var center = new Vector2(Texture.Width / 2, Texture.Height / 2);
            spriteBatch.Draw(Texture, Position, null, Color.White, Rotation, center, Scale, SpriteEffects.None, 0); //Draw with rotation!
        }

        public virtual void Update(GameTime gameTime)
        {
            // Nothing for now.

            if (AfterUpdate != null)
            {
                AfterUpdate(gameTime, this);
            }
        }

        /// <summary>
        /// Returns the center of this Renderable object.
        /// </summary>
        /// <returns>The center of this object as a <see cref="Vector2"/>.</returns>
        public Vector2 GetCenter()
        {
            return (Position + new Vector2(GetWidth() / 2, GetHeight() / 2));
        }

        /// <summary>
        /// Returns the scaled width of the object.
        /// </summary>
        /// <returns>The scaled width.</returns>
        public float GetWidth()
        {
            return (Texture.Width * Scale);
        }

        /// <summary>
        /// Returns the scaled height of the object.
        /// </summary>
        /// <returns>The scaled height.</returns>
        public float GetHeight()
        {
            return (Texture.Height * Scale);
        }

        public Renderable GetCopy()
        {
            return (Renderable)this.MemberwiseClone();
        }

        public void MoveTowards(float rotation, float speed, GameTime gameTime)
        {
            Vector2 directionOfTravel = new Vector2((float)Math.Cos(rotation), (float)Math.Sin(rotation));
            directionOfTravel.Normalize();
            Velocity += directionOfTravel * speed;

            Position += Velocity * (float)gameTime.ElapsedGameTime.TotalSeconds;
        }

        public float Degrees(double amount)
        {
            var multiplier = Math.PI / 180.0;

            return (float)(multiplier * amount);
        }

        public virtual void UpdateCollisionBox()
        {
            var x = (int)(Position.X - (GetWidth() / 2));
            var y = (int)(Position.Y - (GetHeight() / 2));

            var nonRotatedBox = new Rectangle(x, y, (int)GetWidth(), (int)GetHeight());

            var origin = Position;

            List<Vector2> corners = new List<Vector2>()
            {
                new Vector2(x, y),
                new Vector2(x + nonRotatedBox.Width, y),
                new Vector2(x, y + nonRotatedBox.Height),
                new Vector2(x +nonRotatedBox.Width, y + nonRotatedBox.Height),
            };

            for (int i = 0; i < corners.Count; i++)
            {
                // rotate collisionbox
                corners[i] = AngleUtility.RotateAboutOrigin(corners[i], origin, Rotation);
            }

            CollisionBox = new CollisionTracker(corners);
        }

        public virtual void OnCollision(object collidedWith)
        {
            // GNDN
        }
    }
}