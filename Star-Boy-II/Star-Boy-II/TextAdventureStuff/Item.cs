﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Star_Boy_II.TextAdventureStuff
{
    public class Item : Identure, IDescriptor
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
