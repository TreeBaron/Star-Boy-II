﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Star_Boy_II.Missions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Appointments;
using Windows.UI.Core;

namespace Star_Boy_II.TextAdventureStuff
{
    public class PersonFactory
    {
        private static List<string> FirstNames = new List<string>
        {
                // Male names
                "James", "John", "Robert", "Michael", "William",
                "David", "Richard", "Joseph", "Charles", "Thomas",
                "Daniel", "Matthew", "Donald", "Anthony", "Paul",
                "Mark", "George", "Steven", "Kenneth", "Andrew",
                "Edward", "Brian", "Ronald", "Kevin", "Jason",
                "Jeffrey", "Timothy", "Ryan", "Gary", "Nicholas",
                "Eric", "Stephen", "Jonathan", "Larry", "Justin",
                "Scott", "Brandon", "Frank", "Benjamin", "Gregory",
                "Samuel", "Raymond", "Patrick", "Alexander", "Jack",
                "Dennis", "Jerry", "Tyler", "Aaron", "Jose",

                // Female names
                "Mary", "Patricia", "Jennifer", "Linda", "Elizabeth",
                "Barbara", "Susan", "Jessica", "Sarah", "Karen",
                "Nancy", "Lisa", "Margaret", "Betty", "Dorothy",
                "Sandra", "Ashley", "Kimberly", "Donna", "Emily",
                "Michelle", "Carol", "Amanda", "Melissa", "Deborah",
                "Stephanie", "Rebecca", "Laura", "Sharon", "Cynthia",
                "Kathleen", "Helen", "Amy", "Shirley", "Angela",
                "Anna", "Brenda", "Pamela", "Nicole", "Ruth",
                "Katherine", "Christine", "Emma", "Catherine", "Virginia",
                "Debra", "Rachel", "Janet", "Carolyn", "Maria"
         };

        private static List<string> LastNames = new List<string>
            {
                "Smith", "Johnson", "Williams", "Jones", "Brown",
                "Davis", "Miller", "Wilson", "Moore", "Taylor",
                "Anderson", "Thomas", "Jackson", "White", "Harris",
                "Martin", "Thompson", "Garcia", "Martinez", "Robinson",
                "Clark", "Rodriguez", "Lewis", "Lee", "Walker",
                "Hall", "Allen", "Young", "Hernandez", "King",
                "Wright", "Lopez", "Hill", "Scott", "Green",
                "Adams", "Baker", "Gonzalez", "Nelson", "Carter",
                "Mitchell", "Perez", "Roberts", "Turner", "Phillips",
                "Campbell", "Parker", "Evans", "Edwards", "Collins",
                "Stewart", "Sanchez", "Morris", "Rogers", "Reed",

                // Add more last names as needed
            };

        public static string GetRandomName()
        {
            Random r = MetaGameSettings.Random;
            return FirstNames[r.Next(0, FirstNames.Count)]+" "+LastNames[r.Next(0,LastNames.Count)];
        }

        public static List<Person> GetAllPeople()
        {
            var list = new List<Person>();
            list.Add(GetFreddy());
            return list;
        }

        public static Person GetHelmsmen(Level level)
        {
            var helmsman = new Person();
            helmsman.Name = "Helmsman";
            helmsman.Description = "They pilot the ship.";

            var convo1 = new Conversation()
            {
                Question = "What sort of things can you do?",
                Answer = "I can tell you the bearing of station types, and their distance to you. This is helpful if the ship needs repairs or is low on fuel."
            };

            var fuelStation = new Conversation()
            {
                Question = "We need fuel.",
                CustomAnswer = (Game game, Person person, InputManager inputManager, string characterName) =>
                {
                    var player = game._level.Player;
                    var fuelStations = game._level.Objects.Where(x => x.GetType() == typeof(Planet) && ((Planet)x).HasFuel).ToList();
                    var sorted = fuelStations.OrderBy(x => Vector2.Distance(x.Position, player.Position)).ToList();

                    var nearest = (Planet)sorted.FirstOrDefault();

                    if(nearest == null)
                    {
                        return characterName+": Sir, I cannot find any on radar.";
                    }
                    else
                    {
                        var distance = (int)Vector2.Distance(nearest.Position, player.Position);
                        var bearing = AngleUtility.GetAngleToPosition(player.Position, nearest.Position);
                        var angleInDegrees = (int)AngleUtility.RadiansToDegrees(bearing);

                        while (angleInDegrees > 360)
                        {
                            angleInDegrees -= 360;
                        }

                        while (angleInDegrees < 0)
                        {
                            angleInDegrees += 360;
                        }

                        return "Sir, "+nearest.Name+" is bearing "+angleInDegrees+" degrees. It is "+distance+" MSUs away.";
                    }
                }
            };

            var ammoStation = new Conversation()
            {
                Question = "We need ammo.",
                CustomAnswer = (Game game, Person person, InputManager inputManager, string characterName) =>
                {
                    var player = game._level.Player;
                    var fuelStations = game._level.Objects.Where(x => x.GetType() == typeof(Planet) && ((Planet)x).HasAmmo).ToList();
                    var sorted = fuelStations.OrderBy(x => Vector2.Distance(x.Position, player.Position)).ToList();

                    var nearest = (Planet)sorted.FirstOrDefault();

                    if (nearest == null)
                    {
                        return characterName + ": Sir, I cannot find any on radar.";
                    }
                    else
                    {
                        var distance = (int)Vector2.Distance(nearest.Position, player.Position);
                        var bearing = AngleUtility.GetAngleToPosition(player.Position, nearest.Position);
                        var angleInDegrees = (int)AngleUtility.RadiansToDegrees(bearing);

                        while (angleInDegrees > 360)
                        {
                            angleInDegrees -= 360;
                        }

                        while (angleInDegrees < 0)
                        {
                            angleInDegrees += 360;
                        }

                        return "Sir, " + nearest.Name + " is bearing " + angleInDegrees + " degrees. It is " + distance + " MSUs away.";
                    }
                }
            };

            var repairStation = new Conversation()
            {
                Question = "We need repairs.",
                CustomAnswer = (Game game, Person person, InputManager inputManager, string characterName) =>
                {
                    var player = game._level.Player;
                    var fuelStations = game._level.Objects.Where(x => x.GetType() == typeof(Planet) && ((Planet)x).HasRepairs).ToList();
                    var sorted = fuelStations.OrderBy(x => Vector2.Distance(x.Position, player.Position)).ToList();

                    var nearest = (Planet)sorted.FirstOrDefault();

                    if (nearest == null)
                    {
                        return characterName + ": Sir, I cannot find any on radar.";
                    }
                    else
                    {
                        var distance = (int)Vector2.Distance(nearest.Position, player.Position);
                        var bearing = AngleUtility.GetAngleToPosition(player.Position, nearest.Position);
                        var angleInDegrees = (int)AngleUtility.RadiansToDegrees(bearing);

                        while (angleInDegrees > 360)
                        {
                            angleInDegrees -= 360;
                        }

                        while (angleInDegrees < 0)
                        {
                            angleInDegrees += 360;
                        }

                        return "Sir, " + nearest.Name + " is bearing " + angleInDegrees + " degrees. It is " + distance + " MSUs away.";
                    }
                }
            };

            var tradeStation = new Conversation()
            {
                Question = "Where's the nearest trade station?",
                CustomAnswer = (Game game, Person person, InputManager inputManager, string characterName) =>
                {
                    var player = game._level.Player;
                    var fuelStations = game._level.Objects.Where(x => x.TypeName == LocationTypes.Station).ToList();
                    var sorted = fuelStations.OrderBy(x => Vector2.Distance(x.Position, player.Position)).ToList();

                    var nearest = (Planet)sorted.FirstOrDefault();

                    if (nearest == null)
                    {
                        return characterName + ": Sir, I cannot find any on radar.";
                    }
                    else
                    {
                        var distance = (int)Vector2.Distance(nearest.Position, player.Position);
                        var bearing = AngleUtility.GetAngleToPosition(player.Position, nearest.Position);
                        var angleInDegrees = (int)AngleUtility.RadiansToDegrees(bearing);

                        while (angleInDegrees > 360)
                        {
                            angleInDegrees -= 360;
                        }

                        while (angleInDegrees < 0)
                        {
                            angleInDegrees += 360;
                        }

                        return "Sir, " + nearest.Name + " is bearing " + angleInDegrees + " degrees. It is " + distance + " MSUs away.";
                    }
                }
            };

            var setCourseForTrade = new Conversation()
            {
                Question = "Set a course for a station where we can sell the cargo in our hull.",
                CustomAnswer = (Game game, Person person, InputManager inputManager, string personName) =>
                {
                    var player = game._level.Player;
                    bool setCourse = false;
                    foreach (var item in player.Ship.CargoBay)
                    {
                        if (item.Value > 0)
                        {

                            var interested = player.GetNearestPlanetThatWants(item.Key);

                            if (interested != null)
                            {
                                setCourse = true;
                                var angleInDegrees = AngleUtility.RadiansToDegrees(AngleUtility.GetAngleToPosition(player.Position, interested.Position));
                                inputManager.WriteLine("Setting course for " + interested.Name + " bearing " + (int)angleInDegrees);
                                player.QuestQueue.Add(new Quest()
                                {
                                    Objectives = new List<IMission>()
                                {
                                    new GoToMission(interested.Position, GameFactory.TextureDictionary["BeaconSprite"], player, 0)
                                }
                                });
                            }
                        }
                    }

                    if(!setCourse)
                    {
                        inputManager.WriteLine("I'm confused captain. I can't find anything like that. Do we have cargo in the bay?");
                    }

                    return "";
                },
            };

            foreach (var planet in level.MappedPlanets)
            {
                var planetName = planet.Name;
                if (planet.TeamNumber == 1)
                {
                    planetName += " (Hostile)";
                }

                var setCourseConvo = new Conversation()
                {
                    Question = "Set a course for "+planetName,
                    CustomAnswer = (Game game, Person person, InputManager inputManager, string personName) =>
                    {
                        var player = game._level.Player;
                        var angleInDegrees = AngleUtility.RadiansToDegrees(AngleUtility.GetAngleToPosition(player.Position, planet.Position));
                        inputManager.WriteLine("Setting course for " + planet.Name + " bearing " + (int)angleInDegrees);
                        player.QuestQueue.Add(new Quest()
                        {
                            Objectives = new List<IMission>()
                                        {
                                            new GoToMission(planet.Position, GameFactory.TextureDictionary["BeaconSprite"], player, 0)
                                        }
                        });
                        return "";
                    }

                };
                helmsman.Conversations.Add(setCourseConvo);
            }

            helmsman.Conversations.Add(convo1);
            helmsman.Conversations.Add(tradeStation);
            helmsman.Conversations.Add(repairStation);
            helmsman.Conversations.Add(ammoStation);
            helmsman.Conversations.Add(fuelStation);
            //helmsman.Conversations.Add(setCourseForTrade);

            return helmsman;
        }

        public static Person GetCommsOfficer()
        {
            var comms = new Person();
            comms.Name = "Comms Officer";
            comms.Description = "She talks to people.";

            var convo1 = new Conversation()
            {
                Question = "What sort of things can you do?",
                Answer = "I can call for supplies or repairs to be brought to the ship if we are in trouble."
            };

            var convo2 = new Conversation()
            {
                Question = "I don't see any options here.",
                Answer = "Sir, you need money to have stuff delivered to you. No one will come running for free.",
                DisplayConversation = (Player player) =>
                {
                    if (player.Ship.Money < 300)
                    {
                        return false;
                    }
                    return true;
                }
            };

            var emergencyFuel = new Conversation()
            {
                Question = "Call a fuel delivery ship. ($300 Cost)",
                Answer = "They're on their way sir.",
                PersonEffect = async (Person person, Game game, InputManager inputManager) => 
                {
                    var player = game._level.Player;
                    var cargoShip = GameFactory.GetTrafficAI(game._level, player.Position);

                    player.Ship.Money -= 300;

                    var nearestStation = player.GetNearestPlanetOrStation();
                    cargoShip.Position = nearestStation.Position;
                    cargoShip.Ship.Speed *= 3;
                    cargoShip.Ship.TurnRate *= 5;

                    cargoShip.AfterUpdate = (GameTime gametime, Renderable ship) =>
                    {
                        if(Vector2.Distance(player.Position, ship.Position) < 500)
                        {
                            player.Ship.Fuel = player.Ship.MaxFuel;
                            ship.RemoveFromLevel = true;

                            var traffic = GameFactory.GetTrafficAI(game._level, false);
                            traffic.Position = ship.Position;
                            traffic.Rotation = ship.Rotation;
                            traffic.Texture = ship.Texture;
                            traffic.Scale = ship.Scale;
                            game._level.Objects.Add(traffic);
                        }
                    };

                    game._level.Objects.Add(cargoShip);
                },
                DisplayConversation = (Player player) =>
                {
                    if(player.Ship.Money > 300)
                    {
                        return true;    
                    }
                    return false;
                }
            };

            var emergencyRepairs = new Conversation()
            {
                Question = "Call a repair ship. ($300 Cost)",
                Answer = "They're on their way sir.",
                PersonEffect = async (Person person, Game game, InputManager inputManager) =>
                {
                    var player = game._level.Player;
                    var cargoShip = GameFactory.GetTrafficAI(game._level, player.Position);

                    player.Ship.Money -= 300;

                    var nearestStation = player.GetNearestPlanetOrStation();
                    cargoShip.Position = nearestStation.Position;
                    cargoShip.Ship.Speed *= 3;
                    cargoShip.Ship.TurnRate *= 5;

                    cargoShip.AfterUpdate = (GameTime gametime, Renderable ship) =>
                    {
                        if (Vector2.Distance(player.Position, ship.Position) < 500)
                        {
                            player.Ship.Health = player.Ship.MaxHealth;
                            ship.RemoveFromLevel = true;

                            var traffic = GameFactory.GetTrafficAI(game._level, false);
                            traffic.Position = ship.Position;
                            traffic.Rotation = ship.Rotation;
                            traffic.Texture = ship.Texture;
                            traffic.Scale = ship.Scale;
                            game._level.Objects.Add(traffic);
                        }
                    };

                    game._level.Objects.Add(cargoShip);
                },
                DisplayConversation = (Player player) =>
                {
                    if (player.Ship.Money > 300)
                    {
                        return true;
                    }
                    return false;
                }
            };

            var emergencyAmmo = new Conversation()
            {
                Question = "Call for an emergency ammo delivery. ($300 Cost)",
                Answer = "They're on their way sir.",
                PersonEffect = async (Person person, Game game, InputManager inputManager) =>
                {
                    var player = game._level.Player;
                    var cargoShip = GameFactory.GetTrafficAI(game._level, player.Position);

                    player.Ship.Money -= 300;

                    var nearestStation = player.GetNearestPlanetOrStation();
                    cargoShip.Position = nearestStation.Position;
                    cargoShip.Ship.Speed *= 3;
                    cargoShip.Ship.TurnRate *= 5;

                    cargoShip.AfterUpdate = (GameTime gametime, Renderable ship) =>
                    {
                        if (Vector2.Distance(player.Position, ship.Position) < 500)
                        {
                            player.Ship.Ammo = player.Ship.MaxAmmo;
                            ship.RemoveFromLevel = true;

                            var traffic = GameFactory.GetTrafficAI(game._level, false);
                            traffic.Position = ship.Position;
                            traffic.Rotation = ship.Rotation;
                            traffic.Texture = ship.Texture;
                            traffic.Scale = ship.Scale;
                            game._level.Objects.Add(traffic);
                        }
                    };

                    game._level.Objects.Add(cargoShip);
                },
                DisplayConversation = (Player player) =>
                {
                    if (player.Ship.Money > 300)
                    {
                        return true;
                    }
                    return false;
                }
            };

            comms.Conversations.Add(convo1);
            comms.Conversations.Add(convo2);
            comms.Conversations.Add(emergencyFuel);
            comms.Conversations.Add(emergencyRepairs);
            comms.Conversations.Add(emergencyAmmo);

            return comms;
        }

        public static Person GetLogisticsOfficer()
        {
            Person logisticsOfficer = new Person();
            logisticsOfficer.Name = "Arik Argo";
            logisticsOfficer.JobTitle = "Logistics Officer";

            var convo = new Conversation()
            {
                Question = "How's our cargo looking?",
                Answer = "*shrugs*"
            };

            var convo2 = new Conversation()
            {
                Question = "What is our current cargo?",
                CustomAnswer = (Game game, Person person, InputManager inputManager, string personName) =>
                {
                    var player = game._level.Player;

                    inputManager.WriteLine("[Cargo Inventory]");

                    int counter = 1;
                    foreach(var item in player.Ship.CargoBay)
                    {
                        inputManager.WriteLine(counter + ". " + item.Value + " units of " + item.Key);
                        counter++;
                    }

                    return "";
                },
            };

            var convo3 = new Conversation()
            {
                Question = "Who would be interested in our cargo?",
                CustomAnswer = (Game game, Person person, InputManager inputManager, string personName) =>
                {
                    var player = game._level.Player;

                    inputManager.WriteLine("[Cargo Buyers]");

                    int counter = 1;
                    foreach (var item in player.Ship.CargoBay)
                    {
                        var interested = player.GetNearestPlanetThatWants(item.Key);

                        if (interested != null)
                        {
                            var angleInDegrees = AngleUtility.RadiansToDegrees(AngleUtility.GetAngleToPosition(player.Position, interested.Position));
                            inputManager.WriteLine(counter + ". There's a " + interested.TypeName + " called " + interested.Name + " who will buy our " + item.Key + " they are bearing " + (int)angleInDegrees);
                            counter++;
                        }
                        else
                        {
                            inputManager.WriteLine(counter + ". No one seems to want "+item.Value+" units of "+item.Key+". Think before you buy next time.");
                            counter++;
                        }
                    }

                    return "";
                },
            };

            logisticsOfficer.Conversations.Add(convo);
            logisticsOfficer.Conversations.Add(convo2);
            logisticsOfficer.Conversations.Add(convo3);

            return logisticsOfficer;
        }

        public static Person GetPurchasingAgent(Planet location)
        {
            var merchant = new Person();
            merchant.JobTitle = "Purchasing Agent";
            merchant.Name = GetRandomName();
            merchant.Description = "They look extremely rich by the way they dress.";


            var convo1 = new Conversation()
            {
                Question = "How does this work?",
                Answer = "I am a purchasing agent. I purchase goods on behalf of "+location.Name+". My goal is to growth. I purchase goods that allow us to expand. Expansion is key. Once we have enough we build more until we need more. Get the picture?"
            };

            var upgradeConvo = new Conversation()
            {
                Question = "You look excited.",
                Answer = "We have finally aquired enough to upgrade " + location.Name + " to a level " + (location.UpgradeLevel + 1) + " " + location.TypeName + ".",
                PersonEffect = async (Person person, Game game, InputManager inputManager) =>
                {
                    location.Upgrade(location.UpgradeLevel+1);
                },
                DisplayConversation = (Player player) =>
                {
                    return (location.WantsToAquire.Count == 0);
                },
            };

            foreach(var resource in location.WantsToAquire)
            {
                for (int i = 1; i < 11; i++)
                {
                    var amount = 10 * i;
                    var costPerUnit = 2;
                    var item = resource.Key;
                    if (item == ResourceTypes.Steel)
                    {
                        costPerUnit = 3;
                    }
                    else if (item == ResourceTypes.Electronics)
                    {
                        costPerUnit = 5;
                    }
                    else if (item == ResourceTypes.Fuel)
                    {
                        costPerUnit = 15;
                    }

                    var totalProfit = (int)(costPerUnit * amount);

                    var purchaseOption = new Conversation()
                    {
                        Question = "I'd like to sell " + amount + " units of " + item + " for $"+totalProfit+".",
                        CustomAnswer = (Game game, Person person, InputManager inputManager, string personName) =>
                        {
                            var player = game._level.Player;

                            player.Ship.CargoBay[item] -= amount;
                            
                            player.Ship.Money += totalProfit;

                            return "Excellent, I've unloaded " + amount + " units of " + item + " from your ship's cargo bay.";
                        },
                        DisplayConversation = (Player player) =>
                        {
                            if (player.Ship.CargoBay.ContainsKey(item) && player.Ship.CargoBay[item] >= amount)
                            {
                                return true;
                            }

                            return false;
                        }
                    };

                    merchant.Conversations.Add(purchaseOption);
                }
            }
            
            merchant.Conversations.Add(convo1);
            merchant.Conversations.Add(upgradeConvo);
            return merchant;
        }

        public static Person GetMerchant(Planet location)
        {
            var r = MetaGameSettings.Random;
            var merchant = new Person();
            merchant.JobTitle = "Merchant";
            merchant.Name = GetRandomName();
            merchant.Description = "They look extremely rich by the way they dress.";


            var convo1 = new Conversation()
            {
                Question = "How does trading work?",
                Answer = "There are purchasing agents, and merchants. Merchants sell you goods that a planet or station produce. Purchasing agents buy goods that the station or planet needs. You buy from a merchant, then fly to a place who wants what you bought. Stations and planets need resources to grow, but they are seeking different things usually."
            };

            for (int i = 1; i < 21; i++)
            {
                var amount = 10 * i;
                var costPerUnit = 1.0;
                var item = location.Storehouse.ProductionItem;
                if(location.Storehouse.ProductionItem == ResourceTypes.Steel)
                {
                    costPerUnit = 2.0;
                }
                else if (location.Storehouse.ProductionItem == ResourceTypes.Electronics)
                {
                    costPerUnit = 4.0;
                }
                else if (location.Storehouse.ProductionItem == ResourceTypes.Fuel)
                {
                    costPerUnit = 8.0;
                }

                var totalCost = (int)(costPerUnit * amount);

                var purchaseOption = new Conversation()
                {
                    Question = "Buy "+amount+" units of "+item+" for $"+totalCost+".",
                    CustomAnswer = (Game game, Person person, InputManager inputManager, string personName) =>
                    {
                        var player = game._level.Player;

                        if (player.Ship.CargoBay.ContainsKey(item))
                        {
                            player.Ship.CargoBay[item] += amount;
                        }
                        else
                        {
                            player.Ship.CargoBay.Add(item, amount);
                        }

                        player.Ship.Money -= totalCost;

                        var interested = player.GetNearestPlanetThatWants(item);

                        if (interested != null)
                        {
                            player.QuestQueue.Add(new Quest()
                            {
                                Objectives = new List<IMission>()
                                {
                                    new GoToMission(interested.Position, GameFactory.TextureDictionary["BeaconSprite"], player, 0)
                                }
                            });
                        }

                        return "Excellent, I've loaded " + amount + " units of " + item + " into your ship's cargo bay."; 
                    },
                    DisplayConversation = (Player player) =>
                    {
                        if(player.Ship.GetCargoSpaceLeft() >= amount && player.Ship.Money >= totalCost)
                        {
                            return true;
                        }

                        return false;
                    }
                };

                merchant.Conversations.Add(purchaseOption);
            }


            merchant.Conversations.Add(convo1);
            return merchant;
        }

        public static Person GetAmmoMerchant(Random r)
        {
            var ammoMerchant = new Person();
            ammoMerchant.Name = GetRandomName();
            ammoMerchant.Description = "They look like they sell guns for a living.";

            var conversation = new Conversation()
            {
                Question = "What's it like selling ammo?",
                Answer = "It's hit or miss."
            };
            ammoMerchant.Conversations.Add(conversation);

            for (int i = 1; i < 21; i++)
            {
                var ammoAmount = i * 10;
                var dollarAmount = ammoAmount * 3; // $3 per round
                var convo1 = new Conversation()
                {
                    Question = "Buy $"+dollarAmount+" worth of ammo.",
                    Answer = "Lock and load.",
                    DisplayConversation = (Player player) =>
                    {
                        var capacity = (player.Ship.MaxAmmo - player.Ship.Ammo);
                        if (player.Ship.Money > dollarAmount && ammoAmount < capacity)
                        {
                            return true;
                        }

                        return false;
                    },
                    PersonEffect = async (Person person, Game game, InputManager inputManager) =>
                    {
                        game._level.Player.Ship.Ammo += ammoAmount;
                        game._level.Player.Ship.Money -= dollarAmount;
                    }
                };

                ammoMerchant.Conversations.Add(convo1);
            }

            return ammoMerchant;
        }

        public static Person GetFuelMerchant()
        {
            var fuelMerchant = new Person();
            fuelMerchant.Name = GetRandomName();
            fuelMerchant.Description = "They look like a blue alien or something.";

            var convo0 = new Conversation()
            {
                Question = "Wow, these fuel prices are crazy huh?",
                Answer = "I can't believe what the current space president is doing to this country."
            };
            fuelMerchant.Conversations.Add(convo0);

            for (int i = 1; i < 7; i++)
            {
                var dollarAmount = 20 * i;
                var fuelAmount = dollarAmount * 1_000;
                var convo1 = new Conversation()
                {
                    Question = "Buy $" + dollarAmount + " worth of fuel.",
                    Answer = "Go do some zoomies kiddo.",
                    DisplayConversation = (Player player) =>
                    {
                        var capacity = (1.5 * (player.Ship.MaxFuel - player.Ship.Fuel));
                        if (player.Ship.Money > dollarAmount && fuelAmount < capacity)
                        {
                            return true;
                        }

                        return false;
                    },
                    PersonEffect = async (Person person, Game game, InputManager inputManager) =>
                    {
                        game._level.Player.Ship.Fuel += fuelAmount;
                        game._level.Player.Ship.Money -= dollarAmount;
                    }
                };

                fuelMerchant.Conversations.Add(convo1);
            }

            return fuelMerchant;
        }

        public static Person GetMechanic()
        {
            var mechanic = new Person();
            mechanic.Name = GetRandomName();
            mechanic.Description = "They have a limp and look grumpy all the time. They've got oil stained on their face.";

            var convo0 = new Conversation()
            {
                Question = "Are these repairs really neccesary?",
                Answer = "I'm surprised you were able to make it to the station without imploding."
            };
            mechanic.Conversations.Add(convo0);

            mechanic.Conversations.Add(new Conversation()
            {
                Question = "Can I upgrade my ship multiple times?",
                Answer = "Nope. Usually an upgrade is taking advantage of some extra space. Once it's gone, it's gone."
            });

            mechanic.Conversations.Add(new Conversation()
            {
                Question = "How much do ship upgrades cost?",
                Answer = "I charge $1000 for parts and labor.",
                DisplayConversation = (Player player) =>
                {

                    if (player.Ship.Money < 1000)
                    {
                        return true;
                    }
                    return false;
                }
            });


            mechanic.Conversations.Add(new Conversation()
            {
                Question = "I'd like to upgrade my ship's hull integrity for $1,000.",
                Answer = "Done. Your ship has been upgraded.",
                PersonEffect = async (Person person, Game game, InputManager inputManager) =>
                {
                    var player = game._level.Player;
                    player.Ship.HasBeenUpgraded = true;
                    player.Ship.MaxHealth *= 3;
                    player.Ship.Money -= 1000;
                },
                DisplayConversation = (Player player) =>
                {

                    if (player.Ship.Money < 1000)
                    {
                        return false;
                    }

                    return !player.Ship.HasBeenUpgraded;
                }
            });

            mechanic.Conversations.Add(new Conversation()
            {
                Question = "I'd like to upgrade my ship's ammo capacity for $1,000.",
                Answer = "Done. Your ship has been upgraded.",
                PersonEffect = async (Person person, Game game, InputManager inputManager) =>
                {
                    var player = game._level.Player;
                    player.Ship.HasBeenUpgraded = true;
                    player.Ship.MaxAmmo *= 3;
                    player.Ship.Money -= 1000;
                },
                DisplayConversation = (Player player) =>
                {
                    if (player.Ship.Money < 1000)
                    {
                        return false;
                    }

                    return !player.Ship.HasBeenUpgraded;
                }
            });

            mechanic.Conversations.Add(new Conversation()
            {
                Question = "I'd like to upgrade my ship's fuel capacity for $1,000.",
                Answer = "Done. Your ship has been upgraded.",
                PersonEffect = async (Person person, Game game, InputManager inputManager) =>
                {
                    var player = game._level.Player;
                    player.Ship.HasBeenUpgraded = true;
                    player.Ship.MaxFuel *= 3;
                    player.Ship.Money -= 1000;
                },
                DisplayConversation = (Player player) =>
                {
                    if (player.Ship.Money < 1000)
                    {
                        return false;
                    }

                    return !player.Ship.HasBeenUpgraded;
                }
            });

            mechanic.Conversations.Add(new Conversation()
            {
                Question = "I'd like to upgrade my ship's cargo capacity for $1,000.",
                Answer = "Done. Your ship has been upgraded.",
                PersonEffect = async (Person person, Game game, InputManager inputManager) =>
                {
                    var player = game._level.Player;
                    player.Ship.HasBeenUpgraded = true;
                    player.Ship.MaxCargo *= 3;
                    player.Ship.Money -= 1000;
                },
                DisplayConversation = (Player player) =>
                {
                    if(player.Ship.Money < 1000)
                    {
                        return false;
                    }

                    return !player.Ship.HasBeenUpgraded;
                }
            });

            mechanic.Conversations.Add(new Conversation()
            {
                Question = "I'd like to upgrade my ship's speed for $1,000.",
                Answer = "Done. Your ship has been upgraded.",
                PersonEffect = async (Person person, Game game, InputManager inputManager) =>
                {
                    var player = game._level.Player;
                    player.Ship.HasBeenUpgraded = true;
                    player.Ship.Speed *= 3;
                    player.Ship.Money -= 1000;
                },
                DisplayConversation = (Player player) =>
                {
                    if (player.Ship.Money < 1000)
                    {
                        return false;
                    }

                    return !player.Ship.HasBeenUpgraded;
                }
            });

            mechanic.Conversations.Add(new Conversation()
            {
                Question = "I'd like to upgrade my ship's turn rate for $1,000.",
                Answer = "Done. Your ship has been upgraded.",
                PersonEffect = async (Person person, Game game, InputManager inputManager) =>
                {
                    var player = game._level.Player;
                    player.Ship.HasBeenUpgraded = true;
                    player.Ship.TurnRate *= 3;
                    player.Ship.Money -= 1000;
                },
                DisplayConversation = (Player player) =>
                {
                    if (player.Ship.Money < 1000)
                    {
                        return false;
                    }

                    return !player.Ship.HasBeenUpgraded;
                }
            });

            for (int i = 1; i < 7; i++)
            {
                var dollarAmount = 20 * i;
                var repairAmount = dollarAmount*10;
                var convo1 = new Conversation()
                {
                    Question = "Buy $" + dollarAmount + " worth of repairs.",
                    Answer = "You got a good ship, but you need to take better care of it.",
                    DisplayConversation = (Player player) =>
                    {
                        var capacity = (1.5 * (player.Ship.MaxHealth - player.Ship.Health));
                        if (player.Ship.Money > dollarAmount && repairAmount < capacity)
                        {
                            return true;
                        }

                        return false;
                    },
                    PersonEffect = async (Person person, Game game, InputManager inputManager) =>
                    {
                        game._level.Player.Ship.Health += repairAmount;
                        game._level.Player.Ship.Money -= dollarAmount;
                    }
                };

                mechanic.Conversations.Add(convo1);
            }

            return mechanic;
        }

        public static Person GetFreddy()
        {
            var freddy = new Person();
            freddy.Name = "Freddy";
            freddy.Description = "Just a normal guy.";

            var convo1 = new Conversation()
            {
                Question = "Good morning Freddy.",
                Answer = "Good morning."
            };

            var convo2 = new Conversation()
            {
                Question = "What exactly do you do?",
                Answer = "Sir, my job is to stand here next to the self-destruct button. If you give the order I will blow up the ship."
            };

            var convo3 = new Conversation()
            {
                Question = "How long is the self-destruct timer?",
                Answer = "There's not really a countdown timer, it's just a button that I press. If you want I can...do a countdown...Sir."
            };

            var convo4 = new Conversation()
            {
                Question = "Freddy, it's time. Blow up the damn ship.",
                CustomAnswerAsync = async (Game game, Person person, InputManager inputManager, string characterName) =>
                {
                    inputManager.WriteLine(characterName+": Umm, okay Sir. I'll just uhh. Let's see ten...nine...eight...seven...six...five...four...three");
                    inputManager.WriteLine(characterName + ": Sir, I don't mean to be rude or anything but like, I have a wife and kids. Are you sure you want to blow up the ship?");

                    var response = await inputManager.ReadLine();

                    if(response.ToLower() == "yes" || response.ToLower() == "y") 
                    {
                        inputManager.WriteLine(characterName+": Aye captain. Kind of a shame though, I just payed off my space mortgage.");
                        game._level.Player.Ship.Health = 0;
                    }
                    else
                    {
                        return characterName + ": Oh thank God. You know, we just payed off our space mortgage and I was really looking forward to the extra cash flow.";
                    }

                    return "The ship explodes.";
                }
            };

            freddy.Conversations.Add(convo1);
            freddy.Conversations.Add(convo2);
            freddy.Conversations.Add(convo3);
            freddy.Conversations.Add(convo4);
            return freddy;
        }

        internal static Person GetPlayer()
        {
            var player = new Person();
            player.Name = "Player";
            player.Description = "She's the guy.";

            return player;
        }

        internal static Person GetDiscoveryCorpsPerson()
        {
            var person = new Person();
            Random r = new Random();
            person.Name = GetRandomName();
            person.JobTitle = "Exploration Recruiter";
            person.Description = "They seem normal.";

            var convo1 = new Conversation()
            {
                Question = "What is the Discovery Corps?",
                Answer = "Great question! The Discovery Corps is a government agency in charge of encouraging the exploration of the cosmos. We map out star charts and collect alien samples for study back on Earth."
            };

            var convo2 = new Conversation()
            {
                Question = "Can I earn money in the discovery corps?",
                Answer = "Yes, if you ask me for a mission I will give you the locations of planets we need scanned. When you report back here I will wire you money for your efforts."
            };

            var convo3 = new Conversation()
            {
                Question = "I'd like a discovery mission! I'd love to do my part.",
                Answer = "Done! It's all in your ship's computer now. Come back and I'll get you your reward. Thanks for serving the Discovery Corps.",
                PersonEffect = async (Person specificPerson, Game game, InputManager inputManager) =>
                {
                    // Get all planets the player has not scanned...
                    var planets = game._level.Objects.Where(x => x.GetType() == typeof(Planet) && x.TypeName == LocationTypes.Planet).Cast<Planet>();
                    planets = planets.Where(x => x.TypeName == LocationTypes.Planet && x.IsDiscoverable == true);

                    var player = game._level.Player;

                    if(player.QuestQueue.Count(x => x.TypeName == QuestTypes.DiscoveryCorps) > 3)
                    {
                        inputManager.Clear();
                        inputManager.WriteLine("Woah, nevermind. I'm sorry but it looks like you've already got a lot on your plate right now. I'm gonna have to save these quests for people who are less busy, sorry.");
                        return;
                    }

                    if(player.ScannedPlanets.Count() <= planets.Count())
                    {
                        planets = planets.Where(x => !player.ScannedPlanets.Contains(x));
                    }
                    else
                    {
                        inputManager.Clear();
                        inputManager.WriteLine("You've scanned every planet in the sector. Wow! Unfortunately I can't issue you more discovery quests.");
                        return;
                    }

                    // sort by distance to player
                    var sortedPlanets = planets.OrderBy(x => Vector2.Distance(x.Position, player.Position)).ToList();
                    var midDistancePlanet = sortedPlanets[sortedPlanets.Count() / 12];

                    var name = "Quest to go to and scan " + midDistancePlanet.Name;

                    int selector = 0;
                    while(player.QuestQueue.Any(x => x.Name == name))
                    {
                        selector++;
                        int index = (sortedPlanets.Count() / 12) + selector;

                        if (index < sortedPlanets.Count() - 1)
                        {
                            midDistancePlanet = sortedPlanets[index];
                            name = "Quest to go to and scan " + midDistancePlanet.Name;
                        }
                        else
                        {
                            inputManager.WriteLine("I can't find anything to assign to you right now. Sorry.");
                            return;
                        }
                    }

                    var distance = Vector2.Distance(midDistancePlanet.Position, player.Position);

                    var quest = new Quest();
                    quest.Name = name;
                    quest.TypeName = QuestTypes.DiscoveryCorps;
                    var pointerTexture = GameFactory.TextureDictionary["BeaconSprite"];
                    var goToPlanet = new GoToMission(midDistancePlanet.Position, pointerTexture, player, 0);
                    var scanPlanet = new ScanPlanetMission(midDistancePlanet, player, 0);
                    var goToStation = new GoToMission(player.Position, pointerTexture, player, 0);

                    quest.Objectives.Add(goToPlanet);
                    quest.Objectives.Add(scanPlanet);
                    quest.Objectives.Add(goToStation);
                    quest.OnCompleted = () =>
                    {
                        int reward = (int)((distance / 10_000) * 100);
                        player.Ship.Money += reward;

                        OpenAdventureScreenAnd(() =>
                        {
                            inputManager.Hud.ShowAdventureScreen();
                            inputManager.WriteLine("[Mission Complete]");
                            inputManager.WriteLine("Excellent work explorer. I'm wiring $" + reward + " to your account.");
                        });
                    };

                    player.QuestQueue.Add(quest);

                    inputManager.WriteLine("\nQuest Added: "+name);
                }
            };

            person.Conversations.Add(convo1);
            person.Conversations.Add(convo2);
            person.Conversations.Add(convo3);

            return person;
        }

        internal static Person GetBountyOfficer()
        {
            var bountyOfficer = new Person();
            Random r = new Random();
            bountyOfficer.Name = GetRandomName();
            bountyOfficer.JobTitle = "Bounty Officer";
            bountyOfficer.Description = "They seem normal.";
            var pointerTexture = GameFactory.TextureDictionary["BeaconSprite"];

            var convo1 = new Conversation()
            {
                Question = "How does bounty hunting work?",
                Answer = "Space pirates are a scourge on the universe. We pay you for every pirate ship you destroy. It's that simple. It's all easy money...until it's not."
            };

            var convo3 = new Conversation()
            {
                Question = "Give me a target, I'm ready to kill.",
                Answer = "We slipped a tracking device on a privateer ship captained by "+GetRandomName()+". Find them, blow them up and then I'll wire you your reward.",
                PersonEffect = async (Person specificPerson, Game game, InputManager inputManager) =>
                {
                    
                    var player = game._level.Player;

                    var randomPosition = MetaGameSettings.GetRandomMapPosition(MetaGameSettings.Random);

                    while(Vector2.Distance(randomPosition, player.Position) > 20_000 || Vector2.Distance(randomPosition, player.Position) < MetaGameSettings.ScreenDistance)
                    {
                        randomPosition = MetaGameSettings.GetRandomMapPosition(MetaGameSettings.Random);
                    }

                    Quest quest = new Quest();
                    quest.Name = "Kill Pirates";
                    quest.TypeName = QuestTypes.BountyHunter;

                    // spawn bad guy...
                    for(int i = 0; i < (bountyOfficer.PlayerReputation / 50) + 1; i++)
                    {
                        var fighter = GameFactory.GetPirateFighter(game._level, player);
                        fighter.Position = randomPosition;
                        fighter.SearchForTargets = false;
                        game._level.Objects.Add(fighter);
                        fighter.ImmuneToNonPlayerDamage = true;

                        if (i == 0 || i == 1 || i == 3)
                        {
                            var mission = new GoKillMission(fighter, GameFactory.TextureDictionary["BeaconSpriteRed"], player, 50);
                            quest.Objectives.Add(mission);
                        }

                    }

                    var goToStation = new GoToMission(player.Position, pointerTexture, player, 0);

                    quest.Objectives.Add(goToStation);

                    if (bountyOfficer.PlayerReputation > 200)
                    {
                        var fighter = GameFactory.GetPirateMissileFrigate(game._level, player);
                        fighter.Position = randomPosition;
                        fighter.ImmuneToNonPlayerDamage = true;
                        fighter.SearchForTargets = false;
                        game._level.Objects.Add(fighter);
                    }

                    quest.OnCompleted = () =>
                    {
                        var reward = 150 + bountyOfficer.PlayerReputation;
                        player.Ship.Money += reward;

                        bountyOfficer.PlayerReputation += 50;

                        OpenAdventureScreenAnd(() =>
                        {
                            inputManager.Hud.ShowAdventureScreen();
                            inputManager.WriteLine("[Mission Complete]");
                            inputManager.WriteLine("Excellent work captain, I've wired you $" + reward + " for killing those pirates.");

                        });
                    };

                    player.QuestQueue.Add(quest);
                },
                DisplayConversation = (Player player) =>
                {
                    if(player.QuestQueue.Any(x => x.TypeName == QuestTypes.BountyHunter))
                    {
                        return false;
                    }

                    return true;
                }
            };

            bountyOfficer.Conversations.Add(convo1);
            bountyOfficer.Conversations.Add(convo3);

            return bountyOfficer;
        }

        private static void OpenAdventureScreenAnd(Action action)
        {
            // don't touch my garbage
            // https://stackoverflow.com/questions/19341591/the-application-called-an-interface-that-was-marshalled-for-a-different-thread
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                if(action != null)
                {
                    action();
                }
            });
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }
    }
}
