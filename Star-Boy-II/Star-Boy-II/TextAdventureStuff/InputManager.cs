﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;

namespace Star_Boy_II.TextAdventureStuff
{
    public class InputManager : Renderable
    {
        public List<string> Inputs = new List<string>();

        public HUDViewModel Hud { get; set; }

        public InputManager(HUDViewModel hud)
        {
            Hud = hud;
            Hud.InputManager = this;
        }

        public void UIUpdate()
        {
            // don't touch my garbage
            // https://stackoverflow.com/questions/19341591/the-application-called-an-interface-that-was-marshalled-for-a-different-thread
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UIUpdateActual());
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

        }

        private void UIUpdateActual()
        {
            // Check if user hits enter key...
            if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                if (Hud.UserInputText != string.Empty)
                {
                    Inputs.Add(Hud.UserInputText);
                    Hud.DisplayText = Hud.DisplayText + "\n>> " + Hud.UserInputText+"\n\n";
                    Hud.UserInputText = "";

                    Hud.ScrollDown?.Invoke();
                }
            }
        }

        public override void Update(GameTime gameTime)
        {
            UIUpdate();
        }

        public override void Draw(SpriteBatch spriteBatch, Rectangle window)
        {
            // don't do shit
        }

        public async Task<string> ReadLine()
        {
            var lastInputCount = Inputs.Count;

            while(true)
            {
                if (Inputs.Count > lastInputCount)
                {
                    return Inputs.Last();
                }
                else
                {
                    await Task.Delay(100);
                }
            }
        }

        public void WriteLine(string v)
        {
            Hud.DisplayText += v+"\n";
        }

        public void Clear()
        {
            Hud.DisplayText = "";
        }
    }
}
