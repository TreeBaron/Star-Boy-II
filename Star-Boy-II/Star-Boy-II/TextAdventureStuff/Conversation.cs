﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Star_Boy_II.TextAdventureStuff
{
    public class Conversation
    {
        public string Question { get; set; }
        public string Answer { get; set; }

        public Func<Game, Person, InputManager, string, string> CustomAnswer { get; set; }

        public Func<Game, Person, InputManager, string, Task<string>> CustomAnswerAsync { get; set; }

        public Func<Person, Game, InputManager, Task> PersonEffect { get; set; }

        public Func<Player, bool> DisplayConversation { get; set; }

        public Conversation()
        {
            DisplayConversation = (Player player) =>
            {
                return true;
            };
        }

        public async void Converse(Game game, Person user, InputManager inputManager, string characterName = "")
        {
            var answer = characterName + ": " + Answer;

            inputManager.WriteLine("\n>> "+Question+"\n");

            // syntax!
            if (CustomAnswer != null)
            {
                answer = CustomAnswer(game, user, inputManager, characterName);
            }
            else if (CustomAnswerAsync != null)
            {
                answer = await CustomAnswerAsync(game, user, inputManager, characterName);
            }

            inputManager.WriteLine(answer);
            if (PersonEffect != null)
            {
                await PersonEffect(user, game, inputManager);
            }
        }

    }
}
