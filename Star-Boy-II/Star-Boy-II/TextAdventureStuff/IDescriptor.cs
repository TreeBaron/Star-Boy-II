﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Star_Boy_II.TextAdventureStuff
{
    public interface IDescriptor
    {
        string Name { get; set; }

        string Description { get; set; }
    }
}
