using SharpDX.Direct3D11;
using Star_Boy_II.TextAdventureStuff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Star_Boy_II
{
    public class CommandContext
    {
        public List<string> Commands = new List<string>()
        {
            "go to {object}",
            "look",
            "look at {object}",
            "take {object}",
            "use {object}",
            "use {object} on {object}",
            "talk to {object}"
        };

        private InputManager InputManager;

        public Location CurrentLocation { get; set; }

        public List<Location> AllLocations { get; set; }

        public List<Item> AllItems { get; set; }

        public List<Person> AllPeople { get; set; }

        private Game Game { get; set; }

        public CommandContext(InputManager inputManager, Game game)
        {
            InputManager = inputManager;
            Game = game;

            ProcessCommandAsync();
        }

        public async Task ProcessCommandAsync()
        {
            while(true)
            {
                string userCommand = await InputManager.ReadLine();
                string cleanInput = GetCleanInput(userCommand.ToLower());

                if (CurrentLocation == null)
                {
                    InputManager.WriteLine("Error: No Location Found");
                }
                else
                {
                    int counter = 1;
                    foreach (var person in CurrentLocation.People)
                    {
                        int.TryParse(cleanInput, out var value);
                        if (counter == value)
                        {
                            await TalkTo(person);
                        }
                        counter++;
                    }

                    if(cleanInput == "go to deez nuts")
                    {
                        InputManager.WriteLine("got'em");
                    }

                    if(cleanInput == "bofa")
                    {
                        InputManager.WriteLine("deez nuts");
                    }

                    if(cleanInput == "scan")
                    {
                        ScanPlanet();
                    }

                    if (cleanInput == "look")
                    {
                        Look(CurrentLocation);
                    }
                    else if (cleanInput.Contains("talk to") && GetPersonFromInput(cleanInput) != null)
                    {
                        await TalkTo(GetPersonFromInput(cleanInput));
                    }
                    else if (GetLocationFromInput(cleanInput) != null && cleanInput.Contains("go to"))
                    {
                        GoTo(GetLocationFromInput(cleanInput));
                    }
                }
            }
        }

        private void ScanPlanet()
        {
            InputManager.WriteLine("Scanning planet...");

            // get nearest planet...check if it collides with player
            var player = this.Game._level.Player;
            var nearest = player.GetNearestPlanetOrStation();

            nearest.UpdateCollisionBox();

            if(nearest.CollisionBox.Intersects(player.CollisionBox))
            {
                player.ScannedPlanets.Add(nearest);
                InputManager.WriteLine("Successfully scanned: " + nearest.Name);
                InputManager.WriteLine("Planet information has been stored in the ship's databanks.");
            }
            else
            {
                InputManager.WriteLine("No planet in range.");
            }
        }

        public string GetCleanInput(string input)
        {
            string cleaned = "";
            string acceptableCharacters = " abcdefghijklmnopqrstuvwxyz1234567890";
            for(int i = 0; i < input.Length; i++)
            {
                if (acceptableCharacters.Contains(input[i]+string.Empty))
                {
                    cleaned += input[i];
                }
            }
            return cleaned.ToLower();
        }

        private Location GetLocationFromInput(string input)
        {
            foreach (var loc in CurrentLocation.ConnectedTo)
            {
                if (input.Contains(" " + loc.Name.ToLower()))
                {
                    return loc;
                }
            }

            return null;
        }

        private Person GetPersonFromInput(string input)
        {
            foreach (var person in CurrentLocation.People)
            {
                if (input.Contains(" " + person.Name.ToLower()))
                {
                    return person;
                }
            }

            return null;
        }

        public void GoTo(Location location)
        {
            InputManager.Clear();
            InputManager.WriteLine(">> You go to [" + location.Name + "]\n");
            CurrentLocation = location;
            Look(location);
        }

        public void Look(Location location)
        {
            InputManager.WriteLine("["+location.Name+"]");
            InputManager.WriteLine(location.Description);


            if (location.People.Count > 0)
            {
                var counter = 1;
                foreach (var person in location.People)
                {
                    if (string.IsNullOrEmpty(person.JobTitle))
                    {
                        InputManager.WriteLine("\nYou see ["+counter+"] " + person.Name + " here.");
                    }
                    else
                    {
                        InputManager.WriteLine("\nYou see ["+counter+"] " + person.Name + " ("+person.JobTitle+") here.");
                    }
                    counter++;
                }
            }

            if (location.Items.Count > 0)
            {
                Console.WriteLine("\n\tItems");
                foreach (var item in location.Items)
                {
                    InputManager.WriteLine("\t\tYou see a " + item.Name + " here.");
                }
            }

            if (location.ConnectedTo.Count > 0)
            {
                InputManager.WriteLine("\n\tTravel Destinations");
                foreach (var loc in location.ConnectedTo)
                {
                    InputManager.WriteLine("\t\t" + loc.Name);
                }
            }
        }

        public void LookAt(IDescriptor item)
        {

        }

        public void Take(Item item)
        {

        }

        public void Use(Item item)
        {

        }

        public void UseOn(Item item, Person person)
        {

        }

        public void UseOn(Item item, Item secondItem)
        {

        }

        public async Task TalkTo(Person person)
        {
            InputManager.Clear();
            InputManager.WriteLine("[Dialogue Options for " + person.Name + "]\n");
            var counter = 1;
            foreach (var convo in person.Conversations.Where(x => x.DisplayConversation(Game._level.Player) == true))
            {
                InputManager.WriteLine("[" + counter + "] - " + convo.Question);
                counter++;
            }

            while (true)
            {
                var text = await InputManager.ReadLine();
                var cleanInput = GetCleanInput(text);
                if(cleanInput == "bye" || cleanInput == "")
                {
                    InputManager.Clear();
                    InputManager.WriteLine(">> You say goodbye.");
                    Look(CurrentLocation);
                    break;
                }

                counter = 1;
                foreach (var convo in person.Conversations.Where(x => x.DisplayConversation(Game._level.Player) == true))
                {
                    int.TryParse(cleanInput, out var value);
                    if(GetCleanInput(convo.Question) == cleanInput || value == counter)
                    {
                        InputManager.Clear();

                        InputManager.WriteLine("[Dialogue Options for " + person.Name + "]\n");
                        counter = 1;
                        foreach (var convos in person.Conversations.Where(x => x.DisplayConversation(Game._level.Player) == true))
                        {
                            InputManager.WriteLine("[" + counter + "] - " + convos.Question);
                            counter++;
                        }
                        InputManager.WriteLine("\n\n");

                        convo.Converse(Game, Game._level.Player.PlayerPerson, InputManager, person.Name);

                        break;
                    }
                    counter++;
                }
            }
        }
    }
}