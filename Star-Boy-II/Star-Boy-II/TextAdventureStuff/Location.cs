﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Star_Boy_II.TextAdventureStuff
{
    public class Location : Identure, IDescriptor
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public List<Location> ConnectedTo { get; set; } = new List<Location>();

        public List<Item> Items { get; set; } = new List<Item>();

        public List<Person> People { get; set; } = new List<Person>();

        public int UpgradeLevel { get; set; } = 0;

        public string TypeName { get; set; } = LocationTypes.Station;

        public void ConnectsTo(Location location)
        {
            ConnectedTo.Add(location);
        }

        public void DualConnect(Location location)
        {
            location.ConnectsTo(this);
            ConnectsTo(location);
        }
    }
}
