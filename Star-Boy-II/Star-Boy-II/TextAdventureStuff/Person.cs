﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Star_Boy_II.TextAdventureStuff
{
    public class Person : Identure, IDescriptor
    {
        public string Name { get; set; }

        public string JobTitle { get; set; }

        public string Description { get; set; }

        public Dictionary<string, Object> GenericMemory { get; set; } = new Dictionary<string, Object>();

        /// <summary>
        /// What reputation the player has with this person.
        /// Higher is better. 10+ rep per mission is standard.
        /// </summary>
        public int PlayerReputation = 0;

        public List<Conversation> Conversations { get; set; } = new List<Conversation>();
    }
}
