﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Star_Boy_II.TextAdventureStuff
{
    public class ItemFactory
    {
        public static List<Item> GetAllItems()
        {
            var list = new List<Item>();
            list.Add(GetVase());
            return list;
        }

        public static Item GetVase()
        {
            var item = new Item();
            item.Name = "Vase";
            item.Description = "It's a vase.";
            return item;
        }
    }
}
