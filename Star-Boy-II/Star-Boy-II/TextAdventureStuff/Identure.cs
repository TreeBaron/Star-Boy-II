﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Star_Boy_II.TextAdventureStuff
{
    public class Identure
    {
        private Guid? _identure = null;
        public Guid Id {
            get
            {
                if(_identure == null)
                {
                    _identure = Guid.NewGuid();
                }
                return (Guid)_identure;
            }
        }
    }
}
