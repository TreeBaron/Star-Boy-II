﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Star_Boy_II.TextAdventureStuff
{
    public static class LocationFactory
    {
        public static Location GetSpaceship()
        {
            var bridge = GetSpaceshipBridge();
            var sickbay = GetSickbay();
            var engineering = GetEngineering();
            var crewQuarters = GetCrewQuarters();
            var dockingHatch = GetDockingHatch();

            bridge.DualConnect(sickbay);

            engineering.DualConnect(sickbay);

            crewQuarters.DualConnect(sickbay);

            dockingHatch.DualConnect(bridge);

            return dockingHatch;
        }

        public static Location GetPlayerSpaceship(Dictionary<string, Texture2D> textureDictionary, Level level)
        {
            var bridge = GetSpaceshipBridge();
            var sickbay = GetSickbay();
            var engineering = GetEngineering();
            var crewQuarters = GetCrewQuarters();
            var dockingHatch = GetDockingHatch();

            bridge.DualConnect(sickbay);

            engineering.DualConnect(sickbay);

            crewQuarters.DualConnect(sickbay);

            dockingHatch.DualConnect(bridge);

            bridge.People.Add(PersonFactory.GetFreddy());
            bridge.People.Add(PersonFactory.GetHelmsmen(level));
            bridge.People.Add(PersonFactory.GetCommsOfficer());
            bridge.People.Add(PersonFactory.GetLogisticsOfficer());
            sickbay.Items.Add(ItemFactory.GetVase());

            return bridge;
        }

        public static Location GetDockingHatch()
        {
            var location = new Location();
            location.Name = "Docking Hatch";
            location.Description = "A computer monitor sits above a thousand little switches and dials. A large sliding bulkhead door seperates the ship from oblivion.";
            return location;
        }

        public static Location GetSpaceshipBridge()
        {
            var location = new Location();
            location.Name = "The Bridge";
            location.Description = "Various crew members sit at their stations. You stare off at the stars displayed on the viewscreen and dream of adventure.";
            return location;
        }

        public static Location GetSickbay()
        {
            var location = new Location();
            location.Name = "Sickbay";
            location.Description = "A small hospital packed into a tiny room filled with beds. You hope you never end up here.";
            return location;
        }

        public static Location GetEngineering()
        {
            var location = new Location();
            location.Name = "Engineering";
            location.Description = "Large engines hum and pulse. An engineer sits in the corner monitoring energy levels of the reactor. It smells like grease.";
            return location;
        }

        public static Location GetCrewQuarters()
        {
            var location = new Location();
            location.Name = "Crew Quarters";
            location.Description = "Little more than two walls of beds built into the side of the ship. Small curtains allow shift workers to get some sleep. It's dark and quiet all the time.";
            return location;
        }

        public static Location GetPromenade()
        {
            var location = new Location();
            location.Name = "Promenade";
            location.Description = "The promenade stretches around the station in a donut shape. It connects everything together. Busy merchants and travelers walk by. The station feels very busy.";
            return location;
        }

        public static Location GetTradingBooth()
        {
            var location = new Location();
            location.Name = "Trading Emporium";
            location.Description = "A few ship captains idle at nearby consoles. A few merchants are whispering to each other in the corner.";
            return location;
        }

        public static Location GetDiscoveryCorps()
        {
            var location = new Location();
            location.Name = "Discovery Corps";
            location.Description = "A pleasant lobby with free coffee and snacks. A man sits at a desk quietly clicking away at his computer. A large sign reads 'Discovery Corps' and has images below it of famous explorers.";

            var discoveryPerson = PersonFactory.GetDiscoveryCorpsPerson();
            location.People.Add(discoveryPerson);

            return location;
        }
        
        public static Location GetTradeStation(Planet planet, bool addRepairStation, bool addAmmoStation, bool addFuelStation)
        {
            var promenade = GetPromenade();
            var tradingBooth = GetTradingBooth();
            promenade.DualConnect(tradingBooth);
            var discoverCorps = GetDiscoveryCorps();
            promenade.DualConnect(discoverCorps);

            var bountyOffice = GetBountyOffice();
            promenade.DualConnect(bountyOffice);

            var merchant = PersonFactory.GetMerchant(planet);
            var purchasingAgent = PersonFactory.GetPurchasingAgent(planet);

            tradingBooth.People.Add(merchant);
            tradingBooth.People.Add(purchasingAgent);

            if(addRepairStation)
            {
                var thing = GetRepairStation();
                promenade.DualConnect(thing);
                planet.HasRepairs = true;
            }

            if (addAmmoStation)
            {
                var thing = GetAmmoStation(planet.Name);
                promenade.DualConnect(thing);
                planet.HasAmmo = true;
            }

            if (addFuelStation)
            {
                var thing = GetFuelStation();
                promenade.DualConnect(thing);
                planet.HasFuel = true;
            }

            return promenade;
        }

        public static Location GetBountyOffice()
        {
            var bountyOffice = new Location();
            bountyOffice.Name = "Bounty Office";
            bountyOffice.Description = "Large displays flash photos of criminals and their ships. A man sits in shackles on a bench while a guard stands nearby. It smells of cigarettes for some reason.";

            Person bountyOfficer = PersonFactory.GetBountyOfficer();

            bountyOffice.People.Add(bountyOfficer);

            return bountyOffice;
        }

        public static Location GetAmmoStore()
        {
            var location = new Location();
            location.Name = "Ammo Depot";
            location.Description = "You walk through the darkly lit depot. Large crates of ammunition cast shadows across the bay. The ammo merchant sits idle, smoking a cigar and flipping through a magazine.";
            return location;
        }

        public static Location GetAmmoStation(string nameOfStation)
        {
            var tradingBooth = GetAmmoStore();

            Random r = new Random(DateTime.Now.Millisecond);
            var merchant = PersonFactory.GetAmmoMerchant(r);

            tradingBooth.People.Add(merchant);

            return tradingBooth;
        }

        public static Location GetGasStationLobby()
        {
            var location = new Location();
            location.Name = "Gas Station Lobby";
            location.Description = "Bright white lights light up the snack aisle. You breeze past rotating hot dogs and a freezer filled with ice cream. The attendant looks dead behind the eyes.";
            return location;
        }

        public static Location GetFuelStation()
        {
            var tradingBooth = GetGasStationLobby();

            Random r = new Random(DateTime.Now.Millisecond);
            var merchant = PersonFactory.GetFuelMerchant();

            tradingBooth.People.Add(merchant);

            return tradingBooth;
        }

        public static Location GetMechanicBay()
        {
            var location = new Location();
            location.Name = "Mechanics Bay";
            location.Description = "You see a bunch of spaceships jacked up on stilts. A tire tech nods at you as you walk in. There's free popcorn here for some reason.";
            return location;
        }

        public static Location GetRepairStation()
        {
            var tradingBooth = GetMechanicBay();

            Random r = new Random(DateTime.Now.Millisecond);
            var mechanic = PersonFactory.GetMechanic();

            tradingBooth.People.Add(mechanic);

            return tradingBooth;
        }
    }
}
