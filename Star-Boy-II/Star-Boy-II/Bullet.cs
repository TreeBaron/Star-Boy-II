﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Star_Boy_II.Particles;

namespace Star_Boy_II
{
    public class Bullet : Renderable
    {
        public DateTime Age { get; set; } = DateTime.Now;

        public bool IsPowerUp { get; set; } = false;

        SimpleTimer DeathTimer { get; set; }

        public SpaceShip Effect { get; set; }

        public float Speed { get; set; } = 2500;

        private Level Level { get; set; }

        public Action OnDeath { get; set; }

        public Renderable Owner { get; set; }

        public SpaceShip OwnerShip { get; set; }

        public bool TracksTarget { get; set; }

        public Renderable Target { get; set; }

        public SimpleTimer SmokeEmitTimer { get; set; } = new SimpleTimer(0.025);

        public Bullet(double seconds, Level level, SpaceShip effect)
        {
            Effect = effect;
            Level = level;
            DeathTimer = new SimpleTimer(seconds);
        }

        public override void Update(GameTime gameTime)
        {
             DeathTimer.Update(gameTime);

            if(DeathTimer.Done)
            {
                Level.Objects.Add(ParticleEmitterFactory.GetExplosionEmitter(Position, "Bullet death timer."));
                OnDeath?.Invoke();
                RemoveFromLevel = true;
            }

            // handle velocity
            Position += Velocity * (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (!TracksTarget)
            {
                // face direction of travel
                Rotation = (float)Math.Atan2(Velocity.Y, Velocity.X);
            }
            else
            {
                var wantedRotation = AngleUtility.GetAngleToPosition(Position, Target.Position);
                Rotation = wantedRotation;
                MoveTowards(wantedRotation, Speed, gameTime);
                // simulate friction
                Velocity = new Vector2(Velocity.X * 0.99f, Velocity.Y * 0.99f);

                SmokeEmitTimer.Update(gameTime);

                if(SmokeEmitTimer.Done)
                {
                    SmokeEmitTimer.Reset();

                    // smoke position behind missile not in the center
                    var relativePosition1 = new Vector2((GetHeight()) * -1, 0);
                    relativePosition1 = Position + relativePosition1;
                    relativePosition1 = AngleUtility.RotateAboutOrigin(relativePosition1, Position, Rotation);

                    Level.Objects.Add(ParticleEmitterFactory.GetSmokeEmitter(relativePosition1, "Missile/Bullet smoke."));
                }
            }

            UpdateCollisionBox();

            if(IsPowerUp)
            {
                Rotation = AngleUtility.GetAngleToPosition(Position, Level.Camera.CamerasDesiredPosition);
                MoveTowards(Rotation,50f, gameTime);
                // simulate friction
                Velocity = new Vector2(Velocity.X * 0.99f, Velocity.Y * 0.99f);
            }
        }

        public Bullet GetCopy()
        {
            Bullet copy = new Bullet((DeathTimer.TimerLengthInMilliseconds / 1_000), Level, Effect.GetCopy());
            copy.Speed = Speed;
            copy.Owner = Owner;
            copy.OwnerShip = OwnerShip;
            copy.TeamNumber = TeamNumber;
            copy.Target = Target;
            copy.TracksTarget = TracksTarget;
            copy.OnDeath= OnDeath;
            copy.Texture = Texture;
            copy.Scale = Scale;
            copy.Rotation = Rotation;
            return copy;
        }

        public override void OnCollision(Object collidedWith)
        {
            if(collidedWith.GetType() == typeof(Planet) && ((Planet)collidedWith).TypeName == LocationTypes.Planet) 
            {
                return;
            }

            var isBullet =  collidedWith as Bullet;
            var collided = (ICollidable)collidedWith;
            if (collided.TeamNumber != TeamNumber)
            {
                // if we are not a powerup, kill ourselves if we hit a bullet
                if (!this.IsPowerUp)
                {
                    if (isBullet != null && isBullet.IsPowerUp) return;

                    Level.Objects.Add(ParticleEmitterFactory.GetExplosionEmitter(GetCenter(), "Bullet hit another bullet."));

                    if (this.OnDeath != null)
                    {
                        this.OnDeath.Invoke();
                    }

                    RemoveFromLevel = true;

                    if (!OwnerShip.IsPlayer && isBullet != null)
                    {
                        //Level.Objects.Add(GameFactory.GetAmmoPowerUp(TeamNumber, Level, GetCenter()));
                    }
                }
                // if we did not hit a bullet and we are a powerup, remove ourselves
                else if (isBullet == null && this.IsPowerUp)
                {
                    RemoveFromLevel = true;
                }
                // if we are a powerup and we hit a bullet, do nothing
                else if (this.IsPowerUp && isBullet != null)
                {
                    // do nothing
                }
            }
        }
    }
}
