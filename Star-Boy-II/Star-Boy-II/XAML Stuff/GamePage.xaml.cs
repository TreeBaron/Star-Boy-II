﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Microsoft.Xna.Framework.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Windows;
using System.ServiceModel.Channels;
using Windows.UI.Core;
using System.Diagnostics;
using RotaryWheelUserControl;
using Windows.ApplicationModel.UserDataTasks;
using System.Drawing;
using Microsoft.Xna.Framework;
using System.Threading.Tasks;
using SharpDX.Direct2D1;

namespace Star_Boy_II
{
    public sealed partial class GamePage : Page
    {
        Game _game;

        public HUDViewModel HUD { get; set; }

        public GamePage()
        {
            this.InitializeComponent();
            //this.BindingContext = this;

            // Create the game.
            var launchArguments = string.Empty;
            _game = MonoGame.Framework.XamlGame<Game>.Create(launchArguments, Window.Current.CoreWindow, swapChainPanel);
            _game.GamePage = this;
            HUD = new HUDViewModel();
            HUD.Game = _game;
            _game._level.HUD = HUD;
            _game._level.InputManager = new TextAdventureStuff.InputManager(HUD);
            _game._level.CommandContext = new CommandContext(_game._level.InputManager, _game);
           // Important!
           DataContext = HUD;

            rotaryWheel.Slices = new[]
           {
                "Station",
                "Planet",
                "Ship",
            };

            rotaryWheel.PropertyChanged += RotaryWheelDemo_PropertyChanged;

            HUD.ScrollDown = () =>
            {
                //DisplayTextScrollViewer.ScrollToVerticalOffset(DisplayTextScrollViewer.ScrollableHeight);
                DisplayTextScrollViewer.ChangeView(null, DisplayTextScrollViewer.ExtentHeight, null);
            };

        }

        private void RotaryWheelDemo_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var rotaryWheel = (RotaryWheel)sender;

            switch (e.PropertyName)
            {
                case "SelectedItemValue":
                    {
                        Debug.WriteLine(rotaryWheel.SelectedItemValue);
                        HUD.WheelSelection = rotaryWheel.SelectedItemValue;
                        break;
                    }
            }

        }

        private void CloseAdventureScreen(object sender, RoutedEventArgs e)
        {
            HideAdventureScreen();
        }

        private void TextAdventureModeClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                bool foundPlace = false;
                if (HUD.WheelSelection == "SHIP")
                {
                    foundPlace = true;
                    ShowAdventureScreen();
                    _game._level.CommandContext.CurrentLocation = _game._level.Player.SpaceshipLocation;
                    _game._level.CommandContext.Look(_game._level.CommandContext.CurrentLocation);
                }
                else if (HUD.WheelSelection == "STATION")
                {
                    // get nearest station if any...
                    var station = _game._level.Player.GetNearestStation();
                    station.UpdateCollisionBox();
                    _game._level.Player.UpdateCollisionBox();

                    if (station.CollisionBox.Intersects(_game._level.Player.CollisionBox) || Vector2.Distance(_game._level.Player.GetCenter(), station.GetCenter()) < station.GetWidth())
                    {
                        if (station.EntryPoint != null && _game._level.CommandContext != null)
                        {
                            ShowAdventureScreen();
                            _game._level.CommandContext.CurrentLocation = station.EntryPoint;
                            _game._level.CommandContext.Look(_game._level.CommandContext.CurrentLocation);

                            foundPlace = true;
                        }
                    }
                }

                if (!foundPlace)
                {
                    ShowAdventureScreen();
                    _game._level.CommandContext.CurrentLocation = _game._level.Player.SpaceshipLocation;
                    _game._level.CommandContext.Look(_game._level.CommandContext.CurrentLocation);
                }
            }
            catch
            {
                // nothing for now...
            }
        }

        private void ShowAdventureScreen()
        {
            HUD.ShowTextAdventureScreen = Visibility.Visible;
            _game._level.Paused = true;
            HUD.DisplayText = "";
            HUD.ShowMainUI = Visibility.Collapsed;
        }

        private void HideAdventureScreen()
        {
            HUD.ShowTextAdventureScreen = Visibility.Collapsed;
            _game._level.Paused = false;
            HUD.ShowMainUI = Visibility.Visible;
            HUD.InputManager.Inputs.Add(""); // hacky fix to bug
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            //https://github.com/jpoon/RotaryWheel
            MessageBox.Show("Programming By: John Dodd\nCertain Game Assets By: MillionthVector \n Rotary Dial By: Jason Poon\n Certain Game Assets By: Master484", "", new List<string>() { "Okay" });
        }
    }
}
