﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Star_Boy_II
{
    class PlanetNames
    {

        static string[] A =
        {
            "Ar",
            "Qu",
            "Quo",
            "Xan",
            "Tar",
            "Mel",
            "Melb",
            "Par",
            "Lon",
            "Klin",
            "Clin",
            "Qua",
            "Tarr",
            "Maggle",
            "Zin",
            "Yin",
            "Yang",
            "Post",
            "Po",
            "P",
            "RBX-",
            "Dangery",
            "Doodle",
            "Doo",
            "Foo",
            "Loor",
             "Lor",
             "Data",
             "Plex"
        };

        static string[] B =
        {
            "ag",
            "tta",
            "all",
            "up",
            "mi",
            "a",
            "e",
            "i",
            "o",
            "u"
        };

        static string[] C =
        {
            "un",
            "on",
            "ar",
            "ep",
            "en",
            "in",
            "ith",
            "eth",
            "ude",
            "tte",
            "it",
            "an",
            "ex",
            "lip",
            "tar",
            "ol",
            "er",
            "ing",
            "eck",
            "ck",
            "k"
        };

        public static string GetRandomPlanetName()
        {
            Random random = new Random(DateTime.Now.Second);
            string PostFix = "";

            int Rando = random.Next(-5, 11);

            //A SWITCH STATEMENT IN THE WILD! :O
            switch (Rando)
            {
                case 1:
                    PostFix = " I";
                    break;
                case 2:
                    PostFix = " II";
                    break;
                case 3:
                    PostFix = " III";
                    break;
                case 4:
                    PostFix = " IV";
                    break;
                case 5:
                    PostFix = " V";
                    break;
                case 6:
                    PostFix = " VI";
                    break;
                case 7:
                    PostFix = " VII";
                    break;
                case 8:
                    PostFix = " VIII";
                    break;
                case 9:
                    PostFix = " IX";
                    break;
                case 10:
                    PostFix = " X";
                    break;
                default:
                    PostFix = "";
                    break;
            }

            return A[random.Next(0, A.Length)] + B[random.Next(0, B.Length)] + C[random.Next(0, C.Length)] + PostFix;

        }
    }
}
