﻿using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Star_Boy_II
{
    class TextureGen
    {
        private Random random = new Random(DateTime.Now.Millisecond);

        public static Texture2D CreatePlanetTexture(GraphicsDevice graphicsDevice, int Radius = 390, int width = 800, int height = 800,  PlanetSettings planetSettings = null)
        {
            //default value for planet settings
            if (planetSettings == null)
            {
                planetSettings = new PlanetSettings();
                planetSettings.BlendingAllowed = true;
            }

            //http://www.riemers.net/eng/Tutorials/XNA/Csharp/Series2D/Manual_texture_creation.php
            Vector2 center = new Vector2(width / 2, height / 2);

            //Console.WriteLine("Calling Map Painter");
            List<List<Color>> ColorField = MapPainter(width, height, planetSettings);
            //Console.WriteLine("Calling Cloud Painter");
            List<List<Color>> CloudField = CloudPainter(width, height, planetSettings);
            Color[] GroundColors = new Color[width * height];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    Vector2 Pos = new Vector2(x, y);
                    //if the distance is less than circle radius
                    if (Vector2.Distance(center, Pos) < Radius)
                    {
                        //draw algorithm
                        GroundColors[x + y * width] = ColorField[y][x];

                        //cover in clouds...
                        if (CloudField[y][x] == planetSettings.Clouds)
                        {
                            GroundColors[x + y * width] = planetSettings.Clouds;
                        }
                    }
                    //add atmosphere...
                    else if (Vector2.Distance(center, Pos) >= Radius && Vector2.Distance(center, Pos) <= Radius + planetSettings.AtmosphereThickness)
                    {
                        GroundColors[x + y * width] = planetSettings.Atmosphere;
                    }
                    else
                    {
                        GroundColors[x + y * width] = Color.Transparent;
                    }

                }
            }

            Texture2D texture = new Texture2D(graphicsDevice, width, height);
            //Console.WriteLine("Setting Texture Data");
            texture.SetData(GroundColors);

            return texture;
        }

        public static Texture2D CreateAsteroidTexture(GraphicsDevice graphicsDevice, int Radius = 400, int height = 800, PlanetSettings PS = null)
        {
            int width = height;

            //default value for planet settings
            if (PS == null)
            {
                PS = PlanetSettings.GetAsteroidSettings();
            }


            PS.ContinentAmount = 1;
            PS.Atmosphere = Color.Transparent; //no atmosphere

            //http://www.riemers.net/eng/Tutorials/XNA/Csharp/Series2D/Manual_texture_creation.php
            Vector2 center = new Vector2(width / 2, height / 2);
            List<List<Color>> ColorField = AsteroidMapPainter(width, height, PS, Radius);
            Color[] GroundColors = new Color[width * height];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {

                    Vector2 Pos = new Vector2(x, y);
                    //if the distance is less than circle radius
                    if (Vector2.Distance(center, Pos) < Radius * 0.8)
                    {
                        //draw algorithm
                        GroundColors[x + y * width] = ColorField[y][x];
                        //NO CLOUDS                        
                    }
                    else
                    {
                        GroundColors[x + y * width] = Color.Transparent;
                    }

                }
            }

            Texture2D texture = new Texture2D(graphicsDevice, width, height);
            texture.SetData(GroundColors);

            return texture;
        }

        private static List<List<Color>> AsteroidMapPainter(int x_val, int y_val, PlanetSettings PS, int Radius)
        {
            Random random = new Random(DateTime.Now.Second);
            const int DepthLimit = 50;
            List<List<Color>> ColorField = new List<List<Color>>();
            int[,] NumberField = new int[y_val, x_val];

            List<Vector2> CPoints = new List<Vector2>();

            //add asteroid central point...
            for (int i = 0; i < 3 + random.Next(0, 3); i++)
            {
                CPoints.Add(new Vector2(x_val / 2 + random.Next(-1 * Radius / 4, Radius / 4), y_val / 2 + random.Next(-1 * Radius / 4, Radius / 4)));
            }

            //fill number field with 0's
            //also fill colorfield
            for (int y = 0; y < y_val; y++)
            {
                List<Color> locallist = new List<Color>();
                for (int x = 0; x < x_val; x++)
                {
                    NumberField[y, x] = random.Next(0, 10);
                    locallist.Add(Color.Transparent);

                    Vector2 Centerboi = new Vector2(y_val / 2, x_val / 2);
                    Vector2 PositionBoi = new Vector2(y, x);

                    if (Vector2.Distance(Centerboi, PositionBoi) < Radius + random.Next(-100, 100) || random.Next(0, 200) == 1)
                    {
                        //1 in 2 chance big continent
                        if (random.Next(0, 2) == 1)
                        {
                            //150 best
                            int Bonus = ContinentBonus(CPoints, x, y, Radius * 0.3);

                            //add mountain peaks
                            if (random.Next(0, 5) == 1 && Bonus > 95)
                            {
                                Bonus += 300;
                            }
                            Bonus += random.Next(40, 120);

                            NumberField[y, x] += Bonus;
                        }

                    }

                }
                ColorField.Add(locallist);
            }


            //average values with cubes next to them...HORIZONTAL
            for (int i = 0; i < 6; i++) //best 15
            {
                for (int y = 1; y < y_val; y++)
                {
                    for (int x = 1; x < x_val; x++)
                    {
                        NumberField[y, x] = (NumberField[y, x] + NumberField[y, x - 1]) / 2;
                    }
                }
            }

            //average values with cubes next to them...VERTICAL
            for (int i = 0; i < 6; i++) //best 15
            {
                for (int y = 1; y < y_val; y++)
                {
                    for (int x = 1; x < x_val; x++)
                    {
                        NumberField[y, x] = (NumberField[y - 1, x] + NumberField[y, x]) / 2;
                    }
                }
            }


            //copy original values...
            int[,] OriginalField = new int[y_val, x_val];
            for (int y = 0; y < y_val; y++)
            {
                for (int x = 0; x < x_val; x++)
                {
                    OriginalField[y, x] = NumberField[y, x];
                }
            }


            //okay, use the silloutte of the number field over the original...
            for (int y = 0; y < y_val; y++)
            {
                for (int x = 0; x < x_val; x++)
                {
                    if (NumberField[y, x] > DepthLimit)
                    {
                        NumberField[y, x] = OriginalField[y, x];

                        if (NumberField[y, x] < DepthLimit)
                        {
                            NumberField[y, x] += DepthLimit;
                        }
                    }
                }
            }

            //fill colors
            ColorField = FillColorsAsteroid(NumberField, ColorField, DepthLimit, PS);

            return ColorField;
        }

        private static List<List<Color>> MapPainter(int x_val, int y_val, PlanetSettings PS)
        {
            Random random = new Random(DateTime.Now.Second);
            const int DepthLimit = 50;
            List<List<Color>> ColorField = new List<List<Color>>();
            int[,] NumberField = new int[y_val, x_val];

            List<Vector2> CPoints = new List<Vector2>();
            //generate 10 continents
            for (int i = 0; i < PS.ContinentAmount; i++)
            {
                //add random position point
                float leftboi = random.Next(0, x_val);
                float rightboi = random.Next(0, y_val);
                CPoints.Add(new Vector2(leftboi, rightboi));

                //chance of double dipping for mountains and stuff...
                if (random.Next(0, 2) == 1)
                {
                    //CPoints.Add(new Vector2(leftboi, rightboi));
                }

            }
            //Console.WriteLine("Added CPoints for Continents.");

            //Console.WriteLine("Generating Number Field");
            //fill number field with 0's
            //also fill colorfield
            for (int y = 0; y < y_val; y++)
            {
                List<Color> locallist = new List<Color>();
                for (int x = 0; x < x_val; x++)
                {
                    NumberField[y, x] = random.Next(0, 10);
                    locallist.Add(Color.HotPink);

                    //1 in 2 chance big continent
                    if (random.Next(0, 2) == 1)
                    {
                        //150 best
                        int Bonus = ContinentBonus(CPoints, x, y, 250);

                        //add lakes...
                        if (random.Next(0, 17) == 1)
                        {
                            Bonus = random.Next(-75, -35);
                        }

                        //add mountain peaks
                        if (random.Next(0, 5) == 1 && Bonus > 95)
                        {
                            Bonus += 200;
                        }


                        NumberField[y, x] += Bonus;
                    }
                    else
                    {
                        int Bonus = ContinentBonus(CPoints, x, y, 150);

                        //20 55 best
                        NumberField[y, x] += Bonus;
                    }

                    //chance 1 in 50 that a cpoint changes...
                    //simulates brush movement on canvas
                    if (random.Next(0, 201) == 1)
                    {
                        try
                        {
                            int pos = random.Next(0, CPoints.Count);
                            CPoints[pos] = new Vector2(CPoints[pos].X + random.Next(-10, 10), CPoints[pos].Y + random.Next(-10, 10));
                        }
                        catch (Exception e)
                        {
                            //do nothing...
                        }
                    }
                }
                ColorField.Add(locallist);
            }

            //Console.WriteLine("Horizontal Melding...");
            //average values with cubes next to them...HORIZONTAL
            for (int i = 0; i < 3; i++)
            {
                for (int y = 1; y < y_val; y++)
                {
                    for (int x = 1; x < x_val; x++)
                    {
                        NumberField[y, x] = (NumberField[y, x] + NumberField[y, x - 1]) / 2;
                    }
                }
            }

            //Console.WriteLine("Vertical Melding...");
            //average values with cubes next to them...VERTICAL
            for (int i = 0; i < 3; i++)
            {
                for (int y = 1; y < y_val; y++)
                {
                    for (int x = 1; x < x_val; x++)
                    {
                        NumberField[y, x] = (NumberField[y - 1, x] + NumberField[y, x]) / 2;
                    }
                }
            }

            Console.WriteLine("Filling Colors: " + DateTime.Now.Minute + ":" + DateTime.Now.Second);
            //fill colors
            ColorField = FillColors(NumberField, ColorField, DepthLimit, PS);

            return ColorField;
        }

        private static List<List<Color>> CloudPainter(int x_val, int y_val, PlanetSettings PS)
        {
            Random random = new Random(DateTime.Now.Second);
            const int DepthLimit = 50;
            List<List<Color>> ColorField = new List<List<Color>>();
            int[,] NumberField = new int[y_val, x_val];

            List<Vector2> CPoints = new List<Vector2>();
            //generate 8 continents
            for (int i = 0; i < PS.CloudAmount; i++)
            {
                //add random position point
                float leftboi = random.Next(0, x_val);
                float rightboi = random.Next(0, y_val);
                CPoints.Add(new Vector2(leftboi, rightboi));
            }

            //fill number field with 0's
            //also fill colorfield
            for (int y = 0; y < y_val; y++)
            {
                List<Color> locallist = new List<Color>();
                for (int x = 0; x < x_val; x++)
                {
                    NumberField[y, x] = random.Next(0, 10);
                    locallist.Add(Color.Black);

                    //150 best
                    int Bonus = ContinentBonus(CPoints, x, y, random.Next(PS.MinCloudsize, PS.MaxCloudSize));

                    NumberField[y, x] += Bonus;

                    //chance 1 in 50 that a cpoint changes...
                    //simulates brush movement on canvas
                    if (random.Next(0, 51) == 1 && PS.CloudAmount > 0)
                    {
                        int pos = random.Next(0, CPoints.Count);
                        CPoints[pos] = new Vector2(CPoints[pos].X + random.Next(-20, 20), CPoints[pos].Y + random.Next(-10, 10));
                    }
                }
                ColorField.Add(locallist);
            }

            //average values with cubes next to them...HORIZONTAL
            for (int i = 0; i < 3; i++)
            {
                for (int y = 1; y < y_val; y++)
                {
                    for (int x = 1; x < x_val; x++)
                    {
                        NumberField[y, x] = (NumberField[y, x] + NumberField[y, x - 1]) / 2;
                    }
                }
            }

            //average values with cubes next to them...VERTICAL
            for (int i = 0; i < 3; i++)
            {
                for (int y = 1; y < y_val; y++)
                {
                    for (int x = 1; x < x_val; x++)
                    {
                        NumberField[y, x] = (NumberField[y - 1, x] + NumberField[y, x]) / 2;
                    }
                }
            }

            //fill colors
            ColorField = FillCloudColors(NumberField, ColorField, DepthLimit, PS);


            return ColorField;
        }

        private static List<List<Color>> FillColors(int[,] NumberField, List<List<Color>> ColorField, int DepthLimit, PlanetSettings PS)
        {
            //only do this once instead of a billion times
            Color[] GroundLevels = PS.GetGroundLevels();
            Console.WriteLine("Color Fill Size: " + NumberField.GetLength(0) * NumberField.GetLength(1));
            //selects the appropriate color based on height map, and adds it to colorlist...
            for (int y = 0; y < NumberField.GetLength(0); y++)
            {
                for (int x = 0; x < NumberField.GetLength(1); x++)
                {
                    //if number field is less than zero...
                    if (NumberField[y, x] <= 0)
                    {
                        NumberField[y, x] = 1;
                    }
                    else if (NumberField[y, x] > 99)
                    {
                        NumberField[y, x] = 99;
                    }
                    ColorField[y][x] = GroundLevels[NumberField[y, x]];

                    if (ColorField[y][x].A != 255)
                    {
                        Color c = new Color();
                        c.A = 255;
                        c.R = ColorField[y][x].R;
                        c.G = ColorField[y][x].G;
                        c.B = ColorField[y][x].B;
                        ColorField[y][x] = c;
                    }
                }
            }
            Console.WriteLine("Done Filling Colors: " + DateTime.Now.Minute + ":" + DateTime.Now.Second);

            return ColorField;
        }

        private static List<List<Color>> FillColorsAsteroid(int[,] NumberField, List<List<Color>> ColorField, int DepthLimit, PlanetSettings PS)
        {
            Random random = new Random(DateTime.Now.Second);
            //selects the appropriate color based on height map, and adds it to colorlist...
            for (int y = 0; y < NumberField.GetLength(0); y++)
            {
                for (int x = 0; x < NumberField.GetLength(1); x++)
                {
                    //go through color layers one at a time...
                    for (int i = 0; i < 10; i++)
                    {
                        //if number field is less than zero...
                        if (NumberField[y, x] <= 0)
                        {
                            NumberField[y, x] = 1;
                        }

                        int Bar = 120 - i * 10;

                        if (NumberField[y, x] > 50)//best 50
                        {
                            if (NumberField[y, x] > 70)//best60
                            {
                                ColorField[y][x] = PS.GetSimpleGroundLevels()[random.Next(5, 9)];
                                break;
                            }
                            else
                            {
                                ColorField[y][x] = PS.GetSimpleGroundLevels()[10 - 1 - i];
                                break;
                            }
                        }
                    }

                }
            }

            return ColorField;
        }

        private static List<List<Color>> FillCloudColors(int[,] NumberField, List<List<Color>> ColorField, int DepthLimit, PlanetSettings PS)
        {
            //selects the appropriate color based on height map, and adds it to colorlist...
            for (int y = 0; y < NumberField.GetLength(0); y++)
            {
                for (int x = 0; x < NumberField.GetLength(1); x++)
                {
                    if (NumberField[y, x] > 50)
                    {
                        ColorField[y][x] = PS.Clouds;
                    }
                }
            }

            return ColorField;
        }

        //loops through a set of points, and determines if 
        //a bonus should be given, and 
        public static int ContinentBonus(List<Vector2> CPoints, int posx, int posy, double ContinentDistance = 100)
        {
            Random random = new Random(DateTime.Now.Second);
            Vector2 Pos = new Vector2(posx, posy);
            int Total = 0;

            foreach (Vector2 V in CPoints)
            {
                //modify local distance to add rough coast lines
                //continentdistance/2 , continent distance
                //int LocalDistanceCheck = random.Next((int)ContinentDistance/2,(int)ContinentDistance);
                int LocalDistanceCheck = (int)ContinentDistance;

                //land bonus
                if (Vector2.Distance(Pos, V) < LocalDistanceCheck)
                {
                    //lakes less likely as we approach center
                    if (Vector2.Distance(Pos, V) < LocalDistanceCheck / 100 * 96)
                    {
                        //best 0 15
                        Total = random.Next(50, 106);
                    }
                    else
                    {
                        //best 10 to 28
                        Total = random.Next(40, 85);
                    }
                }
            }

            if (Total == 0)
            {
                //lower number for less islands
                //best -75 to -25
                //return random.Next(-15, -10);
                return 0;
            }
            else
            {
                return Total;
            }
        }


    }
}
