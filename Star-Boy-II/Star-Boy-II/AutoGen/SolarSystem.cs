﻿/*
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Star_Boy_II;

namespace Avoider.AutoGen
{
    class SolarSystem
    {

        /// <summary>
        /// Makes a solar system.
        /// </summary>
        /// <param name="Position">The center position of the solar system (the sun's position)</param>
        /// <param name="CurrentLevel">The level associated with.</param>
        /// <param name="threatLevel">The amount of enemy bases in this star system.</param>
        /// <param name="PlanetCount">The amount of planets in this star system.</param>
        /// <returns></returns>
        public static List<Gravity_Source> GenerateRandomSolarSystem(Vector2 Position,Level CurrentLevel,int threatLevel = 3,int PlanetCount = 5)
        {
            List<Gravity_Source> SolSys = new List<Gravity_Source>();

            Console.WriteLine("Generating Random Solar System");

            //generate the sun!
            Texture2D SunText = AutoGen.TextureGen.CreatePlanetTexture(580, 1200, 1200, PlanetSettings.GetSunPlanetSettings());
            Gravity_Source Sun = new Gravity_Source("SUN", SunText, 10.0f,Position,CurrentLevel, 64000f);
            Sun.Locked = true;
            SolSys.Add(Sun);

            //keep track of orbit distance...
            float OrbitDistance = 800+Sun.GetWidth();
            float IncrementAmount = 200;
            float LastRotationSpeed = 0.0f;

            //generate number of planets...
            for(int i = 0; i < PlanetCount; i++)
            {
                Console.WriteLine("Making Planet "+i+" of "+PlanetCount);
                Texture2D PlanetTexture = null;

                Random random = new Random(DateTime.Now.Millisecond);

                int Rando = random.Next(0,10);

                if(Rando == 0)
                {
                    Console.WriteLine("Generating Random Asteroid");
                    PlanetTexture = AutoGen.TextureGen.CreateAsteroidTexture(290, 600, PlanetSettings.GetAsteroidSettings(true));
                }
                else if (Rando == 1)
                {
                    Console.WriteLine("Generating Ice Planet");
                    PlanetTexture = AutoGen.TextureGen.CreatePlanetTexture(290, 600, 600, PlanetSettings.GetIcePlanetSettings());
                }
                else if (Rando == 2)
                {
                    Console.WriteLine("Generating Random Earth-Like Planet");
                    PlanetTexture = AutoGen.TextureGen.CreatePlanetTexture(290, 600, 600, new PlanetSettings());
                }
                else if (Rando == 3)
                {
                    Console.WriteLine("Generating Random Lava Planet");
                    PlanetTexture = AutoGen.TextureGen.CreatePlanetTexture(290, 600, 600, PlanetSettings.GetLavaPlanetSettings());
                }
                else if (Rando == 4)
                {
                    Console.WriteLine("Generating Totally Random Planet");
                    PlanetTexture = AutoGen.TextureGen.CreatePlanetTexture(290, 600, 600, PlanetSettings.GetRandomPlanetSettings());
                }
                else if (Rando == 5)
                {
                    Console.WriteLine("Generating Totally Random Planet");
                    PlanetTexture = AutoGen.TextureGen.CreatePlanetTexture(290, 600, 600, PlanetSettings.GetSuperRandomPlanetSettings());
                }
                else
                {
                    Console.WriteLine("Generating Totally Random Planet");
                    PlanetTexture = AutoGen.TextureGen.CreatePlanetTexture(290, 600, 600, PlanetSettings.GetRandomOceanPlanetSettings());
                }

                Gravity_Source PlanetBoi = new Gravity_Source(PlanetNames.GetRandomPlanetName(), PlanetTexture, random.Next(16,64), Position, CurrentLevel,0);
                if (LastRotationSpeed == 0)
                {
                    LastRotationSpeed = PlanetBoi.OrbitSpeed;
                }
                else
                {
                    LastRotationSpeed = PlanetBoi.OrbitSpeed * 0.05f;
                }

                PlanetBoi.WillNowOrbit(Sun,OrbitDistance+IncrementAmount+(PlanetBoi.GetWidth()*2));
                IncrementAmount += PlanetBoi.GetWidth()*2;

                //decide how far away next planet in orbit will be...
                IncrementAmount += (IncrementAmount * (float)random.NextDouble());

                //set planet draw scale
                PlanetBoi.scale *= 0.75f;

                SolSys.Add(PlanetBoi);
            }

            //add enemy bases
            AddEnemyBases(threatLevel, SolSys, CurrentLevel);

            return SolSys;
        }

        private static void AddEnemyBases(int amount,List<Gravity_Source> planets,Level currentLevel)
        {
            for(int i = 0; i < amount; i++)
            {
                //do not select the sun
                Gravity_Source planet = planets[Settings.R.Next(1,planets.Count)];

                //generate enemy base
                StarBase enemyBase = currentLevel.Add_Starbase(planet.Position);

                //have it orbit planet
                enemyBase.WillNowOrbit(planet);

                //add it to game objects list
                currentLevel.Game_Objects.Add(enemyBase);
            }
        }
    }
}
*/