﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System;

namespace Star_Boy_II
{
    class PlanetSettings
    {
        /// <summary>
        /// Displays at the planet's edges.
        /// </summary>
        public Color Atmosphere = Color.CornflowerBlue; //best cornflowerblue
        public int AtmosphereThickness = -1; //best -1
        public int CloudAmount = 135; //best 135
        public Color Clouds = Color.WhiteSmoke;
        public int ContinentAmount = 12; //best 12
        public int MaxCloudSize = 85; //best 85
        public int MinCloudsize = 17; //best 17
        public bool BlendingAllowed = false;

        public static Random R = new Random(DateTime.Now.Second);

        private Color[] GroundLevels = new Color[10]
        {
            //begin ocean colors...
            new Color(15,15,255,255),
            Color.Blue, //0,0,255,255
            Color.Blue,
            Color.Blue,
            Color.Blue, //best is deepskyblue or cyan or darkturquoise lol
            Color.Khaki, //best is khaki
            Color.LimeGreen, //lime green
            Color.ForestGreen, //forest green
            Color.DarkGray, //sandy brown
            Color.White //light gray
        };

        //activate smoothing of ground levels, based on basic input of 10
        public Color[] GetGroundLevels()
        {
            List<Color> AdvancedGroundLevels = new List<Color>();
            for (int i = 0; i < GroundLevels.Length; i++)
            {
                for (int x = 0; x < 10; x++)
                {
                    AdvancedGroundLevels.Add(GroundLevels[i]);
                }
            }

            //shuffle list!
            //simulate detail and mixture!
            if (BlendingAllowed)
            {
                for (int i = 0; i < 3; i++)
                {
                    AdvancedGroundLevels = ShuffleList(AdvancedGroundLevels);
                    AdvancedGroundLevels = BlendList(AdvancedGroundLevels);
                }
            }

            return AdvancedGroundLevels.ToArray();
        }

        public Color[] GetSimpleGroundLevels()
        {
            return GroundLevels;
        }


        public List<Color> BlendList(List<Color> ListBoi)
        {
            //blend colors...
            for (int i = 1; i < ListBoi.Count; i++)
            {
                //if the next color is not the same as this...
                if (ListBoi[i - 1] != ListBoi[i])
                {
                    Color NewBoi = new Color();
                    NewBoi.R = (byte)((ListBoi[i - 1].R + ListBoi[i].R) / 2);
                    NewBoi.B = (byte)((ListBoi[i - 1].B + ListBoi[i].B) / 2);
                    NewBoi.G = (byte)((ListBoi[i - 1].G + ListBoi[i].G) / 2);

                    ListBoi[i - 1] = NewBoi;
                }
            }
            return ListBoi;
        }

        public List<Color> ShuffleList(List<Color> ListBoi)
        {
            for (int i = 1; i < ListBoi.Count; i++)
            {
                //50 50
                if (PlanetSettings.R.Next(0, 2) == 1)
                {
                    //ah the old swaperoo
                    Color Temp = ListBoi[i - 1];
                    ListBoi[i - 1] = ListBoi[i];
                    ListBoi[i] = Temp;
                }
            }


            return ListBoi;
        }


        public static PlanetSettings GetLavaPlanetSettings()
        {
            Color[] ColorLevels = new Color[10]
            {
                //begin ocean colors...
                Color.Yellow,
                Color.DarkOrange,
                Color.Orange,
                Color.OrangeRed,
                Color.MonoGameOrange,
                Color.Red,
                Color.DarkRed,
                Color.DarkOrange,
                Color.Black,
                Color.DimGray
            };

            PlanetSettings NewPS = new PlanetSettings();
            NewPS.GroundLevels = ColorLevels;
            NewPS.Clouds = Color.WhiteSmoke;
            NewPS.CloudAmount = 0;
            NewPS.AtmosphereThickness = -1;
            NewPS.Atmosphere = Color.Black;
            NewPS.ContinentAmount *= 3;
            NewPS.BlendingAllowed = true;

            return NewPS;
        }

        public static PlanetSettings GetSunPlanetSettings()
        {
            int rando = PlanetSettings.R.Next(0, 12);
            Color PrimaryColor = Color.Yellow;

            if (rando == 1)
            {
                PrimaryColor = Color.Red;
            }
            else if (rando == 2)
            {
                PrimaryColor = Color.Blue;
            }
            else if (rando == 3)
            {
                PrimaryColor = Color.White;
            }
            else if (rando == 4)
            {
                PrimaryColor = Color.Purple;
            }
            else if (rando == 5)
            {
                PrimaryColor = Color.Green;
            }
            else if (rando == 6)
            {
                PrimaryColor = Color.Orange;
            }

            Color[] ColorLevels = new Color[10]
            {
                //begin ocean colors...
                GetColorVariant(PrimaryColor,5),
                PrimaryColor,
                GetColorVariant(PrimaryColor,5),
                PrimaryColor,
                GetColorVariant(PrimaryColor,5),
                PrimaryColor,
                GetColorVariant(PrimaryColor,10),
                PrimaryColor,
                PrimaryColor,
                GetColorVariant(PrimaryColor,15),
            };

            PlanetSettings NewPS = new PlanetSettings();
            NewPS.GroundLevels = ColorLevels;
            NewPS.Clouds = Color.Yellow;
            NewPS.CloudAmount = 0;
            NewPS.AtmosphereThickness = -1;
            NewPS.Atmosphere = Color.Black;
            NewPS.ContinentAmount *= 3;
            NewPS.BlendingAllowed = true;

            return NewPS;
        }


        public static PlanetSettings GetIcePlanetSettings()
        {
            Color[] ColorLevels = new Color[10]
            {
                //begin ocean colors...
                Color.White,
                Color.Cyan,
                Color.Cyan,
                Color.LightCyan,
                Color.GhostWhite,
                Color.WhiteSmoke,
                Color.LightCyan,
                Color.FloralWhite,
                Color.WhiteSmoke,
                Color.LightGray,
            };

            PlanetSettings NewPS = new PlanetSettings();
            //set the layers of colors
            NewPS.GroundLevels = ColorLevels;
            NewPS.Clouds = Color.White;
            NewPS.CloudAmount = 180;
            NewPS.AtmosphereThickness = 1;
            NewPS.Atmosphere = Color.GhostWhite;
            NewPS.ContinentAmount = 30;
            NewPS.MinCloudsize = 45;
            NewPS.BlendingAllowed = true;

            return NewPS;
        }

        public static PlanetSettings GetRandomPlanetSettings()
        {
            // earth type intercept
            if(R.NextDouble() < 0.2)
            {
                return new PlanetSettings();
            }

            // earth type intercept
            if (R.NextDouble() < 0.2)
            {
                return GetIcePlanetSettings();
            }

            // earth type intercept
            if (R.NextDouble() < 0.2)
            {
                return GetLavaPlanetSettings();
            }

            // earth type intercept
            if (R.NextDouble() < 0.2)
            {
                return GetOreAsteroidSettings();
            }

            Color Top1 = GetRandomColor();
            Color Bottom1 = GetRandomColor();

            Color[] ColorLevels = new Color[10]
            {
                //begin ocean colors...
                Bottom1,
                GetRandomColor(),
                GetRandomColor(),
                Bottom1,
                Top1,
                GetRandomColor(),
                Top1,
                Top1,
                Top1,
                GetRandomColor()
            };

            PlanetSettings NewPS = new PlanetSettings();
            //set the layers of colors
            NewPS.GroundLevels = ColorLevels;
            NewPS.Clouds = GetRandomColor();
            NewPS.CloudAmount = PlanetSettings.R.Next(-200, 200);
            NewPS.CloudAmount = Math.Max(0, NewPS.CloudAmount);
            NewPS.AtmosphereThickness = PlanetSettings.R.Next(0, 10);
            Color Atmo = GetRandomColor();
            NewPS.Atmosphere = Atmo;
            NewPS.ContinentAmount = PlanetSettings.R.Next(0, 120);
            NewPS.MinCloudsize = PlanetSettings.R.Next(0, 60);
            NewPS.BlendingAllowed = true;

            return NewPS;
        }

        public static PlanetSettings GetRandomOceanPlanetSettings()
        {
            Color Bottom1 = GetRandomColor();

            Color[] ColorLevels = new Color[10]
            {
                //begin ocean colors...
                Bottom1,
                Bottom1,
                Bottom1,
                Bottom1,
                Bottom1,
                Bottom1,
                GetRandomColor(),
                GetRandomColor(),
                GetRandomColor(),
                GetRandomColor()
            };

            PlanetSettings NewPS = new PlanetSettings();
            //set the layers of colors
            NewPS.GroundLevels = ColorLevels;
            NewPS.Clouds = GetRandomColor();
            NewPS.CloudAmount = PlanetSettings.R.Next(-200, 200);
            NewPS.CloudAmount = Math.Max(0, NewPS.CloudAmount);
            NewPS.AtmosphereThickness = PlanetSettings.R.Next(0, 10);
            Color Atmo = GetRandomColor();
            NewPS.Atmosphere = Atmo;
            NewPS.ContinentAmount = PlanetSettings.R.Next(0, 120);
            NewPS.MinCloudsize = PlanetSettings.R.Next(0, 60);
            NewPS.BlendingAllowed = true;

            return NewPS;
        }

        public static PlanetSettings GetSuperRandomPlanetSettings()
        {
            Color[] ColorLevels = new Color[10]
            {
                //begin ocean colors...
                GetRandomColor(),
                GetRandomColor(),
                GetRandomColor(),
                GetRandomColor(),
                GetRandomColor(),
                GetRandomColor(),
                GetRandomColor(),
                GetRandomColor(),
                GetRandomColor(),
                GetRandomColor()
            };

            PlanetSettings NewPS = new PlanetSettings();
            //set the layers of colors
            NewPS.GroundLevels = ColorLevels;
            NewPS.Clouds = GetRandomColor();
            NewPS.CloudAmount = PlanetSettings.R.Next(-200, 200);
            NewPS.CloudAmount = Math.Max(0, NewPS.CloudAmount);
            NewPS.AtmosphereThickness = PlanetSettings.R.Next(0, 10);
            Color Atmo = GetRandomColor();
            NewPS.Atmosphere = Atmo;
            NewPS.ContinentAmount = PlanetSettings.R.Next(0, 120);
            NewPS.MinCloudsize = PlanetSettings.R.Next(0, 60);
            NewPS.BlendingAllowed = true;

            return NewPS;
        }


        public static PlanetSettings GetRandomColorSquareSettings()
        {

            Color Top1 = GetRandomColor();


            Color[] ColorLevels = new Color[10]
            {
                //begin ocean colors...
                Top1,
                Top1,
                Top1,
                Top1,
                Top1,
                Top1,
                Top1,
                Top1,
                Top1,
                Top1
            };

            PlanetSettings NewPS = new PlanetSettings();
            //set the layers of colors
            NewPS.GroundLevels = ColorLevels;
            NewPS.Clouds = Top1;
            NewPS.CloudAmount = 0;
            NewPS.AtmosphereThickness = 0;
            NewPS.Atmosphere = Top1;
            NewPS.ContinentAmount = 0;
            NewPS.MinCloudsize = 0;
            NewPS.MaxCloudSize = 0;
            NewPS.BlendingAllowed = true;

            return NewPS;
        }

        public static PlanetSettings GetRandomColorSquareSettings(Color A)
        {

            Color Top1 = A;


            Color[] ColorLevels = new Color[10]
            {
                //begin ocean colors...
                Top1,
                Top1,
                Top1,
                Top1,
                Top1,
                Top1,
                Top1,
                Top1,
                Top1,
                Top1
            };

            PlanetSettings NewPS = new PlanetSettings();
            //set the layers of colors
            NewPS.GroundLevels = ColorLevels;
            NewPS.Clouds = Top1;
            NewPS.CloudAmount = 0;
            NewPS.AtmosphereThickness = 0;
            NewPS.Atmosphere = Top1;
            NewPS.ContinentAmount = 0;
            NewPS.MinCloudsize = 0;
            NewPS.MaxCloudSize = 0;
            NewPS.BlendingAllowed = true;


            return NewPS;
        }

        public static PlanetSettings GetAsteroidSettings(bool OreAsteroids = true)
        {

            Color[] ColorLevels = new Color[10]
            {
                //begin ocean colors...
                Color.Transparent,
                Color.Transparent,
                Color.Transparent,
                Color.Transparent,
                Color.Transparent,
                GetColorVariant(new Color(49,49,49)),
                GetColorVariant(new Color(49,49,49)),
                GetColorVariant(new Color(49,49,49)),
                GetColorVariant(new Color(59,59,59)),
                GetColorVariant(new Color(79,79,79)),
            };

            if (OreAsteroids == true)
            {
                if (PlanetSettings.R.Next(0, 5) == 2)
                {
                    Color OrCol = GetRandomColor();
                    ColorLevels[9] = OrCol;

                    //ultra rare super ore filled asteroid
                    if (PlanetSettings.R.Next(0, 101) == 50)
                    {
                        ColorLevels[8] = OrCol;
                    }
                }
            }

            PlanetSettings NewPS = new PlanetSettings();
            //set the layers of colors
            NewPS.GroundLevels = ColorLevels;
            NewPS.BlendingAllowed = false;//important

            return NewPS;
        }

        public static PlanetSettings GetOreAsteroidSettings()
        {

            Color[] ColorLevels = new Color[10]
            {
                //begin ocean colors...
                GetColorVariant(new Color(53,53,53)),
                GetColorVariant(new Color(53,53,53)),
                GetColorVariant(new Color(53,53,53)),
                GetColorVariant(new Color(53,53,53)),
                GetColorVariant(new Color(53,53,53)),
                GetColorVariant(new Color(49,49,49)),
                GetColorVariant(new Color(49,49,49)),
                GetColorVariant(new Color(49,49,49)),
                GetColorVariant(new Color(59,59,59)),
                GetColorVariant(new Color(79,79,79)),
            };

            Color OrCol = GetRandomColor();
            ColorLevels[9] = OrCol;

            //ultra rare super ore filled asteroid
            if (PlanetSettings.R.Next(0, 101) == 50)
            {
                ColorLevels[8] = OrCol;
            }

            PlanetSettings NewPS = new PlanetSettings();
            //set the layers of colors
            NewPS.GroundLevels = ColorLevels;

            return NewPS;
        }

        public static Color GetRandomColor()
        {
            Color c = new Color
                (
                (byte)PlanetSettings.R.Next(0, 256),
                (byte)PlanetSettings.R.Next(0, 256),
                (byte)PlanetSettings.R.Next(0, 256)
                );
            c.A = 255;
            return c;
        }

        public static Color GetColorVariant(Color A, int variance = 5, bool GrayScale = true)
        {
            Color B = new Color(0, 0, 0);
            if (GrayScale == false)
            {
                B = new Color(A.R + PlanetSettings.R.Next(-variance, variance), A.G + PlanetSettings.R.Next(-variance, variance), A.B + PlanetSettings.R.Next(-variance, variance));
            }
            else
            {
                int datboi = PlanetSettings.R.Next(-variance, variance);
                B = new Color(A.R + datboi, A.G + datboi, A.B + datboi);
            }
            B.A = 255;
            return B;
        }

    }
}
