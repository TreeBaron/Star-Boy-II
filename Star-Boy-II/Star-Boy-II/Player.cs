using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Star_Boy_II.Missions;
using Star_Boy_II.Particles;
using Star_Boy_II.TextAdventureStuff;
using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI.Core;

namespace Star_Boy_II
{
    public class Player : Renderable
    {

        /// <summary>
        /// Various components (visual, logical) that as a whole compose the player.
        /// </summary>
        List<Renderable> PlayerComponents { get; set; } = new List<Renderable>();

        /// <summary>
        /// A list of guns.
        /// </summary>
        public List<Gun> Guns { get; set; } = new List<Gun>();

        /// <summary>
        /// The player's spaceship data.
        /// </summary>
        public SpaceShip Ship = SpaceShip.GetPlayerShip();

        /// <summary>
        /// The camera which we can command to follow the player.
        /// </summary>
        Camera Camera { get; set; }

        private Game Game { get; set; }

        private SimpleTimer ParticleTimer { get; set; } = new SimpleTimer(0.01);

        private Random Random { get; set; } = new Random();

        private TrafficAI PlayerNavigation { get; set; }

        private SimpleTimer AsteroidTimer { get; set; } = new SimpleTimer(10);

        private SimpleTimer RaidTimer { get; set; } = new SimpleTimer(90);

        public Renderable TargetReticle { get; set; }

        private List<ParticleEmitter> ShipEngineEmitters { get; set; } = new List<ParticleEmitter>();

        public List<Quest> QuestQueue { get; set; } = new List<Quest>();

        public Location SpaceshipLocation { get; set; }

        public Person PlayerPerson { get; set; }

        public string SpaceshipImagePath { get; set; }
        public List<Planet> ScannedPlanets { get; set; } = new List<Planet>();
        public List<Renderable> KillList { get; set; } = new List<Renderable>();

        private List<Renderable> ActiveRaiders = new List<Renderable>();

        public void Initialize(Game game, Camera camera, Dictionary<string, Texture2D> textureDictionary)
        {

            SpaceshipImagePath = "/Textures/SpaceshipSprite.png";
            Game = game;
            var texture = game.Content.Load<Texture2D>("SpaceshipSprite");
            Texture = texture;
            Scale = 0.2f;
            Ship.IsPlayer = true;
            Camera = camera;

            PlayerNavigation = GameFactory.GetTrafficAI(game._level, false);
            PlayerNavigation.Ship = Ship;
            SpaceshipLocation = LocationFactory.GetPlayerSpaceship(textureDictionary, Game._level);

            PlayerPerson = PersonFactory.GetPlayer();

            // Testing stuff
            //Ship.Money = 10_000;

        }

        public override void Draw(SpriteBatch spriteBatch, Rectangle window)
        {

            QuestDrawing(spriteBatch);

            base.Draw(spriteBatch, window);

            foreach(var component in PlayerComponents)
            {
                component.Draw(spriteBatch, window);
            }

            PointGuns(window);

            MoveMouseTarget(window);

            TargetReticle.Draw(spriteBatch, window);

        }

        private void QuestUpdating(GameTime gameTime)
        {
            List<Quest> toRemove = new List<Quest>();
            foreach(var quest in QuestQueue)
            {
                quest.Update(gameTime);
                var completed = quest.IsComplete(Game._level, this);

                if(completed)
                {
                    toRemove.Add(quest);
                }
            }

            foreach(var quest in toRemove)
            {
                QuestQueue.Remove(quest);
            }
        }

        private void QuestDrawing(SpriteBatch spriteBatch)
        {
            foreach (var quest in QuestQueue)
            {
                quest.Draw(spriteBatch);
            }
        }

        public override void Update(GameTime gameTime)
        {
            #region Some stuff

            QuestUpdating(gameTime);

            base.Update(gameTime);

            //float wantedRotation = AngleUtility.GetAngleToPosition(Position, CurrentWaypoint);

            //Rotation = AngleUtility.RotationLogic(Rotation, wantedRotation, Ship.TurnRate);

            // handle velocity
            Position += Velocity * (float)gameTime.ElapsedGameTime.TotalSeconds;

            // simulate friction
            Velocity = new Vector2(Velocity.X * 0.9f, Velocity.Y * 0.9f);

            UpdateCollisionBox();
            #endregion

            TargetReticle.Update(gameTime);

            foreach(var component in PlayerComponents)
            {
                component.Update(gameTime);
            }

            if(Ship.IsDead())
            {
                Game._level.Objects.Add(ParticleEmitterFactory.GetExplosionEmitter(GetCenter(), "Player has died. Dead player ship."));
                //Game._level.Paused = true;
                MessageBox.Show("Game Over!", "Game Over!", new List<string>() { "I suck!" });
                Game.ResetGame();
            }

            // update camera to follow the spaceship body of the player
            Camera.CamerasDesiredPosition = Position;

            // Controls
            Controls(gameTime);

            // guns stay with player
            for(int i = 0; i < Guns.Count; i++)
            {
                var gun = Guns[i];
                gun.Position = Position;
                gun.Update(gameTime);
            }

            PlaceEngineFlames(gameTime);

            // update collision box
            UpdateCollisionBox();

            SpawnAsteroids(gameTime);

            RaidTimer.Update(gameTime);

            SpawnRaider();
        }

        public void SpawnRaider(bool immediate = false)
        {
            if (RaidTimer.Done || immediate)
            {

                RaidTimer.Reset();

                if(ActiveRaiders.Count > 5)
                {
                    ActiveRaiders = ActiveRaiders.Where(x => x.RemoveFromLevel == false).ToList();
                    return;
                }

                var multiplier = 10;
                var futurePosition = Vector2.Multiply(Velocity, multiplier);
                var measuredVelocity = new Vector2(10, 10);

                if (Velocity.X > 1 || Velocity.Y > 1 || Velocity.X < -1 || Velocity.Y < -1)
                {
                    measuredVelocity = Velocity;
                }
                    while (Vector2.Distance((futurePosition + Position), Position) < MetaGameSettings.ScreenDistance)
                    {
                        multiplier += 5;
                        futurePosition = Vector2.Multiply(measuredVelocity, multiplier);
                    }

                    futurePosition = futurePosition + Position;

                    // Turn into a combat ai in the blink of an eye
                    var combatModel = GameFactory.GetMissileFrigate(Game._level, this);
                    var fighter1 = GameFactory.GetEnemyFighter(Game._level, this);
                    var fighter2 = GameFactory.GetEnemyFighter(Game._level, this);

                    combatModel.Position = futurePosition;
                    fighter1.Position = futurePosition + new Vector2(-200, 0);
                    fighter2.Position = futurePosition + new Vector2(200, 0);

                    // Remove if far enough from player
                    combatModel.AfterUpdate = (GameTime gameTime, Renderable ship) =>
                    {
                        if(Vector2.Distance(this.Position, ship.Position) > MetaGameSettings.ScreenDistance * 6)
                        {
                            ship.RemoveFromLevel = true;
                            ActiveRaiders.Remove(ship);
                        }
                    };
                    combatModel.AfterUpdate = (GameTime gameTime, Renderable ship) =>
                    {
                        if (Vector2.Distance(this.Position, ship.Position) > MetaGameSettings.ScreenDistance * 6)
                        {
                            ship.RemoveFromLevel = true;
                            ActiveRaiders.Remove(ship);
                        }
                    };
                    combatModel.AfterUpdate = (GameTime gameTime, Renderable ship) =>
                    {
                        if (Vector2.Distance(this.Position, ship.Position) > MetaGameSettings.ScreenDistance * 6)
                        {
                            ship.RemoveFromLevel = true;
                            ActiveRaiders.Remove(ship);
                        }
                    };

                Random r = new Random();

                    if (r.NextDouble() < 0.5)
                    {
                        Game._level.Objects.Add(combatModel);
                        ActiveRaiders.Add(combatModel);
                    }

                    if (r.NextDouble() < 0.5)
                    {
                        Game._level.Objects.Add(fighter1);
                        ActiveRaiders.Add(fighter1);
                    }

                    ActiveRaiders.Add(fighter2);
                    Game._level.Objects.Add(fighter2);

            }
        }

        private void SpawnAsteroids(GameTime gameTime)
        {

            AsteroidTimer.Update(gameTime);
            if (AsteroidTimer.Done)
            {
                var multiplier = 10;
                var futurePosition = Vector2.Multiply(Velocity, multiplier);
                var measuredVelocity = new Vector2(3, 3);

                if (Velocity.X > 1 || Velocity.Y > 1 || Velocity.X < -1 || Velocity.Y < -1)
                {
                    measuredVelocity = Velocity;
                }
                while (Vector2.Distance((futurePosition + Position), Position) < MetaGameSettings.ScreenDistance)
                {
                    multiplier += 5;
                    futurePosition = Vector2.Multiply(measuredVelocity, multiplier);
                }

                futurePosition = futurePosition + Position;

                    // power up asteroids
                    for (int i = 0; i < Random.Next(1,30); i++)
                    {
                        var asteroid = GameFactory.GetRandomAsteroid(Game._level);
                        asteroid.Position = futurePosition + new Vector2(Random.Next(-2000, 2000), Random.Next(-2000, 2000));

                        Game._level.Objects.Add(asteroid);
                    }
                
                AsteroidTimer.Reset();
            }
        }

        private void PointGuns(Rectangle window)
        {
            var mouse = Mouse.GetState();
            var shipPosition = new Vector2(window.Width / 2, window.Height / 2);

            var mousePosition = new Vector2(mouse.X, mouse.Y);

            // get angle to mouse from ship
            float rotation = AngleUtility.GetAngleToPosition(shipPosition, mousePosition);

            foreach(var gun in Guns)
            {
                gun.Rotation = rotation;
            }
        }

        private void MoveMouseTarget(Rectangle window)
        {
            var mouse = Mouse.GetState();
            var halfScreen = new Vector2(window.Width / 2, window.Height / 2);

            var mousePosition = new Vector2(mouse.X, mouse.Y);
            mousePosition = mousePosition + (Camera.CamerasDesiredPosition - halfScreen);

            // place target on mouse
            TargetReticle.Position = mousePosition;
        }

        /// <summary>
        /// Detect if we are touching a planet and if so, give the player a random quest!
        /// </summary>
        public void HailedPlanetEvent()
        {
            var closePlanet = GetNearestPlanetOrStation();

            if (Vector2.Distance(closePlanet.GetCenter(), GetCenter()) < closePlanet.GetWidth())
            {
                if (closePlanet.TypeName == LocationTypes.Planet || closePlanet.TypeName == LocationTypes.Station)
                {
                    //todo: something happens here...
                }
            }
        }

        public Planet GetNearestPlanetThatWants(string item)
        {
            var planets = Game._level.Objects.Where(x => x.GetType() == typeof(Planet)).Cast<Planet>();
            var wantsResource = planets.Where(planet => planet.WantsToAquire.ContainsKey(item) && planet.EntryPoint != null);

            var sorted = wantsResource.OrderBy(x => Vector2.Distance(Position, x.Position));

            return sorted.FirstOrDefault();
        }

        private void PlaceEngineEmitters()
        {
            if(Ship.Fuel <= 0)
            {
                return;
            }

            var relativePosition1 = new Vector2((GetHeight()) * -1, GetWidth() / 8);
            relativePosition1 = Position + relativePosition1;
            relativePosition1 = AngleUtility.RotateAboutOrigin(relativePosition1, Position, Rotation);
            var emitter1 = ParticleEmitterFactory.GetPlayerShipFlameEmitter(relativePosition1, "Player ship engines.");

            var relativePosition2 = new Vector2((GetHeight()) * -1, (GetWidth() / 8) * -1);
            relativePosition2 = Position + relativePosition2;
            relativePosition2 = AngleUtility.RotateAboutOrigin(relativePosition2, Position, Rotation);
            var emitter2 = ParticleEmitterFactory.GetPlayerShipFlameEmitter(relativePosition2, "Player ship engines.");

            Game._level.Objects.Add(emitter1);
            Game._level.Objects.Add(emitter2);

            ShipEngineEmitters.Add(emitter1);
            ShipEngineEmitters.Add(emitter2);
        }

        public void PlaceEngineFlames(GameTime gameTime)
        {
            ParticleTimer.Update(gameTime);

            if(ShipEngineEmitters.Count == 0 && Ship.Fuel > 0)
            {
                PlaceEngineEmitters();
            }

            if(ParticleTimer.Done || Ship.Fuel <= 0)
            {
                ParticleTimer.Reset();
                
                foreach(var emitter in ShipEngineEmitters)
                {
                    emitter.RemoveFromLevel = true;
                }
                ShipEngineEmitters.Clear();

                PlaceEngineEmitters();
            }
        }

        private int PercentTotal(double current, double max)
        {
            return (int)((current / max) * 100.0);
        }

        public void UIUpdate()
        {
            // don't touch my garbage
            // https://stackoverflow.com/questions/19341591/the-application-called-an-interface-that-was-marshalled-for-a-different-thread
            #pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UIUpdateActual());
            #pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

        }

        private void UIUpdateActual()
        {
            var hud = Game._level.HUD;

            hud.ScrollDown();

            var angleOfShip = Math.Round(AngleUtility.RadiansToDegrees(this.Rotation), 0);
            while (angleOfShip > 360)
            {
                angleOfShip -= 360;
            }

            while (angleOfShip < 0)
            {
                angleOfShip += 360;
            }

            hud.Bearing = "Bearing: "+angleOfShip;

            hud.Position = "("+(int)this.Position.X+","+(int)this.Position.Y+")";

            try
            {
                var angle = GetAngleToNearestPlanetOrStation();
                var angleInDegrees = AngleUtility.RadiansToDegrees(angle);

                while(angleInDegrees > 360)
                {
                    angleInDegrees -= 360;
                }

                while (angleInDegrees < 0)
                {
                    angleInDegrees += 360;
                }

                var nearestPlanet = GetNearestPlanetOrStation();

                hud.PlanetBearing = nearestPlanet.Name+": " + Math.Round(angleInDegrees, 0);
            }
            catch(Exception ex)
            {
                var garbage = ex;
            }

            hud.Money = "$" + Ship.Money;
            hud.Ammo = Ship.Ammo;

            hud.Health = PercentTotal(Ship.Health, Ship.MaxHealth);
            hud.Fuel = PercentTotal(Ship.Fuel, Ship.MaxFuel);

            hud.SpaceshipImagePath = SpaceshipImagePath;

            hud.ShipStatusText = "";
            if(Ship.Health < 0.75 * Ship.MaxHealth)
            {
                hud.ShipStatusText += "Reactor Status - Nominal";
            }
            else if (Ship.Health < 0.5 * Ship.MaxHealth)
            {
                hud.ShipStatusText += "Reactor Status - Damaged";
            }
            else if (Ship.Health < 0.25 * Ship.MaxHealth)
            {
                hud.ShipStatusText += "Reactor Status - CRITICAL DAMAGE";
            }

            hud.ShipStatusText += "\nFuel Measure - " + Ship.Fuel + " MGUs";
            hud.ShipStatusText += "\nFuel Capacity - " + Ship.MaxFuel + " MGUs";

            if(Ship.Ammo >= 1)
            {
                hud.ShipStatusText += "\nAmmunition Reserves - "+Ship.Ammo;
            }
            else
            {
                hud.ShipStatusText += "\nAmmunition Reserves - DEPLETED";
            }
            hud.ShipStatusText += "\nAmmunition Capacity - " + Ship.MaxAmmo;
            hud.ShipStatusText += "\nShip Bearing - "+AngleUtility.RadiansToDegrees(Rotation)+" Degrees";
            hud.ShipStatusText += "\nMax Speed - " + Ship.Speed + " Per MSU";

            if(Ship.Health < 0.25*Ship.MaxHealth)
            {
                hud.ShipStatusText += "!!! -DAMAGE CRITICAL SEEK REPAIR- !!!";
            }
        }

        private void Controls(GameTime gameTime)
        {

            // move forward
            if (Ship.Fuel > 0)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.W))
                {
                    MoveTowards(Rotation, Ship.Speed, gameTime);
                    Ship.Fuel--;
                }

                // boost
                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                {
                    MoveTowards(Rotation, Ship.Speed * 3, gameTime);
                    Ship.Fuel -= 3;
                }
            }

            // turn left
            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                Rotation -= Ship.TurnRate;
            }

            // turn right
            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                Rotation += Ship.TurnRate;
            }
            

            // fire guns
            if (Mouse.GetState().RightButton == ButtonState.Pressed)
            {
                if (Ship.Ammo > 0)
                {
                    foreach (var gun in Guns)
                    {
                        if (gun.Fire(Velocity))
                        {
                            //this.Game._level.SoundEffectDictionary["pew"].Play();
                            Ship.Ammo--;
                        }
                    }
                }
            }
        }

        public override void OnCollision(Object collidedWith)
        {
            ICollidable renderableCol = collidedWith as ICollidable;
            if (collidedWith.GetType() == typeof(Bullet))
            {
                var bullet = (Bullet)collidedWith;
                if (bullet.TeamNumber != this.TeamNumber)
                {
                    Ship.ApplyEffect(bullet.Effect);
                }
            }
            else if (renderableCol != null)
            {
                //SlidePast(this as ICollidable, renderableCol);

                /*
                var otherShip = renderableCol as TrafficAI;
                if (otherShip != null)
                {
                    Vector2 collisionPoint = Position + otherShip.Position;
                    collisionPoint = new Vector2(collisionPoint.X / 2, collisionPoint.Y / 2);

                    Game._level.Objects.Add(ParticleEmitterFactory.GetShreddedMetalEmitter(collisionPoint));
                    Game._level.Objects.Add(ParticleEmitterFactory.GetSparkEmitter(collisionPoint));
                }
                */

                var asteroid = renderableCol as Asteroid;
                if(asteroid != null)
                {
                    Ship.Health -= MetaGameSettings.PlayerAsteroidDamage;
                }
            }
        }

        public Planet GetNearestPlanetOrStation()
        {
            var levelObjects = Game._level.Objects.Where(x => x != null);
            var sorted = levelObjects
                .Where(x => x != null && x.TypeName != null &&
                x.TypeName == LocationTypes.Planet || x.TypeName == LocationTypes.Station)
                .OrderBy(x => Vector2.Distance(this.GetCenter(), x.Position));
            var nearestPlanet = sorted.FirstOrDefault();
            return ((Planet)nearestPlanet);
        }

        public Planet GetNearestStation()
        {
            var planets = Game._level.Objects.Where(x => x.GetType() == typeof(Planet)).Cast<Planet>();
            var planetsFiltered = planets.Where(planet => planet.TypeName == LocationTypes.Station);
            var planetsSorted = planets.OrderBy(x => Vector2.Distance(x.Position, this.Position)).ToList();

            return planetsSorted.First();
        }

        public float GetAngleToNearestPlanetOrStation()
        {
            var planet = GetNearestPlanetOrStation();

            if (planet == null) return 0f;

            return AngleUtility.GetAngleToPosition(this.GetCenter(), planet.GetCenter());
        }
    }
}
