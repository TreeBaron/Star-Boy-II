using Microsoft.Xna.Framework;
using Star_Boy_II;
using System.Collections.Generic;
using System.Linq;

public class CollisionTracker
{
    public List<(Point, Point)> LinesOfRectangle { get; set; }

    public List<Vector2> CornerVectors { get; set; }

    /// <summary>
    /// Upper left corner.
    /// </summary>
    public Vector2 Position { get; set; }

    private Vector2 Center {get; set;}

    /// <summary>
    /// Give the corners like you are reading. Left to right,
    /// top to bottom.
    /// </summary>
    /// <param name="corners"></param>
    public CollisionTracker(List<Vector2> corners)
    {
        Position = corners[0];

        var upperLeftCorner = corners[0];
        var upperRightCorner = corners[1];

        var lowerLeftCorner = corners[2];
        var lowerRightCorner = corners[3];

        CornerVectors = new List<Vector2>() { 
            upperLeftCorner,
            upperRightCorner,
            lowerLeftCorner,
            lowerRightCorner,
        };

        LinesOfRectangle = new List<(Point, Point)>()
        {
            (VectorToPoint(upperLeftCorner),VectorToPoint(upperRightCorner)),
            (VectorToPoint(upperLeftCorner),VectorToPoint(lowerLeftCorner)),
            (VectorToPoint(upperRightCorner),VectorToPoint(lowerRightCorner)),
            (VectorToPoint(lowerLeftCorner),VectorToPoint(lowerRightCorner)),
        };

    }

    private Point VectorToPoint(Vector2 vector)
    {
        return new Point((int)vector.X, (int)vector.Y);
    }

    public float GetCircleRadius()
    {
        var leftCorner = CornerVectors[0];
        var rightCorner = CornerVectors[1];
        var lowerRightCorner = CornerVectors[3];

        var firstDistance = Vector2.Distance(leftCorner, rightCorner);
        var secondDistance = Vector2.Distance(leftCorner, lowerRightCorner);

        return ((float)System.Math.Min(firstDistance, secondDistance)) / 2.0f;

    }

    public Vector2 GetCenterOfTracker()
    {
        return GetCenter(CornerVectors.First(), CornerVectors.Last());
    }

    public Vector2 GetCenter(Vector2 firstPoint, Vector2 secondPoint)
    {
        return new Vector2(firstPoint.X + secondPoint.X / 2, firstPoint.Y + secondPoint.Y / 2);
    }

    public bool Intersects(CollisionTracker checkBox)
    {
        var distance = checkBox.GetCircleRadius() + GetCircleRadius();
        var firstResult = Vector2.Distance(checkBox.GetCenterOfTracker(), GetCenterOfTracker()) < distance;

        if(firstResult == true)
        {
            return true;
        }

        return AngleUtility.RectanglesCollide(LinesOfRectangle, checkBox.LinesOfRectangle);
    }
}
